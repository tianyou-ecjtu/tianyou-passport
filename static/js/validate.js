function showErrorHelp(name, err) {
    if (err) {
        $('#input-' + name).addClass('is-invalid');
        $('#help-' + name).text(err);
        return false;
    } else {
        $('#input-' + name).removeClass('is-invalid');
        $('#help-' + name).text('');
        return true;
    }
}

function getFormErrorAndShowHelp(name, val, info = "") {
    var err = val($('#input-' + name).val(), info);
    return showErrorHelp(name, err);
}

function validateUsername(str, info) {
    if (str.length == 0) {
        return '用户名不能为空';
    } else if (!/^[a-zA-Z0-9_]+$/.test(str)) {
        return '用户名应只包含英文字母、数字和下划线';
    } else if (str.length > 30) {
        return '用户名长度不应超过30'
    } else {
        return '';
    }
}

function validatePassword(str, info) {
    if (str.length == 0) {
        return '密码不能为空';
    } else if (str.length < 6) {
        return '密码长度不应小于6';
    } else if (!/^[!-~]+$/.test(str)) {
        return '密码应只包含可见ASCII字符';
    } else {
        return '';
    }
}

function validateIDCard(str, info) {
    if (str.length == 0) {
        return '身份证号不能为空';
    } else if (str.length != 18) {
        return '身份证号长度应为18位'
    } else if (/^[0-9]{17}[0-9X]$/.test(str)) {
        return '';
    } else {
        return '身份证号应只包含数字和X';
    }
}

function validateRealname(str, info) {
    if (str.length == 0) {
        return '姓名不能为空';
    } else if (/^[\u4e00-\u9fa5]{1,6}(·[\u4e00-\u9fa5]{1,6}){0,2}([,，][\u4e00-\u9fa5]{1,6}(·[\u4e00-\u9fa5]{1,6}){0,2})*$/.test(str)) {
        return '';
    } else {
        return '姓名应只包含中文和间隔号'
    }
}

function validateEmail(str, info) {
    if (str.length == 0) {
        return '电子邮箱不能为空';
    } else if (str.length > 50) {
        return '电子邮箱地址太长';
    } else if (!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    .test(str)) {
        return '电子邮箱地址非法';
    } else {
        return '';
    }
}

function validateMobile(str, info) {
    if (str.length == 0) {
        return '联系电话不能为空';
    } else if (str.length != 11) {
        return '联系电话长度应为11位'
    } else if (!/^[0-9]{11}$/.test(str)) {
        return '联系电话应只包含数字';
    } else {
        return '';
    }
}

function validateMailcode(str, info) {
    if (str.length == 0) {
        return '邮政编码不能为空';
    } else if (str.length != 6) {
        return '邮政编码长度应为6位'
    } else if (!/^[0-9]{6}$/.test(str)) {
        return '邮政编码只包含数字';
    } else {
        return '';
    }
}

function validateAddress(str, info) {
    if (str.length == 0) {
        return '家庭地址不能为空';
    } else if (str.length < 8) {
        return '家庭地址长度太短';
    } else {
        return '';
    }
}

function validateNotEmpty(str, info) {
    if (str == null || str.length == 0 || str == 0) {
        return info + '不能为空';
    } else {
        return '';
    }
}

function validateIntNotEmpty(str, info) {
    if (str == null || str.length == 0 || str == 0) {
        return info + '不能为空';
    } else if (!/^[0-9]+$/.test(str)) {
        return info + '应为正整数';
    } else {
        return '';
    }
}

function validateScoreAllowEmpty(str, info) {
    if (str == null || str.length == 0) {
        return '';
    }
    
    if (!/^[0-9]+$/.test(str)) {
        return info + '应为正整数';
    } else {
        return '';
    }
}

function validateNumberNotEmpty(str, info) {
    if (str == null || str.length == 0 || str == 0) {
        return info + '不能为空';
    } else if (!/^[+-]?[0-9]+(\.[0-9]+)?$/.test(str)) {
        return info + '应只包含数字';
    } else {
        return '';
    }
}

function validateYear(str, info) {
    if (str == null || str.length == 0 || str == 0) {
        return info + '不能为空';
    } else if (!/^[1-9][0-9]{3}$/.test(str)) {
        return info + '不合法';
    } else if (str < 1900 | str > 2156) {
        return info + '取值范围为 1901-2155';
    } else {
        return ''
    }
}

function validatePermissionName(str) {
    if (str.length == 0) {
        return '权限名称不能为空';
    } else if (/^_(\S)*$/.test(str)) {
        return '权限名称不能以下划线开始';
    } else if (!/^[a-zA-Z0-9][a-zA-Z0-9_]*$/.test(str)) {
        return '权限名称应只包含英文字母、数字和下划线';
    } else if (str.length > 30) {
        return '权限名称长度不应超过30'
    } else {
        return '';
    }
}

function validateURL(str, info = 'URL') {
    if (str.length == 0) {
        return info + '不能为空';
    } else if (!/^(https?):\/\/[^\s/$.?#].[^\s]*$/.test(str)) {
        return info + '不合法';
    } else {
        return '';
    }
}

function validateECJTUID(str, info = ''){
    if (str.length == 0) {
        return info + '不能为空';
    } else if (!/^[!-~]+$/.test(str)) {
        return info + '应只包含可见ASCII字符';
    } else if (str.length >= 6 && str.length != 16) {
        return info + '不合法';
    } else {
        return '';
    }
}