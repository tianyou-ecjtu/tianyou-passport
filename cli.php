<?php
    if (php_sapi_name() != "cli") {
        header("Location:/404");
        exit;
    }

    date_default_timezone_set('PRC');
    
    if (!defined("ABSPATH")) {
        define("ABSPATH", dirname(__FILE__). '/');
    }

    define("load", true);
    define("cli", true);
    
    require_once ABSPATH. '/lib/lib-load.php';
?>