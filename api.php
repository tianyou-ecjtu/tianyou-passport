<?php
    error_reporting(E_ERROR | E_PARSE);
    
    date_default_timezone_set('PRC');

    if (!defined("ABSPATH")) {
        define("ABSPATH", $_SERVER['DOCUMENT_ROOT'] . '/');
    }

    define("load", true);
    define("api", true);

    define("FRAMEWORK_EXCEPTION_FORMAT_JSON", TRUE);
    
    require_once ABSPATH. '/lib/lib-load.php';
?>