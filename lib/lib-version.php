<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $version = [
        "framework" => [
            "version" => frame::getConst("framework_version"),
            "ipdatabase_v4" => frame::getConst("framework_ipdatabase_v4_version"),
            "ipdatabase_v6" => frame::getConst("framework_ipdatabase_v6_version"),
        ],
        "system" => "1.0",
        "composment" => [
            "constanttime" => "2.4.0",
            "enum" => "1.0.3",
            "google2fa" => "8.0.0",
            "phpmailer" => "6.6.3",
            "phpSpreadSheet" => "1.24.1",
            "qrcode" => "2.0.0",
            "simplecache" => "1.0.1"
        ]
    ];
    
    frame::configSet("version", $version);
?>