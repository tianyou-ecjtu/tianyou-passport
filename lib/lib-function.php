<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    /**
     * Get system settings
     * 
     * @param string
     * @return string
     */

    function getSystemVariable($name, $cacheOpt = true){
        if (!$cacheOpt) {
            return db::selectFirst("SELECT * FROM `TABLEPREFIX_variables` where `name` = ?", "s", [$name])["value"];
        }
        
        return Cache::getArray("SystemVariable_{$name}", function($name) {
            return db::selectFirst("SELECT * FROM `TABLEPREFIX_variables` where `name` = ?", "s", [$name])["value"];
        }, [$name]);
    }

    /**
     * Autoload system settings
     * 
     * @param null
     * @return null
     */

    function loadSystemVariables(){
        $query_rows = Cache::getArray("SystemVariables", function() {
            return db::selectAll("SELECT * FROM `TABLEPREFIX_variables` where `autoload` = '1'");
        });

        foreach($query_rows as $query_row){
            frame::configSet($query_row['name'], $query_row['value']);
        }
    }

    /**
     * Update system variable
     *  
     * @param string $name
     * @param string $value
     * @return null
     */

    function updateSystemVariable($name, $value) {
        db::update("UPDATE `TABLEPREFIX_variables` SET `value` = ? where `name` = ?", "ss", [$value, $name]);
        Cache::unset("SystemVariable_{$name}");
    }

    function getCDNLink($link) {
        $cdn_url = frame::configGet("site/cdn");

        if (substr($cdn_url, -1) == "/") {
            return $cdn_url . $link;
        } else {
            return $cdn_url . "/" . $link;
        }
    }

    function getSiteLink($link) {
        $site_url = frame::configGet("site/url");

        if (substr($site_url, -1) == "/") {
            return $site_url . $link;
        } else {
            return $site_url . "/" . $link;
        }
    }

    /**
     * Check CSRF
     * 
     * @return bool
     */

    function checkCSRF() {
        if ($_GET["csrf"] != frame::clientKey() && $_POST["csrf"] != frame::clientKey()) {
            log::writeLog(2, 2, 404, "CSRF-Token 无效");
            return false;
        }
        return true;
    }

    function checkCaptcha() {
        if (!isset($_POST['recaptcha'])) {
            return false;
        }
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.recaptcha.net/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret" => frame::configGet("recaptcha/secret_key"),
            "response" => $_POST['recaptcha']
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($response, true)["success"];
    }

    function redirect($url) {
        header("Location: ".$url);
        exit;
    }

    function invalid($code, $title, $reason) {
        log::writeLog(2, 2, $code, $title, $reason);
        redirect("/404");
    }
    
    function escapeHTML($str) {
        $str = str_replace("\\", "&#92;", $str);
        $str = str_replace('\'', "&#39;", $str);
        $str = str_replace("\"", "&quot;", $str);
        $str = str_replace("<", "&lt;", $str);
        $str = str_replace(">", "&gt;", $str);
        return $str;
    }

    function getParam($name, $method = "POST") {
        if ($method == "POST") {
            $str = escapeHTML($_POST[$name]);
        } else {
            $str = escapeHTML($_GET[$name]);
        }
        
        return db::escape($str);
    }

    function getPageID() {
        $id = $_GET["page"];

        if (empty($id)) {
            return 1;
        } else if (!empty($id) && !is_numeric($id)) {
            log::writeLog(2, 2, 401, "请求非法", "页面 ID 非法");
            return -1;
        }

        return $id;
    }

    function getPageURL($page) {
        $str = "";
        $arr = $_GET;
        $arr["page"] = $page;

        foreach ($arr as $key => $value) {
            if (empty($str)) {
                $str .= "?{$key}={$value}";
            } else {
                $str .= "&{$key}={$value}";
            }
        }

        return $str;
    }

    function validateUsername($username) {
        return is_string($username) && preg_match('/^[a-zA-Z0-9_]{1,30}$/', $username);
    }

    function validatePassword($password) {
        return is_string($password) && strlen($password) >= 6 && preg_match('/^[!-~]+$/', $password);
    }

    function validatePermissionName($name) {
        return is_string($name) && preg_match('/^[a-zA-Z0-9][a-zA-Z0-9_]{0,29}$/', $name);
    }
    
    function validateRealname($realname) {
        return is_string($realname) && (preg_match('/^[\x7f-\xff]+$/', $realname) || preg_match('/^[a-zA-Z0-9_]{1,20}$/', $realname));
    }

    function validateIDCard($idcard) {
        if (!preg_match('/^\d{17}[0-9X]$/', $idcard)) {
            return false;
        }

        $areaNum = substr($idcard, 0, 6);
        $dateNum = substr($idcard, 6, 8);

        $provinceCode = substr($areaNum, 0, 2);
        if ($provinceCode < 10 || $provinceCode > 65) {
            return false;
        }

        $year  = intval(substr($dateNum, 0, 4));
        $month = intval(substr($dateNum, 4, 2));
        $day   = intval(substr($dateNum, 6, 2));

        if (!checkdate($month, $day, $year)) {
            return false;
        }

        $currYear = date('Y');
        if ($year > $currYear) {
            return false;
        }

        $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        $tokens = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];

        $checkSum = 0;
        for ($i = 0; $i < 17; $i++) {
            $checkSum += intval($idcard[$i]) * $factor[$i];
        }

        $mod   = $checkSum % 11;
        $token = $tokens[$mod];

        $lastChar = strtoupper($idcard[17]);

        if ($lastChar != $token) {
            return false;
        }

        return true;
    }

    function validateEmail($email) {
        return is_string($email) && strlen($email) <= 50 && preg_match('/^(.+)@(.+)$/', $email);
    }
    
    function validateMobile($mobile) {
        return is_string($mobile) && strlen($mobile) == 11 && preg_match('/^[0-9]{11}$/', $mobile);
    }

    function validateYear($year) {
        return is_string($year) && strlen($year) == 4 && preg_match('/^[1-9][0-9]{3}$/', $year) && $year >= 1901 && $year <= 2155;
    }

    function validateDatetime($date) {
        if (empty($date)) {
            return true;
        }

        return strtotime(date('c', strtotime($date))) == strtotime($date);
    }

    function validateURL($url) {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    function hideEmail($str) {
        if (strpos($str, '@')) { 
            $email_array = explode("@", $str); 
            $prevfix = (strlen($email_array[0]) < 4) ? "" : substr($str, 0, 3); //邮箱前缀 
            $count = 0; 
            $str = preg_replace('/(.+)@/', '***@', $str, -1, $count); 
            $rs = $prevfix . $str; 
        }
        return $rs; 
    }
?>