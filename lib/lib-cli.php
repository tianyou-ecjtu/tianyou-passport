<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    const TIME_LIMIT = 60;

    ini_set('max_execution_time', TIME_LIMIT);
    set_time_limit(TIME_LIMIT);

    function setInterval($func, $time) {
        $timeStart = hrtime(true) / 1e+6;
        $timeLimitMill = TIME_LIMIT * 1000;
        $funcAvgTime = 0;

        $times = floor($timeLimitMill / $time);

        for ($i=0; $i<$times; $i++) {
            $funcStartTime = hrtime(true) / 1e+6;

            if ($funcStartTime - $timeStart + $funcAvgTime < $timeLimitMill) {
                call_user_func_array($func, []);
            } else {
                return;
            }

            $funcEndTime = hrtime(true) / 1e+6;
            $funcAvgTime = ($funcAvgTime * $i + ($funcEndTime - $funcStartTime)) / ($i + 1);
            
            sleep($time / 1000);
        }
    }

    $handlers = [
        "cron:mail" => function() {
            setInterval(function(){
                mail::cron();
            }, 3000);
        },
    ];

    function printHelpMessage($handlers) {
        print("tianyou-passport Command-Line Interface\n");
        print("Usage : php cli.php [task] [arg...]\n");
        print("\nAvailable Task List: \n");

        foreach ($handlers as $name => $func) {
            print(" - {$name}\n");
        }

        exit;
    }

    $argc = $_SERVER['argc'];
    $argv = $_SERVER['argv'];

    if ($argc <= 1) {
        printHelpMessage($handlers);
    }
    
    if (!isset($handlers[$argv[1]])) {
        printHelpMessage($handlers);
    }

    call_user_func_array($handlers[$argv[1]], array_slice($argv, 2));
?>