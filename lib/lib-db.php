<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class db_private extends db {
        protected static $conn;
        protected static $data;
    }

    class db_public extends db {
        protected static $conn;
        protected static $data;
    }
    
    cache::init();
    
    function db_set($name) {
        $className = "db_{$name}";
        $className::set(frame::configGet("mysql_{$name}/ip"), frame::configGet("mysql_{$name}/username"), frame::configGet("mysql_{$name}/password"), frame::configGet("mysql_{$name}/database"), frame::configGet("mysql_{$name}/port"), frame::configGet("mysql_{$name}/table_prefix"));
    }

    db_set("private");
    db_set("public");
?>