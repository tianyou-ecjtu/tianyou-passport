<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    function classLoaderConstantTime($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('ParagonIE'.DIRECTORY_SEPARATOR.'ConstantTime', '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'constanttime' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
    
    spl_autoload_register('classLoaderConstantTime');
?>