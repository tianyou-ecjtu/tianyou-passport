<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    function classLoaderGoogle2FA($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('BaconQrCode'.DIRECTORY_SEPARATOR, '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
    
    spl_autoload_register('classLoaderGoogle2FA');
?>