<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    function classLoaderPHPSpreadSheet($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('PhpOffice'.DIRECTORY_SEPARATOR, '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'phpSpreadSheet' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
    
    spl_autoload_register('classLoaderPHPSpreadSheet');
?>