<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    function classLoaderSimpleCache($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('Psr'.DIRECTORY_SEPARATOR.'SimpleCache', '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'simplecache' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
    
    spl_autoload_register('classLoaderSimpleCache');
?>