<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    function classLoaderEnum($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('DASPRiD'.DIRECTORY_SEPARATOR.'Enum', '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'enum' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
    
    spl_autoload_register('classLoaderEnum');
?>