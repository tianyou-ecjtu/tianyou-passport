<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }
    
    loadFile("/lib/3rdparty/phpmailer/src/Exception.php");
    loadFile("/lib/3rdparty/phpmailer/src/PHPMailer.php");
    loadFile("/lib/3rdparty/phpmailer/src/SMTP.php");
?>