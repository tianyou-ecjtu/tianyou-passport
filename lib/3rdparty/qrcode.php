<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    load3rdparty("enum");
    
    function classLoaderQrCode($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $path = str_replace('PragmaRX'.DIRECTORY_SEPARATOR.'Google2FA', '', $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'google2fa' . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }

    spl_autoload_register('classLoaderQrCode');
?>