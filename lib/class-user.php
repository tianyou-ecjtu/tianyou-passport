<?php

    /**
     * User Operation
     * 
     * @since 1.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class user {
        private $id = null;
        private $username = null;
        private $userinfo = [];
        private $basicinfo = [];
        private $authBySession = true;
        private $waiting2FA = false;

        private function clear() {
            frame::deleteCookie("login");

            frame::deleteSession("uid");
            frame::deleteSession("username");
            frame::deleteSession("waiting_2fa_".$this->id);

            $this->id = null;
            $this->username = null;
            $this->userinfo = [];
            $this->authBySession = true;
            $this->waiting2FA = false;
        }

        private function cacheUser($uid) {
            return Cache::getArray("User_{$uid}", function($uid) {
                return db::selectFirst("SELECT * FROM `TABLEPREFIX_users` where `id` = ?", "i", [$uid]);
            },array($uid));
        }

        private function cacheUserInfo($uid) {
            return Cache::getArray("UserInfo_{$uid}", function($uid) {
                $userinfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_userinfo` where `uid` = ?", "i", [$uid]);
                if (empty($userinfo["realname"]) && !empty($userinfo["ecjtuID"])) {
                    $presets = db::selectFirst("SELECT * FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` = ?", "s", [$userinfo["ecjtuID"]]);
                    if (!empty($presets["description"])) {
                        $userinfo["realname"] = $presets["description"];
                    }
                }
                return $userinfo;
            }, array($uid));
        }

        private function cacheUserGroup($uid) {
            return Cache::getArray("UserGroup_{$uid}", function($uid) {
                $ecjtuID = $this->getUserInfo()["ecjtuID"];

                $groupPreset = db::selectAll("SELECT `gid` FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` = ?", "s", [$ecjtuID]);
                $groupAssign = db::selectAll("SELECT `gid` FROM `TABLEPREFIX_user_group` where `uid` = ?", "i", [$uid]);

                $groups = [];
                $ids = [];

                foreach ($groupPreset as $group) {
                    $id = $group["gid"];

                    if (!array_key_exists($id, $ids)) {
                        $ids[$id] = '';
                        array_push($groups, ["gid" => $id]);
                    }
                }

                foreach ($groupAssign as $group) {
                    $id = $group["gid"];

                    if (!array_key_exists($id, $ids)) {
                        $ids[$id] = '';
                        array_push($groups, ["gid" => $id]);
                    }
                }
                
                return $groups;
            }, [$uid]);
        }

        private function cacheUserGroupPermission($gid, $cid) {
            return Cache::getArray("UserGroupPermission_{$gid}_{$cid}", function($gid, $cid) {
                $permission = [];
                $permissionGroupInfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_permission_group` where `gid` = ? and `cid` = ?", "ii", [$gid, $cid]);

                if (!empty($permissionGroupInfo)) {
                    $permissionGroup = json_decode($permissionGroupInfo["permission"], true);

                    foreach ($permissionGroup as $permissionGroupSingle => $permissionGroupSingleVal) {
                        $permission[$permissionGroupSingle] = true;
                    }
                }

                return $permission;
            }, [$gid, $cid]);
        }

        private function cacheUserPermission($uid, $cid) {
            return Cache::getArray("UserPermission_{$uid}_{$cid}", function($uid, $cid) {
                $grantPermission = [];
                $revokePermission = [];

                $permissionUserInfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_permission_user` where `uid` = ? and `cid` = ?", "ii", [$uid, $cid]);

                if (!empty($permissionUserInfo)) {
                    $permissionUser = json_decode($permissionUserInfo["permission"], true);

                    foreach ($permissionUser as $permissionUserSingle => $permissionUserSingleVal) {
                        if ($permissionUserSingleVal == 1) {
                            $grantPermission[$permissionUserSingle] = true;
                        } else if ($permissionUserSingleVal == -1) {
                            $revokePermission[$permissionUserSingle] = true;
                        }
                    }
                }

                return ["grant" => $grantPermission, "revoke" => $revokePermission];
            }, [$uid, $cid]);
        }

        private static function cacheUserGroupInfo($gid) {
            return Cache::getArray("UserGroupInfo_{$gid}", function($gid) {
                return db::selectFirst("SELECT * FROM `TABLEPREFIX_groups` where `id` = ?", "i", [$gid]);
            },array($gid));
        }

        public static function cacheUserUpdate($uid) {
            Cache::unset("User_{$uid}");
        }

        public static function cacheUserInfoUpdate($uid) {
            Cache::unset("UserInfo_{$uid}");
        }

        public static function cacheUserGroupUpdate($uid) {
            Cache::unset("UserGroup_{$uid}");
        }
        
        public static function cacheUserGroupInfoUpdate($gid) {
            Cache::unset("UserGroupInfo_{$gid}");
        }

        public static function cacheUserGroupPermissionUpdate($gid, $cid) {
            Cache::unset("UserGroupPermission_{$gid}_{$cid}");
        }

        public static function cacheUserGroupPermissionUpdateAll($gid) {
            Cache::unsetPattern("UserGroupPermission_{$gid}_*");
        }

        public static function cacheUserPermissionUpdate($uid, $cid) {
            Cache::unset("UserPermission_{$uid}_{$cid}");
        }

        public static function cacheUserPermissionUpdateAll($uid) {
            Cache::unsetPattern("UserPermission_{$uid}_*");
        }

        public static function cacheClientPermissionUpdateAll($cid) {
            Cache::unsetPattern("UserPermission_*_{$cid}");
            Cache::unsetPattern("UserGroupPermission_*_{$cid}");
        }

        function __construct($uid = "", $username = "") {
            $constructByUID = !empty($uid) && is_numeric($uid);
            $constructByUsername = !empty($username) && validateUsername($username);

            if (!$constructByUID && !$constructByUsername) {
                if (frame::issetCookie("login") && frame::issetSession("uid")) {
                    $this->id = frame::readSession("uid");
                    $this->username = frame::readSession("username");
                } else {
                    return;
                }
            } else {
                if (!$constructByUID && $constructByUsername) {
                    $uid = Cache::getString("UserByUsername_{$username}", function($username) {
                        return db::selectFirst("SELECT `id` FROM `TABLEPREFIX_users` where `username` = ? LIMIT 1", "s", [$username])["id"];
                    }, [$username]);
                }

                $this->id = $uid;
                $this->authBySession = false;
            }

            $user = $this->cacheUser($this->id);
            $userinfo = $this->cacheUserInfo($this->id);

            if ($this->authBySession == false) {
                if (count($user)) {
                    $this->username = $user["username"];
                } else {
                    $this->id = $this->username = null;
                    return;
                }
            }

            if (frame::readSession("waiting_2fa_".$this->id)) {
                $this->waiting2FA = true;
            }

            $this->basicinfo = $userinfo;
            $this->userinfo = array_merge(
                (array)$user,
                (array)$userinfo
            );
        }

        public function check() {
            if ($this->id == null) {
                return false;
            }
            return true;
        }

        public function checkAuth() {
            if ($this->checkExist() && !$this->isBlock() && $this->authBySession) {
                return true;
            }
            return false;
        }

        public function checkExist() {
            if ($this->check() && !$this->isDelete()) {
                return true;
            }
            return false;
        }

        public function checkLogin() {
            if ($this->checkAuth() && $this->userinfo["isLogin"] == true && !$this->isWaiting2FA()) {
                return true;
            }
            return false;
        }

        public function getUsername() {
            return $this->username;
        }

        public function getID() {
            return $this->id;
        }

        public function getBasicInfo() {
            return $this->basicinfo;
        }

        public function getUserInfo() {
            return $this->userinfo;
        }

        public function getUserGroup() {
            return $this->cacheUserGroup($this->id);
        }

        public function getPermission($cid) {
            $permission = [];
            $groups = $this->getUserGroup();

            foreach ($groups as $group) {
                $permission = array_merge($permission, $this->cacheUserGroupPermission($group["gid"], $cid));
            }

            $userPermission = $this->cacheUserPermission($this->id, $cid);
            $permission = array_merge($permission, $userPermission["grant"]);

            foreach ($userPermission["revoke"] as $permissionRevokeName => $permissionRevokeVal) {
                unset($permission[$permissionRevokeName]);
            }

            return $permission;  
        }

        public static function getUserGroupInfo($gid) {
            return self::cacheUserGroupInfo($gid);
        }
        
        public static function getUserGroupAssignCnt($gid) {
            return db::num_rows("SELECT `id` FROM `TABLEPREFIX_user_group` where `gid` = ?", "i", [$gid]);
        }

        public static function getUserGroupPresetCnt($gid) {
            return db::num_rows("SELECT `id` FROM `TABLEPREFIX_user_group_preset` where `gid` = ?", "i", [$gid]);
        }

        public function get2FA() {
            return json_decode(frame::twoWayDecryption($this->userinfo["google2fa"], $this->userinfo["password"], $this->userinfo["salt"]), true);
        }

        public function getRecentLogin() {
            return db::selectAll("SELECT * FROM `TABLEPREFIX_logs_login` where `uid` = ? ORDER BY `date` DESC LIMIT 5", "i", [$this->id]);
        }

        public function getBindInfo() {
            return db::selectFirst("SELECT * FROM `TABLEPREFIX_ecjtu_bind` where `uid` = ?", "i", [$this->getID()]);
        }

        public function isAdmin() {
            return $this->userinfo["isAdmin"];
        }

        public function isBlock() {
            return $this->userinfo["isBlock"] == 1;
        }

        public function isDelete() {
            return $this->userinfo["isBlock"] == -1;
        }

        public function isVerifyEmail() {
            return empty($this->userinfo["emailUnverify"]) && $this->isVerifyEmailExist();
        }

        public function isVerifyEmailExist() {
            return !empty($this->userinfo["email"]);
        }

        public function isInfoComplete() {
            return $this->userinfo["isInfoComplete"];
        }

        public function isVerify() {
            return $this->isVerifyEmail() && $this->isInfoComplete();
        }

        public function isEnable2FA() {
            return !empty($this->userinfo["google2fa"]);
        }

        public function isWaiting2FA() {
            return $this->isEnable2FA() && $this->waiting2FA;
        }

        public function isBindEcjtu() {
            return !empty($this->userinfo["ecjtuID"]);
        }

        public function update2FA($secret, $timestamp) {
            $data = [ 
                "secret" => $secret,
                "timestamp" => $timestamp
            ];

            $result = frame::twoWayEncryption(json_encode($data), $this->userinfo["password"], $this->userinfo["salt"]);

            db::update("UPDATE `TABLEPREFIX_users` SET `google2fa` = ? where `id` = '$this->id'", "s", [$result]);
            self::cacheUserUpdate($this->id);
        }

        public function validate2FA($cid = 1) {
            $this->isWaiting2FA = false;
            frame::deleteSession("waiting_2fa_".$this->id);

            db::insert("INSERT INTO `TABLEPREFIX_logs_login` (`uid`, `date`, `cid`, `ip`) VALUES (?, ?, ?, ?)", "isis", [$this->id, date("Y-m-d H:i:s"), $cid, frame::getIP()]);
            db::update("UPDATE `TABLEPREFIX_userinfo` SET `isLogin` = 1 where `uid` = ?", "i", [$this->id]);
            self::cacheUserInfoUpdate($this->id);
        }

        public function checkPassword($password) {
            return frame::oneWayEncryption($password, $this->userinfo["salt"]) == $this->userinfo["password"];
        }

        public function auth($user, $pass, $cid = 1) {
            if (empty($user) || empty($pass) || !validateUsername($user) || !validatePassword($pass)) {
                return false;
            }
            
            $user = db::selectFirst("SELECT * FROM `TABLEPREFIX_users` where `username`= ?", "s", [$user]);
            $passwordCipher = frame::oneWayEncryption($pass, $user["salt"]);

            if ($passwordCipher == $user["password"]) {
                frame::createSession("uid", $user["id"]);
                frame::createSession("username", $user["username"]);

                $date = date("Y-m-d H:i:s");
                $ip = frame::getIP();

                $this->id = $user["id"];
                $this->username = $user["username"];
                $this->userinfo = $user;
                $this->userinfo["lastLoginTime"] = $date;
                $this->userinfo["lastLoginIP"] = $ip;

                if ($this->isBlock()) {
                    return true;
                }

                if ($this->isEnable2FA()) {
                    define("LOG_OVERRIDE_UID", $this->id);
                    frame::createSession("waiting_2fa_".$this->id, true);
                    db::update("UPDATE `TABLEPREFIX_users` SET `lastLoginTime` = ?, `lastLoginIP` = ? where `id` = ?", "ssi", [$date, $ip, $this->id]);

                    if ($user["isAdmin"]) {
                        log::writeLog(2, 3, 211, "管理员登录 - 等待2FA");
                    } else {
                        log::writeLog(2, 3, 201, "用户登录 - 等待2FA");
                    }
                } else {
                    db::insert("INSERT INTO `TABLEPREFIX_logs_login` (`uid`, `date`, `cid`, `ip`) VALUES (?, ?, ?, ?)", "isis", [$this->id, $date, $cid, $ip]);
                    db::update("UPDATE `TABLEPREFIX_users` SET `lastLoginTime` = ?, `lastLoginIP` = ? where `id` = ?", "ssi", [$date, $ip, $this->id]);
                    db::update("UPDATE `TABLEPREFIX_userinfo` SET `isLogin` = 1 where `uid` = ?", "i", [$this->id]);

                    if ($user["isAdmin"]) {
                        log::writeLog(2, 3, 211, "管理员登录");
                    } else {
                        log::writeLog(2, 3, 201, "用户登录");
                    }
                }

                self::cacheUserUpdate($this->id);
                self::cacheUserInfoUpdate($this->id);
            } else {
                $this->logout();
            }
        }

        public function remoteAuth($user, $pass, $cid) {
            if (empty($user) || empty($pass) || !validateUsername($user) || !validatePassword($pass)) {
                return false;
            }
            
            $user = db::selectFirst("SELECT * FROM `TABLEPREFIX_users` where `username`= ?", "s", [$user]);
            $passwordCipher = frame::oneWayEncryption($pass, $user["salt"]);

            if ($passwordCipher == $user["password"]) {
                $date = date("Y-m-d H:i:s");
                $ip = frame::getIP();

                $this->id = $user["id"];
                $this->username = $user["username"];
                $this->userinfo = $user;
                $this->userinfo["lastLoginTime"] = $date;
                $this->userinfo["lastLoginIP"] = $ip;

                if ($this->isBlock()) {
                    return true;
                }

                if ($this->isEnable2FA()) {
                    define("LOG_OVERRIDE_UID", $this->id);
                    db::update("UPDATE `TABLEPREFIX_users` SET `lastLoginTime` = ?, `lastLoginIP` = ? where `id` = ?", "ssi", [$date, $ip, $this->id]);

                    if ($user["isAdmin"]) {
                        log::writeLog(2, 3, 211, "管理员登录 - 等待2FA");
                    } else {
                        log::writeLog(2, 3, 201, "用户登录 - 等待2FA");
                    }
                } else {
                    db::insert("INSERT INTO `TABLEPREFIX_logs_login` (`uid`, `date`, `cid`, `ip`) VALUES (?, ?, ?, ?)", "isis", [$this->id, $date, $cid, $ip]);
                    db::update("UPDATE `TABLEPREFIX_users` SET `lastLoginTime` = ?, `lastLoginIP` = ? where `id` = ?", "ssi", [$date, $ip, $this->id]);
                    db::update("UPDATE `TABLEPREFIX_userinfo` SET `isLogin` = 1 where `uid` = ?", "i", [$this->id]);

                    if ($user["isAdmin"]) {
                        log::writeLog(2, 3, 211, "管理员登录");
                    } else {
                        log::writeLog(2, 3, 201, "用户登录");
                    }
                }
                
                self::cacheUserUpdate($this->id);
                self::cacheUserInfoUpdate($this->id);
            }

            return false;
        }

        public function setLoginStatus($save) {
            frame::createCookie("login", true, $save ? 7 * 24 * 3600 : 0);
        }

        public function logout() {
            db::update("UPDATE `TABLEPREFIX_userinfo` SET `isLogin` = 0 where `uid` = ?", "i", [$this->id]);
            self::cacheUserInfoUpdate($this->id);

            $this->clear();
        }

        public function sendAuthEmail($email) {
            $token = frame::randString(32);
            $expire = time() + 12 * 3600;
            $data = ["uid" => $this->getID(), "username" => $this->getUsername(), "token" => $token];

            mail::queue($email, "auth_email", frame::configGet("site/name")."邮箱验证", $data);
            db::update("update `TABLEPREFIX_users` SET `emailAuthToken` = ?, `emailAuthExpire` = ? where `id` = ?", "sii", [$token, $expire, $this->getID()]);
        }

        public function updateProfile($realname, $idcard, $email, $mobile, $year) {
            $sex = substr($idcard, 16, 1) % 2;
            $birth = substr($idcard, 6, 4)."-".substr($idcard, 10, 2)."-".substr($idcard, 12, 2);

            db::update("UPDATE `TABLEPREFIX_userinfo` SET `realname` = ?, `idcard` = ?, `sex` = ?, `birthday`= ?, `mobile` = ?, `year` = ?, `isInfoComplete` = '1' where `uid` = ?", "ssissii", [$realname, $idcard, $sex, $birth, $mobile, $year, $this->id]);
            
            if ($email != $this->userinfo["emailUnverify"] && $email != $this->userinfo["email"]) {
                $this->sendAuthEmail($email);
            }

            if ($email != $this->userinfo["email"]) {
                db::update("UPDATE `TABLEPREFIX_userinfo` SET `emailUnverify` = ? where `uid` = ?", "si", [$email, $this->id]);
            } else {
                db::update("UPDATE `TABLEPREFIX_userinfo` SET `emailUnverify` = null where `uid` = ?", "i", [$this->id]);
            }

            self::cacheUserInfoUpdate($this->id);
        }

        public function forgetPassword() {
            $token = frame::randString(32);
            $expire = time() + 12 * 3600;
    
            db::update("update `TABLEPREFIX_users` SET `forgetPasswordToken` = ?, `forgetPasswordExpire` = ? where `id` = ?", "sii", [$token, $expire, $this->getID()]);

            $data = ["uid" => $this->getID(), "username" => $this->getUsername(), "token" => $token];
            mail::queue($this->getUserinfo()["email"], "forget_password", frame::configGet("site/name")."找回账号", $data);
            
            self::cacheUserInfoUpdate($this->id);
        }

        public function updatePassword($password) {
            $salt = frame::randString(128);
            $passwordCipher = frame::oneWayEncryption($password, $salt);

            db::update("UPDATE `TABLEPREFIX_users` SET `salt` = '$salt', `password` = '$passwordCipher', `google2fa` = NULL where `id` = '$this->id'");
            self::cacheUserUpdate($this->id);

            if ($this->isAdmin()) {
                log::writeLog(2, 3, 212, "修改管理员密码");
            } else {
                log::writeLog(2, 3, 202, "修改用户密码");
            }
        }

        public function bindECJTU($username, $password) {
            $bindInfo = $this->getBindInfo();

            if ($bindInfo["status"] == 1) {
                return;
            }

            $salt = frame::randString(16);

            if (empty($bindInfo)) {
                db::insert("INSERT INTO `TABLEPREFIX_ecjtu_bind` (`uid`, `ecjtuID`, `data`, `salt`) VALUES (?, ?, ?, ?)", "isss", [$this->id, $username, frame::twoWayEncryption($password, $salt, $salt), $salt]);
            } else {
                db::update("UPDATE `TABLEPREFIX_ecjtu_bind` SET `ecjtuID` = ?, `data` = ?, `salt` = ?, `status` = 1 where `uid` = ?", "sssi", [$username, frame::twoWayEncryption($password, $salt, $salt), $salt, $this->id]);
            }
        }

        public function resetProfile() {
            db::update("UPDATE `TABLEPREFIX_userinfo` SET `realname` = null, `idcard` = null, `ecjtuID` = null, `sex` = null, `birthday`= null, `email` = null, `emailUnverify` = null, `mobile` = null, `isInfoComplete` = '0' where `uid` = ?", "i", [$this->id]);
            
            self::cacheUserInfoUpdate($this->id);
        }

        public function getForgetToken() {
            $uid = $this->id;
            $token = frame::randString(32);
            $expire = time() + 12 * 3600;
    
            db::update("update `TABLEPREFIX_users` SET `forgetPasswordToken` = ?, `forgetPasswordExpire` = ? where `id` = ?", "sii", [$token, $expire, $uid]);

            return $token;
        }

        public static function move($old, $new) {
            $old_user = new user($old);
            $new_user = new user($new);

            if (!$old_user->checkExist() || !$new_user->checkExist()) {
                return;
            }

            $ecjtuID = $new_user->getUserInfo()["ecjtuID"];

            db::update("UPDATE `TABLEPREFIX_users` SET `isBlock` = ? where `id` = ?", "ii", [$new_user->isBlock(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_api_auth` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_ecjtu_bind` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_logs` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_logs_login` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_permission_user` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_user_group` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);
            db::update("UPDATE `passport_logs_login` SET `uid` = ? where `uid` = ?", "ii", [$new_user->getID(), $old_user->getID()]);

            db::delete("DELETE FROM `TABLEPREFIX_userinfo` where `uid` = ?", "i", [$new_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_userinfo` SET `uid` = ?, `ecjtuID` = ? where `uid` = ?", "isi", [$new_user->getID(), $ecjtuID, $old_user->getID()]);

            db::delete("DELETE FROM `TABLEPREFIX_users` where `id` = ?", "i", [$new_user->getID()]);
            db::update("UPDATE `TABLEPREFIX_users` SET `id` = ? where `id` = ?", "ii", [$new_user->getID(), $old_user->getID()]);

            self::cacheUserUpdate($old_user->getID());
            self::cacheUserInfoUpdate($old_user->getID());
            self::cacheUserGroupUpdate($old_user->getID());
            self::cacheUserPermissionUpdateAll($old_user->getID());

            self::cacheUserUpdate($new_user->getID());
            self::cacheUserInfoUpdate($new_user->getID());
            self::cacheUserGroupUpdate($new_user->getID());
            self::cacheUserPermissionUpdateAll($new_user->getID());
        }

        public static function searchUser($keyword, array $options = [], bool $fuzzy = TRUE) {
            $support_option = ["realname", "idcard", "mobile", "email", "username", "ecjtuID"];
            
            if (empty($options)) {
                $options = $support_option;
            }

            $firstOptionFlag = true;

            $sql = "SELECT * FROM `TABLEPREFIX_userinfo` where ";
            $argc = "";
            $argv = [];

            foreach ($options as $option) {
                if (!in_array($option, $support_option)) {
                    return false;
                }

                $argc .= "s";

                if (!$firstOptionFlag) {
                    $sql .= " or ";
                }

                if ($fuzzy) {
                    $sql .= "{$option} like ?";
                    array_push($argv, "%{$keyword}%");
                } else {
                    $sql .= "{$option} = ?";
                    array_push($argv, $keyword);
                }

                $firstOptionFlag = false;
            }

            return db::selectAll($sql . " ORDER BY `uid` DESC LIMIT 10 ", $argc, $argv);
        }

        public static function searchGroup($keyword, bool $fuzzy = true) {
            if ($fuzzy) {
                return db::selectAll("SELECT * FROM `TABLEPREFIX_groups` where `name` like ?", "s", ["%{$keyword}%"]);
            } else {
                return db::selectAll("SELECT * FROM `TABLEPREFIX_groups` where `name` = ?", "s", [$keyword]);
            }
        }

        public function checkCallback($callback) {
            if (frame::issetSession("auth_{$callback}_callback")) {
                $token = frame::readSession("auth_{$callback}_callback_token");

                db::update("UPDATE `TABLEPREFIX_api_auth` SET `uid` = ?, `username` = ?, `status` = ? where `token` = ?", "isis", [$this->getID(), $this->getUsername(), 1, $token]);

                $url = sprintf("%s?token=%s", frame::readSession("auth_{$callback}_callback"), $token);

                frame::deleteSession("auth_{$callback}_callback");
                frame::deleteSession("auth_{$callback}_callback_cid");
                frame::deleteSession("auth_{$callback}_callback_name");
                frame::deleteSession("auth_{$callback}_callback_token");
                
                exit("<script>sessionStorage.removeItem('callback'); sessionStorage.removeItem('callbackName'); window.location.href = '{$url}';</script>");
            }
        }
    }
?>