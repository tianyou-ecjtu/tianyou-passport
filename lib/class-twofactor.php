<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    load3rdparty("constanttime");
    load3rdparty("qrcode");
    load3rdparty("google2fa");
    
    use PragmaRX\Google2FA\Google2FA;
    use BaconQrCode\Renderer\ImageRenderer;
    use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
    use BaconQrCode\Renderer\RendererStyle\RendererStyle;
    use BaconQrCode\Writer;
    
    class twofactor {
        private $google2fa = null;
        private $user = null;
        private $secret = null;
        private $timestamp = null;

        public function __construct($user) {
            $this->google2fa = new Google2FA();
            $this->user = $user;

            if (!$this->user->isEnable2FA()) {
                if (frame::issetSession("2fa")) {
                    $this->secret = frame::readSession("2fa");
                } else {
                    $this->secret = $this->generateSecretKey();
                }
            } else {
                $data = $this->user->get2FA();
                $this->secret = $data["secret"];
                $this->timestamp = $data["timestamp"];
            }
        }

        private function generateSecretKey() {
            return $this->google2fa->generateSecretKey();
        }

        public function getQRCode($sitename, $username) {
            frame::createSession("2fa", $this->secret);

            $g2faUrl = $this->google2fa->getQRCodeUrl(
                $sitename,
                $username,
                $this->secret
            );
            
            $writer = new Writer(
                new ImageRenderer(
                    new RendererStyle(400),
                    new ImagickImageBackEnd()
                )
            );
            
            return base64_encode($writer->writeString($g2faUrl));
        }

        public function validate($code, $callback_cid = 1) {
            $timestamp = $this->google2fa->verifyKeyNewer($this->secret, $code, $this->timestamp);

            if ($timestamp !== false) {
                $this->user->update2FA($this->secret, $timestamp);
                $this->user->validate2FA($callback_cid);
                log::writeLog(2, 3, 308, "双因素验证通过");
                return true;
            } else {
                return false;
            }
        }

        public function setup($code) {
            $secret = frame::readSession("2fa");
            $timestamp = $this->google2fa->verifyKeyNewer($secret, $code, $this->timestamp);

            if ($timestamp !== false) {
                frame::deleteSession("2fa");
                $this->user->update2FA($this->secret, $timestamp);
                log::writeLog(2, 3, 307, "设置双因素验证");
                return true;
            } else {
                return false;
            }
        }
    }
?>