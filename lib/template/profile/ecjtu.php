<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if ($_POST["ecjtu"] == "ecjtu") {
        if (!checkCSRF()) {
            exit("expired");
        }

        $username = getParam("username");
        $password = getParam("password");

        if (empty($username) || empty($password) || (strlen($username) > 6 && strlen($username) != 16)) {
            exit();
        }
        
        if (db::num_rows("SELECT `uid` FROM `TABLEPREFIX_userinfo` where `ecjtuID` = ? and `isThirdpart` = 0", "s", [$username])) {
            exit("username");
        }

        $user->bindECJTU($username, $password);
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets['profile'] = '';
        return true;
    }

    define("LEFTBAR_DISPLAY_HOME", TRUE);

    /**
     * Status 0: Already bind
     * Status 1: Waiting for result
     * Status 2: Wrong username or password
     * Status 3: System Error
     * Status 4: No result
     */

    $isBindEcjtu = $user->isBindEcjtu();
    $bindInfo = $user->getBindInfo();
    $bindStatus = 4;

    if ($isBindEcjtu) {
        $bindStatus = 0;
        $ecjtuID = $user->getUserInfo()["ecjtuID"];
    } else {
        if (!empty($bindInfo)) {
            $bindStatus = $bindInfo["status"];
            $ecjtuID = $bindInfo["ecjtuID"];
        }
    }
?>

<div class="container">
    <div class="main-body">
          <div class="row gutters-sm mt-5">
            <?php route::include("template/profile/leftbar") ?>
            
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <h6 class="d-flex align-items-center mb-4 card-title">智慧交大账号绑定</h6>
                        <?php if ($bindStatus == 0): ?>
                        <div class="alert alert-success mb-4" role="alert">智慧交大账号绑定已完成</div>
                        <?php elseif ($bindStatus == 1): ?>
                        <div class="alert alert-warning mb-4" role="alert">正在进行验证，请稍候</div>
                        <script> setTimeout(function(){location.reload();},3000); </script>
                        <?php elseif ($bindStatus == 2): ?>
                        <div class="alert alert-danger mb-4" role="alert">学工号或密码错误，请重试</div>
                        <?php elseif ($bindStatus == 3): ?>
                        <div class="alert alert-danger mb-4" role="alert">系统错误，请重试</div>
                        <?php endif ?>               
                        <form id="form-ecjtu" action="#">
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">学工号</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="username" class="form-control" placeholder="请输入智慧交大学工号" value="<?= $ecjtuID ?>" id="input-username" <?= $bindStatus <= 1 ? "disabled" : NULL ?>>
                                    <div class="invalid-feedback" id="help-username"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">密码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="password" class="form-control" placeholder="请输入智慧交大密码" value="" id="input-password" <?= $bindStatus <= 1 ? "disabled" : NULL ?>>
                                    <div class="invalid-feedback" id="help-password"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="submit" class="btn btn-primary px-4" value="提交" <?= $bindStatus <= 1 ? "disabled" : NULL ?>>
                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            function validatePasswordPost() {
                                var ok = true;
                                ok &= getFormErrorAndShowHelp('username', validateECJTUID, "学工号");
                                ok &= getFormErrorAndShowHelp('password', validateNotEmpty, "密码");
                                return ok;
                            }

                            function submitPasswordPost() {
                                if (!validatePasswordPost()) {
                                    return false;
                                }

                                $.post('/ecjtu', {
                                    csrf : "<?= frame::clientKey() ?>",
                                    username : $('#input-username').val(),
                                    password : $('#input-password').val(),
                                    ecjtu : "ecjtu"
                                }, function(msg) {
                                    if (msg == 'ok') {
                                        location.reload();
                                    } else if (msg == 'expired') {
                                        showErrorHelp("username", "页面会话已过期");
                                    } else if (msg == 'username') {
                                        showErrorHelp("username", "学工号已被绑定");
                                    } else {
                                        showErrorHelp("username", "未知错误");
                                    }
                                });

                                return true;
                            }

                            $(document).ready(function() {
                                $('#form-ecjtu').submit(function(e) {
                                    e.preventDefault();
                                    submitPasswordPost();
                                });
                            });
                        </script>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>