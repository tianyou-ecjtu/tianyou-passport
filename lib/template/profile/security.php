<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];
    $google2fa = new twofactor($user);

    if ($_POST["security"] == "password") {
        if (!checkCSRF()) {
            exit("expired");
        }

        $oldpassword = getParam("oldpassword");
        $newpassword = getParam("newpassword");
        $newpassword_repeat = getParam("newpassword_repeat");

        if (!validatePassword($oldpassword) || !validatePassword($newpassword)) {
            exit("");
        }
        
        if ($newpassword != $newpassword_repeat) {
            exit("newpassword");
        }

        if ($oldpassword == $newpassword) {
            exit("same");
        }

        if (!$user->checkPassword($oldpassword)) {
            exit("oldpassword");
        }

        $user->updatePassword($newpassword);
        exit("ok");
    } else if ($_POST["security"] == "2fa") {
        if (!checkCSRF()) {
            exit("expired");
        }

        $code = getParam("code");    
        
        if (empty($code) || !is_numeric($code)) {
            exit("");
        }

        if (strlen($code) != 6 || !$google2fa->setup($code)) {
            exit("code");
        } else {
            log::writeLog(2, 3, 222, "设置两步验证");
            exit("ok");
        }
    }

    if($assetsFlag == 1){
        $assets['profile'] = '';
        return true;
    }

    define("LEFTBAR_DISPLAY_HOME", TRUE);
?>

<div class="container">
    <div class="main-body">
          <div class="row gutters-sm mt-5">
            <?php route::include("template/profile/leftbar") ?>
            
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-4 card-title">修改密码</h6>
                        <form id="form-password" action="#">
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">旧密码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="password" class="form-control" placeholder="请输入您的旧密码" value="" id="input-oldpassword">
                                    <div class="invalid-feedback" id="help-oldpassword"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">新密码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="password" class="form-control" placeholder="请输入您的新密码" value="" id="input-newpassword">
                                    <div class="invalid-feedback" id="help-newpassword"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">确认密码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="password" class="form-control" placeholder="请再次输入您的新密码" value="" id="input-newpassword-repeat">
                                    <div class="invalid-feedback" id="help-newpassword-repeat"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="submit" class="btn btn-primary px-4" value="提交">
                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            function validatePasswordPost() {
                                var ok = true;
                                ok &= getFormErrorAndShowHelp('oldpassword', validatePassword);
                                ok &= getFormErrorAndShowHelp('newpassword', validatePassword);
                                ok &= getFormErrorAndShowHelp('newpassword-repeat', validatePassword);
                                return ok;
                            }

                            function submitPasswordPost() {
                                if (!validatePasswordPost()) {
                                    return false;
                                }

                                $.post('/security', {
                                    csrf : "<?= frame::clientKey() ?>",
                                    oldpassword : $('#input-oldpassword').val(),
                                    newpassword : $('#input-newpassword').val(),
                                    newpassword_repeat : $('#input-newpassword-repeat').val(),
                                    security : "password"
                                }, function(msg) {
                                    if (msg == 'ok') {
                                        window.location.href="/logout";
                                    } else if (msg == 'expired') {
                                        showErrorHelp("oldpassword", "页面会话已过期");
                                    } else if (msg == 'oldpassword') {
                                        showErrorHelp("oldpassword", "密码不正确");
                                    } else if (msg == 'newpassword') {
                                        showErrorHelp("newpassword", "两次密码输入不相同");
                                        showErrorHelp("newpassword-repeat", "两次密码输入不相同");
                                    } else if (msg == 'same') {
                                        showErrorHelp("newpassword", "新密码和旧密码相同");
                                        showErrorHelp("newpassword-repeat", "新密码和旧密码相同");
                                    } else {
                                        showErrorHelp("oldpassword", "未知错误");
                                    }
                                });

                                return true;
                            }

                            $(document).ready(function() {
                                $('#form-password').submit(function(e) {
                                    e.preventDefault();
                                    submitPasswordPost();
                                });
                            });
                        </script>
                    </div>
                </div>

              <div class="row gutters-sm">
                <div class="col-sm-12">
                  <div class="card h-100">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-4 card-title">双因素验证 2FA</h6>
                      <?php if (!$user->isEnable2FA()): ?>
                        <form id="form-code" action="#">
                            <div class="row mb-4" style="text-align: center; ">
                                <img src="data:image/png;base64, <?= $google2fa->getQRCode(frame::configGet("site/name"), $user->getUsername()); ?>" style="width: 25%; margin: 0 auto;"/>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">验证码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="text" class="form-control" placeholder="使用2FA软件扫描二维码后输入6位OTP验证码" value="" id="input-code">
                                    <div class="invalid-feedback" id="help-code"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="submit" class="btn btn-primary px-4" value="提交">
                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            function validate2FAPost() {
                                var ok = true;
                                ok &= getFormErrorAndShowHelp('code', validateIntNotEmpty, "OTP验证码");
                                return ok;
                            }

                            function submit2FAPost() {
                                if (!validate2FAPost()) {
                                    return false;
                                }

                                $.post('/security', {
                                    csrf : "<?= frame::clientKey() ?>",
                                    code : $('#input-code').val(),
                                    security : "2fa"
                                }, function(msg) {
                                    if (msg == 'ok') {
                                        window.location.reload();
                                    } else if (msg == 'expired') {
                                        showErrorHelp("code", "页面会话已过期");
                                    } else if (msg == 'code') {
                                        showErrorHelp("code", "验证码不正确");
                                    } else {
                                        showErrorHelp("code", "未知错误");
                                    }
                                });

                                return true;
                            }

                            $(document).ready(function() {
                                $('#form-code').submit(function(e) {
                                    e.preventDefault();
                                    submit2FAPost();
                                });
                            });
                        </script>
                      <?php else: ?>
                        <div class="mt-3">
                            <p>双因素验证 2FA 已启用</p>
                            <p>登录时需要使用 Google Authentication 令牌生成器生成 6 位 OTP 验证码进行二次验证 </p>
                            <p>若 Google Authentication 设备丢失，或需解绑、更换双因素验证设备，请修改密码后重新绑定 2FA</p>
                            <p>修改密码或忘记密码后双因素验证将自动失效</p>
                        </div>
                      <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>