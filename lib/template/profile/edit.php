<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if ($_POST["profile"] == "edit") {
        if (!checkCSRF()) {
            exit("expired");
        }

        $realname = getParam("realname");
        $idcard = getParam("idcard");
        $email = getParam("email");
        $mobile = getParam("mobile");
        $year = getParam("year");
        $password = getParam("password");

        if (!validateRealname($realname) || !validateEmail($email) || !validateMobile($mobile) || empty($year) || !is_numeric($year)) {
            exit("");
        }

        if (!validateIDCard($idcard)) {
            exit("idcard");
        }

        if (!$user->checkPassword($password)) {
            exit("password");
        }

        if ($user->isInfoComplete() && ($user->getUserInfo()["realname"] != $realname || $user->getUserInfo()["idcard"] != $idcard)) {
            exit("");
        }

        if (db::num_rows("SELECT `uid` FROM `TABLEPREFIX_userinfo` where `idcard` = ? and `uid` != ?", "si", [$idcard, $user->getID()])) {
            exit("idcard2");
        }

        $user->updateProfile($realname, $idcard, $email, $mobile, $year);

        log::writeLog(2, 3, 203, "修改个人信息");
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets['profile'] = '';
        $assets['ip'] = '';
        return true;
    }

    $loginLogs = $user->getRecentLogin();
?>

<div class="container">
    <div class="main-body">
          <div class="row gutters-sm mt-5">
             <?php route::include("template/profile/leftbar") ?>
            
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <form id="form-profile" action="#">
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">真实姓名</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="text" class="form-control" id="input-realname" placeholder="请输入您的姓名" value="<?= $user->getUserInfo()["realname"] ?>" <?= !$user->isInfoComplete() ? NULL : "disabled" ?>>
                                    <div class="invalid-feedback" id="help-realname"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">身份证号</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="text" class="form-control" id="input-idcard" placeholder="请输入您的身份证号" value="<?= $user->getUserInfo()["idcard"] ?>" <?= !$user->isInfoComplete() ? NULL : "disabled" ?>>
                                    <div class="invalid-feedback" id="help-idcard"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">电子邮箱</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="text" class="form-control" id="input-email" placeholder="请输入您的电子邮箱" value="<?= empty($user->getUserinfo()["emailUnverify"]) ? $user->getUserinfo()["email"] : $user->getUserinfo()["emailUnverify"]?>">
                                    <div class="invalid-feedback" id="help-email"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">联系电话</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="text" class="form-control" id="input-mobile" placeholder="请输入您的联系电话" value="<?= $user->getUserInfo()["mobile"] ?>">
                                    <div class="invalid-feedback" id="help-mobile"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">入学年份</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="number" class="form-control" id="input-year" placeholder="请输入您的入学年份" value="<?= $user->getUserInfo()["year"] ?>">
                                    <div class="invalid-feedback" id="help-year"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-3">
                                    <h6 class="mb-2">账户密码</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <input type="password" class="form-control" placeholder="请输入您的密码" value="" id="input-password">
                                    <div class="invalid-feedback" id="help-password"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-secondary">
                                    
                                    <a href="/"><input type="button" class="btn btn-secondary px-4" value="放弃修改"></a>
                                    <input type="submit" class="btn btn-primary px-4 ml-2" value="保存修改">
                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            function validateProfilePost() {
                                var ok = true;
                                ok &= getFormErrorAndShowHelp('realname', validateRealname);
                                ok &= getFormErrorAndShowHelp('idcard', validateIDCard);
                                ok &= getFormErrorAndShowHelp('email', validateEmail);
                                ok &= getFormErrorAndShowHelp('mobile', validateMobile);
                                ok &= getFormErrorAndShowHelp('password', validatePassword);
                                ok &= getFormErrorAndShowHelp('year', validateYear, '入学年份');
                                return ok;
                            }

                            function submitProfilePost() {
                                if (!validateProfilePost()) {
                                    return false;
                                }

                                $.post('/edit', {
                                    csrf : "<?= frame::clientKey() ?>",
                                    realname : $('#input-realname').val(),
                                    idcard : $('#input-idcard').val(),
                                    email : $('#input-email').val(),
                                    mobile : $('#input-mobile').val(),
                                    year : $('#input-year').val(),
                                    password : $('#input-password').val(),
                                    profile : "edit"
                                }, function(msg) {
                                    if (msg == 'ok') {
                                        location.href = "/";
                                    } else if (msg == 'expired') {
                                        showErrorHelp("realname", "页面会话已过期");
                                    } else if (msg == 'password') {
                                        showErrorHelp("password", "账户密码不正确");
                                    } else if (msg == 'idcard') {
                                        showErrorHelp("idcard", "身份证号不正确");
                                    } else if (msg == 'idcard2') {
                                        showErrorHelp("idcard", "身份证号已被注册");
                                    } else {
                                        showErrorHelp("realname", "未知错误");
                                    }
                                });

                                return true;
                            }

                            $(document).ready(function() {
                                $('#form-profile').submit(function(e) {
                                    e.preventDefault();
                                    submitProfilePost();
                                });
                            });
                        </script>
                    </div>
                </div>

              <div class="row gutters-sm">
                <div class="col-sm-12">
                  <div class="card h-100">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-4">近期登录记录</h6>
                      <table class="table table-striped table-hover text-center">
                        <thead>
                            <tr>
                            <th scope="col">日期</th>
                            <th scope="col">服务</th>
                            <th scope="col">登录IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($loginLogs as $log): ?>
                            <?php $site = new client($log["cid"]); ?>
                            <tr>
                            <td class="text-center"><?= $log["date"] ?></th>
                            <td class="text-center"><?= $site->getClientInfo()["shortname"] ?></td>
                            <td class="text-center"><a href="#" data-toggle="tooltip" data-placement="top" title="<?= frame::getIPLocation($log["ip"]); ?>"><?= $log["ip"] ?></a></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>



            </div>
          </div>

        </div>
    </div>