<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];
    
    if (!empty($_GET["redirect"])) {
      $user->checkCallback(getParam("redirect", "GET"));
    }
    
    if ($_POST["action"] == "resend") {
      if (!checkCSRF()) {
          exit("expired");
      }

      $user->sendAuthEmail($user->getUserinfo()["emailUnverify"]);

      log::writeLog(2, 3, 206, "重新发送验证邮件");
      exit("ok");
    }

    if($assetsFlag == 1){
        $assets['profile'] = '';
        $assets['ip'] = '';
        return true;
    }

    $loginLogs = $user->getRecentLogin();
?>

<div class="container">
    <div class="main-body">
          <div class="row gutters-sm mt-5">
            <?php route::include("template/profile/leftbar") ?>
            
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">真实姓名</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?= $user->getUserInfo()["realname"] ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">身份证号</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?= $user->getUserInfo()["idcard"] ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">电子邮箱</h6>
                    </div>
                    <div class="col-sm-9 text-secondary <?= !$user->isVerifyEmail() ? "text-danger" : NULL ?>">
                      <?php if ($user->isInfoComplete() && !$user->isVerifyEmail()): ?>
                        <a data-toggle="modal" data-target="#resend-modal">请点击验证邮件内链接进行验证</a>
                      <?php else: ?>
                        <?= $user->getUserInfo()["email"] ?>
                      <?php endif ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">联系电话</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?= $user->getUserInfo()["mobile"] ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">入学年份</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?= $user->getUserInfo()["year"] ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-12">
                      <a class="btn btn-info" href="/edit">编辑</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row gutters-sm">
                <div class="col-sm-12">
                  <div class="card h-100">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-4">近期登录记录</h6>
                      <table class="table table-striped table-hover text-center">
                        <thead>
                            <tr>
                            <th scope="col">日期</th>
                            <th scope="col">服务</th>
                            <th scope="col">登录IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($loginLogs as $log): ?>
                            <?php $site = new client($log["cid"]); ?>
                            <tr>
                            <td class="text-center"><?= $log["date"] ?></th>
                            <td class="text-center"><?= $site->getClientInfo()["shortname"] ?></td>
                            <td class="text-center"><a href="#" data-toggle="tooltip" data-placement="top" title="<?= frame::getIPLocation($log["ip"]); ?>"><?= $log["ip"] ?></a></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>



            </div>
          </div>

        </div>
    </div>

    <div id="resend-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <form id="form-resend" action="#">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否重新发送验证邮件</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            function submitResendPost() {
                $.post('/', {
                    csrf : "<?= frame::clientKey() ?>",
                    action : "resend"
                }, function(msg) {
                    if (msg == 'ok') {
                        location.reload();
                    }
                });
                return true;
            }

            $(document).ready(function() {
                $('#form-resend').submit(function(e) {
                    e.preventDefault();
                    submitResendPost();
                });
            });
        </script>
    </div>