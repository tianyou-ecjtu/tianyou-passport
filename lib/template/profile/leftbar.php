<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];
    $grav_url = "https://sdn.geekzu.org/avatar/" . md5( strtolower($user->getUserInfo()["email"])) . "?s=150";

    $clientList = client::getClientDisplayList($user);
?>

<div class="col-md-4 mb-3">
    <div class="card">
    <div class="card-body">
        <div class="d-flex flex-column align-items-center text-center">
        <img src="<?= $grav_url ?>" class="rounded-circle avatar">
        <div class="mt-3">
            <h4><?= $user->getUsername(); ?></h4>
            <?php if (!$user->isVerify()): ?>
            <p class="text-secondary mt-3 mb-3 text-danger">
                <?php if (!$user->isInfoComplete()): ?>
                账号基本信息不完整
                <?php elseif (!$user->isVerifyEmail()): ?>
                账号邮箱未验证
                <?php endif ?>
            </p>
            <?php else: ?>
            <p class="text-secondary mt-3 mb-3">
                账号状态正常
            </p>
            <?php endif ?>
            <?php if (defined("LEFTBAR_DISPLAY_HOME")): ?>
            <a href="/"><button class="btn btn-outline-primary">个人主页</button></a>
            <?php else: ?>
            <a href="/security"><button class="btn btn-outline-primary">安全设置</button></a>
            <?php endif ?>
            <a href="/logout"><button class="btn btn-primary ml-1">退出</button></a>
        </div>
        </div>
    </div>
    </div>
    <div class="card mt-3">
    <ul class="list-group list-group-flush">
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
        <h6 class="mb-0">智慧交大</h6>
        <?php if ($user->isBindEcjtu()) : ?>
            <a href="/ecjtu"><span class="text-secondary">已绑定</span></a>
        <?php else: ?>
        <a href="/ecjtu"><span class="text-secondary">点击绑定</span></a>
        <?php endif ?>
        </li>
        <?php if ($user->isAdmin()): ?>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
        <h6 class="mb-0">认证平台管理</h6>
        <a href="/panel/sites"><span class="text-secondary">点击跳转</span></a>
        </li>
        <?php endif ?>
        <?php foreach ($clientList as $client): ?>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
        <h6 class="mb-0"><a title="<?= $client["name"] ?>"><?= $client["shortname"] ?></a></h6>
        <a href="<?= $client["url"] ?>"><span class="text-secondary">点击跳转</span></a>
        </li>
        <?php endforeach ?>
    </ul>
    </div>
</div>