<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if (!empty($_POST["setting"])) {
        if (!checkCSRF()) {
            exit("expired");
        }
    }

    if ($_POST["setting"] == "setting") {
        $sitename = getParam("sitename");
        $shortname = getParam("shortname");
        $orgname = getParam("orgname");
        $orgsite = getParam("orgsite");
        $icp = getParam("icp");

        if (empty($sitename) || empty($shortname) || empty($orgname) || empty($orgsite) || empty($icp)) {
            exit("");
        } 

        if (!validateURL($orgsite)) {
            exit("url");
        }

        updateSystemVariable("site/name", $sitename);
        updateSystemVariable("site/shortname", $shortname);
        updateSystemVariable("site/organization", $orgname);
        updateSystemVariable("site/organization_site", $orgsite);
        updateSystemVariable("site/icp", $icp);
        
        Cache::unset("SystemVariables");

        log::writeLog(2, 3, 321, "修改系统设置");
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        return true;
    }
?>

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>
        
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">系统设置</h4>
                                <form id="form-setting" action="#">
                                    <div class="form-body mt-3">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="text-dark" for="input-sitename">站点名称</label>
                                                    <input class="form-control" id="input-sitename" type="text"
                                                        placeholder="请输入站点名称" value="<?= getSystemVariable("site/name") ?>">
                                                    <div class="invalid-feedback" id="help-sitename"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="text-dark" for="input-shortname">站点简称</label>
                                                    <input class="form-control" id="input-shortname" type="text"
                                                        placeholder="请输入站点简称" value="<?= getSystemVariable("site/shortname") ?>">
                                                    <div class="invalid-feedback" id="help-shortname"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="text-dark" for="input-orgname">组织名称</label>
                                                    <input class="form-control" id="input-orgname" type="text"
                                                        placeholder="请输入组织名称" value="<?= getSystemVariable("site/organization") ?>">
                                                    <div class="invalid-feedback" id="help-orgname"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="text-dark" for="input-orgsite">组织网站</label>
                                                    <input class="form-control" id="input-orgsite" type="text"
                                                        placeholder="请输入组织网站" value="<?= getSystemVariable("site/organization_site") ?>">
                                                    <div class="invalid-feedback" id="help-orgsite"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="text-dark" for="input-icp">ICP 备案</label>
                                                    <input class="form-control" id="input-icp" type="text"
                                                        placeholder="请输入ICP备案号" value="<?= getSystemVariable("site/icp") ?>">
                                                    <div class="invalid-feedback" id="help-icp"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="text-left float-right">
                                            <button type="submit" class="btn btn-info">提交</button>
                                            <button type="reset" class="btn btn-dark">重置</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <script type="text/javascript">
                            function validateSettingPost() {
                                var ok = true;
                                ok &= getFormErrorAndShowHelp('sitename', validateNotEmpty, '站点名称');
                                ok &= getFormErrorAndShowHelp('shortname', validateNotEmpty, '站点简称');
                                ok &= getFormErrorAndShowHelp('orgname', validateNotEmpty, '组织名称');
                                ok &= getFormErrorAndShowHelp('orgsite', validateURL, '组织网站');
                                ok &= getFormErrorAndShowHelp('icp', validateNotEmpty, 'ICP备案');
                                return ok;
                            }

                            function submitSettingPost() {
                                if (!validateSettingPost()) {
                                    return false;
                                }

                                $.post('/panel/system/setting', {
                                    csrf : "<?= frame::clientKey() ?>",
                                    sitename : $('#input-sitename').val(),
                                    shortname : $('#input-shortname').val(),
                                    orgname : $('#input-orgname').val(),
                                    orgsite : $('#input-orgsite').val(),
                                    icp : $('#input-icp').val(),
                                    setting : "setting"
                                }, function(msg) {
                                    if (msg == 'ok') {
                                        location.reload();
                                    } else if (msg == 'expired') {
                                        showErrorHelp("sitename", "页面会话已过期");
                                    } else if (msg == 'url') {
                                        showErrorHelp("orgsite", "组织网站不合法");
                                    } else {
                                        showErrorHelp("sitename", "未知错误");
                                    }
                                });

                                return true;
                            }

                            $(document).ready(function() {
                                $('#form-setting').submit(function(e) {
                                    e.preventDefault();
                                    submitSettingPost();
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>