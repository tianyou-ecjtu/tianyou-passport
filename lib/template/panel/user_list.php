<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "forget") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            invalid(401, "请求非法", "请求参数不合法");
            exit();
        }

        $forgetUser = new user($id);

        if (!$forgetUser->checkExist()) {
            invalid(402, "请求非法", "用户 ID 不存在");
            exit();
        }

        $token = $forgetUser->getForgetToken();

        log::writeLog(2, 3, 301, "重置用户密码");
        exit(json_encode(
            [
                "msg" => "ok",
                "link" => getSiteLink("forget/".$token)
            ]
        ));
    } else if ($_POST["action"] == "reset") {
        $id = getParam("id");
        $resetUser = new user($id);

        if (empty($id) || !is_numeric($id)) {
            invalid(401, "请求非法", "请求参数不合法");
            exit();
        }

        if (!$resetUser->checkExist()) {
            invalid(402, "请求非法", "用户 ID 不存在");
            exit();
        }

        $resetUser->resetProfile();

        log::writeLog(2, 3, 302, "重置用户个人信息");
        exit("ok");   
    } else if ($_POST["action"] == "block") {
        $id = getParam("id");
        $status = getParam("status");

        $blockUser = new user($id);

        if (empty($id) || !is_numeric($id) || !isset($status) || !is_numeric($status) || ($status != 0 && $status != 1)) {
            invalid(401, "请求非法", "请求参数不合法");
            exit();
        }

        if (!$blockUser->checkExist()) {
            invalid(402, "请求非法", "用户 ID 不存在");
            exit();
        }

        db::update("UPDATE `TABLEPREFIX_users` SET `isBlock` = ? where `id` = ?", "ii", [$status, $id]);
        user::cacheUserUpdate($id);

        log::writeLog(2, 3, 303, "修改用户账户状态");
        exit("ok");   
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        $assets["base64"] = '';
        return true;
    }

    $pageURL = "/panel/user/list";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");

        $sql = "FROM `TABLEPREFIX_userinfo` where (`username` = ? or `realname` like ? or `idcard` = ? or `email` = ? or `emailUnverify` = ? or `mobile` = ?) and `isThirdpart` = 0";

        $argt = "ssssss";
        $argv = [$keyword, "{$keyword}%", $keyword, $keyword, $keyword, $keyword];
    } else {
        $sql = "FROM `TABLEPREFIX_userinfo` where `isThirdpart` = 0";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT `uid` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT `uid` {$sql} order by `uid` DESC LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">注册用户列表</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">用户名</th>
                                                    <th scope="col">姓名</th>
                                                    <th scope="col">联系电话</th>
                                                    <th scope="col">状态</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $user_data = new user($data["uid"]);?>
                                                
                                                <tr>
                                                    <td><?= $user_data->getID() ?></td>
                                                    <td><?= $user_data->getUsername() ?></td>
                                                    <td><?= $user_data->getUserInfo()["realname"] ?></td>
                                                    <td><?= $user_data->getUserInfo()["mobile"] ?></td>
                                                    <?php if ($user_data->isBlock()): ?>
                                                    <td class="text-danger">已停用</td>
                                                    <?php elseif ($user_data->isAdmin()): ?>
                                                    <td>正常</td>
                                                    <?php elseif (!$user_data->isInfoComplete()): ?>
                                                    <td class="text-danger">未完善</td>
                                                    <?php elseif (!$user_data->isVerify()): ?>
                                                    <td class="text-danger">未认证</td>
                                                    <?php else: ?>
                                                    <td>正常</td>
                                                    <?php endif ?>
                                                    <td><a href="#" data-toggle="modal" data-target="#info-modal" data-id="<?= $user_data->getID() ?>"><span class="fas fa-search"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#block-modal" data-id="<?= $user_data->getID() ?>"  data-username="<?= $user_data->getUsername() ?>" data-block="<?= $user_data->isBlock() ? 0 : 1 ?>"><span class="fas <?= $user_data->isBlock() ? "fa-check-circle" : "fa-pause-circle"?>"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#forget-modal" data-id="<?= $user_data->getID() ?>" data-username="<?= $user_data->getUsername() ?>"><span class="fas fa-key"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#reset-modal" data-id="<?= $user_data->getID() ?>" data-username="<?= $user_data->getUsername() ?>"><span class="fas fa-address-card"></span></a></td>
                                                </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="info-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">详细信息 - <span id="info-username"></span></h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <div style="padding:20px">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered no-wrap text-center">
                            <tbody>
                                <tr>
                                    <td>姓名</td>
                                    <td id="info-realname"></td>    
                                    <td>性别</td>
                                    <td id="info-sex"></td>                              
                                </tr>
                                <tr>
                                    <td>身份证号</td>
                                    <td id="info-idcard"></td>
                                    <td>生日</td>
                                    <td id="info-birthday"></td>
                                </tr>
                                <tr>
                                    <td>电子邮箱</td>
                                    <td id="info-email"></td>
                                    <td>联系电话</td>
                                    <td id="info-mobile"></td>
                                </tr>
                                <tr>
                                    <td>待验证邮箱</td>
                                    <td id="info-email-unverify"></td>
                                    <td>学工号</td>
                                    <td id="info-ecjtu"></td>
                                </tr>
                                <tr>
                                    <td>注册时间</td>
                                    <td id="info-register-date"></td>
                                    <td>注册IP</td>
                                    <td id="info-register-ip"></td>
                                </tr>
                                <tr>
                                    <td>上次登录时间</td>
                                    <td id="info-last-date"></td>
                                    <td>上次登录IP</td>
                                    <td id="info-last-ip"></td>
                                </tr>
                                <tr>
                                    <td>查询日志</td>
                                    <td id="info-log-user" colspan="3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="forget-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-forget" action="#">
                    <input type="hidden" name="forget-id" id="forget-id" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认重置 <span id="forget-username"></span> 的账户密码</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitDeletePost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#forget-id').val(),
                    action : "forget"
                }, function(msg) {
                    res = $.parseJSON(msg)
                    if (res.msg == 'ok') {
                        window.open(res.link)
                        $('#forget-modal').modal('hide');
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-forget').submit(function(e) {
                    e.preventDefault();
                    submitDeletePost();
                });
            });
        </script>
    </div>

    <div id="block-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-block" action="#">
                    <input type="hidden" name="block-id" id="block-id" value=""/>
                    <input type="hidden" name="block-status" id="block-status" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认<span id="block-status-text"></span>账户 <span id="block-username"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitBlockPost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#block-id').val(),
                    status : $('#block-status').val(),
                    action : "block"
                }, function(msg) {
                    if (msg == 'ok') {
                        location.reload()
                        $('#block-modal').modal('hide');
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-block').submit(function(e) {
                    e.preventDefault();
                    submitBlockPost();
                });
            });
        </script>
    </div>

    <div id="reset-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-reset" action="#">
                    <input type="hidden" name="reset-id" id="reset-id" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认重置 <span id="reset-username"></span> 的个人信息（包括三方绑定信息）</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitResetPost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#reset-id').val(),
                    action : "reset"
                }, function(msg) {
                    if (msg == 'ok') {
                        location.reload()
                        $('#reset-modal').modal('hide');
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-reset').submit(function(e) {
                    e.preventDefault();
                    submitResetPost();
                });
            });
        </script>
    </div>

    <script>
        $('#info-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')

            $.post('/apiv2/user/getUserInfoFull', {
                csrf: '<?= frame::clientKey() ?>',
                data: Base64.encode(JSON.stringify({id: id}))
            }, function(data) {
                res = JSON.parse(data)
                res = JSON.parse(Base64.decode(res.data))

                modal.find('#info-username').text(res.username)
                modal.find('#info-realname').text(res.realname)
                modal.find('#info-idcard').text(res.idcard)
                modal.find('#info-birthday').text(res.birthday)
                modal.find('#info-email').text(res.email)
                modal.find('#info-mobile').text(res.mobile)
                modal.find('#info-ecjtu').text(res.ecjtuID)
                modal.find('#info-register-date').text(res.registerTime)
                modal.find('#info-register-ip').text(res.registerIP)
                modal.find('#info-last-date').text(res.lastLoginTime)
                modal.find('#info-last-ip').text(res.lastLoginIP)
                modal.find('#info-email-unverify').text(res.emailUnverify)
                modal.find('#info-log-user').html("<a href=\"/panel/system/log?keyword="+res.username+"\" target=\"_blank\">点击此处</a>")

                if (res.sex != null) {
                    modal.find('#info-sex').text(res.sex == 1 ? "男" : "女")
                }
            });
        });

        $('#info-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            $(['username', 'realname', 'sex', 'province', 'year', 'idcard', 'birthday', 'email', 'mobile', 'email-unverify', 'ecjtu', 'register-date', 'register-ip', 'last-date', 'last-ip']).each(function () {
                modal.find('#info-' + this).text("")
            });

            modal.find("#info-log-user").html("")
        })

        $('#forget-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var username = button.data('username')

            modal.find('#forget-username').text(username)
            modal.find('#forget-id').val(id)
        });

        $('#forget-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#forget-username').text("")
            modal.find("#forget-id").val("")
        });

        $('#reset-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var username = button.data('username')

            modal.find('#reset-username').text(username)
            modal.find('#reset-id').val(id)
        });

        $('#reset-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#reset-username').text("")
            modal.find("#reset-id").val("")
        });

        $('#block-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var username = button.data('username')
            var block = button.data('block')

            modal.find('#block-username').text(username)
            modal.find('#block-id').val(id)
            modal.find('#block-status').val(block)

            if (block) {
                modal.find('#block-status-text').text("禁用")
            } else {
                modal.find('#block-status-text').text("启用")
            }
        });

        $('#block-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#block-username').text("")
            modal.find("#block-id").val("")
        });

    </script>