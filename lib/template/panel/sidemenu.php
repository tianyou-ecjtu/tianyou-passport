<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if($assetsFlag == 1){
        return true;
    }

?>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/"
                                aria-expanded="false"><i data-feather="server" class="feather-icon"></i><span
                                    class="hide-menu">个人主页</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu font-14">站点权限</span></li>
                        <?php $list = client::getClientList(); ?>
                        <?php foreach ($list as $site): ?>
                            <?php if ($site["id"] == 1) continue ?>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i data-feather="globe" class="feather-icon"></i><span
                                    class="hide-menu"> <?= $site["shortname"] ?> </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="/panel/permission/<?= $site["id"]?>/list" class="sidebar-link"><span
                                            class="hide-menu"> 权限列表
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/permission/<?= $site["id"]?>/user" class="sidebar-link"><span
                                            class="hide-menu"> 用户权限
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/permission/<?= $site["id"]?>/group" class="sidebar-link"><span
                                            class="hide-menu"> 用户组权限
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                        <?php endforeach ?>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu font-14">通用</span></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/panel/sites"
                                aria-expanded="false"><i data-feather="database" class="feather-icon"></i><span
                                    class="hide-menu">站点管理
                                </span></a>
                        </li>    
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i data-feather="user" class="feather-icon"></i><span
                                    class="hide-menu"> 用户管理 </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="/panel/user/list" class="sidebar-link"><span
                                            class="hide-menu"> 注册用户列表
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/user/3rdparty" class="sidebar-link"><span
                                            class="hide-menu"> 三方用户列表
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/user/ecjtu" class="sidebar-link"><span
                                            class="hide-menu"> 智慧交大绑定
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i data-feather="users" class="feather-icon"></i><span
                                    class="hide-menu"> 用户组管理 </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="/panel/group/list" class="sidebar-link"><span
                                            class="hide-menu"> 用户组列表
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/group/preset" class="sidebar-link"><span
                                            class="hide-menu"> 用户组预置
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/group/assign" class="sidebar-link"><span
                                            class="hide-menu"> 用户组分配
                                        </span></a>
                                </li>
                            </ul>
                        </li>                    
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span
                                    class="hide-menu"> 系统管理 </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="/panel/system/log" class="sidebar-link"><span
                                            class="hide-menu"> 系统日志
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="/panel/system/setting" class="sidebar-link"><span
                                            class="hide-menu"> 系统设置
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>