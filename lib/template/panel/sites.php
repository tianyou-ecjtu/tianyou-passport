<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "block") {
        $id = getParam("id");
        $status = getParam("status");

        if (empty($id) || !isset($status) || !is_numeric($id) || ($status != 0 && $status != 1)) {
            exit();
        }

        $client = new client($id);
        
        if (!$client->check()) {
            invalid(404, "请求非法", "站点ID不存在");
        }

        $client->updateStatus($status);
        log::writeLog(2, 3, 317, "修改站点状态");
        exit("ok");
    } else if ($_POST["action"] == "reset") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit();
        }

        $client = new client($id);
        
        if (!$client->check()) {
            invalid(404, "请求非法", "站点ID不存在");
        }

        $token = $client->resetToken();
        log::writeLog(2, 3, 317, "重置站点主密钥");

        exit("{\"status\": \"ok\", \"token\": \"{$token}\"}");
    } else if ($_POST["action"] == "add") {
        $name = getParam("name");
        $shortname = getParam("shortname");
        $url = getParam("url");
        $display = getParam("display");

        if (empty($name) || empty($shortname) || empty($url) || !is_numeric($display) || ($display != -1 && $display != 0 && $display != 1)) {
            exit();
        }

        if (!validateURL($url)) {
            exit("url");
        }

        $token = client::generateToken();
        $id = db::insert("INSERT INTO `TABLEPREFIX_clients` (`name`, `shortname`, `masterKey`, `url`, `isDisplay`) VALUES (?,?,?,?,?)", "ssssi", [$name, $shortname, $token, $url, $display]);

        $client = new client($id);

        if (!$client->check()) {
            exit();
        }

        db::insert("INSERT INTO `TABLEPREFIX_permission_list` (`cid`, `name`, `description`) VALUES (?, ?, ?)", "iss", [$id, '_display_frontend', "统一认证平台导航栏显示本站点"]);

        client::cacheClientDisplayListUpdate();
        log::writeLog(2, 3, 314, "新增站点");

        exit("{\"status\": \"ok\", \"token\": \"{$token}\"}");
    } else if ($_POST["action"] == "edit") {
        $id = getParam("id");
        $name = getParam("name");
        $shortname = getParam("shortname");
        $url = getParam("url");
        $display = getParam("display");

        if (empty($id) || empty($name) || empty($shortname) || empty($url) || !is_numeric($id) || !is_numeric($display) || ($display != -1 && $display != 0 && $display != 1)) {
            exit();
        }

        if (!validateURL($url)) {
            exit("url");
        }

        $client = new client($id);

        if (!$client->check()) {
            exit();
        }

        $client->updateInfo($name, $shortname, $url, $display);

        client::cacheClientDisplayListUpdate();
        log::writeLog(2, 3, 315, "编辑站点");

        exit("ok");   
    } else if ($_POST["action"] == "delete") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit();
        }

        $client = new client($id);

        if (!$client->check()) {
            exit();
        }

        $client->delete();

        client::cacheClientDisplayListUpdate();
        log::writeLog(2, 3, 316, "删除站点");

        exit("ok");        
    }
    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        return true;
    }

    $pageURL = "/panel/sites";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");

        $sql = "FROM `TABLEPREFIX_clients` where `name` like ? or `shortname` like ? or `url` like ?";

        $argt = "sss";
        $argv = ["%{$keyword}%", "%{$keyword}%", "%{$keyword}%"];
    } else {
        $sql = "FROM `TABLEPREFIX_clients`";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT `id` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT `id` {$sql} ORDER BY `id` LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">站点管理</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                        <div style="margin-left: auto !important; order: 2;">
                                            <a href="#" data-toggle="modal" data-target="#add-modal"><button class="btn btn-primary waves-effect waves-light float-right ml-2" type="button"><span class="btn-label"><i class="fas fa-plus-circle"></i></span> 新增 </button></a>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">站点简称</th>
                                                    <th scope="col">主密钥</th>
                                                    <th scope="col">站点地址</th>
                                                    <th scope="col">状态</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $client = new client($data["id"]); ?>
                                                <tr>
                                                    <td><?= $client->getID() ?></td>
                                                    <td><?= $client->getClientInfo()["shortname"] ?></td>
                                                    <td><?= substr($client->getClientInfo()["masterKey"], 0, 3) ?>***</td>
                                                    <td><?= $client->getClientInfo()["url"] ?></td>
                                                    <?php if ($client->isEnable()): ?>
                                                    <td>正常</td>
                                                    <?php else: ?>
                                                    <td class="text-danger">已停用</td>
                                                    <?php endif ?>
                                                    <?php if ($data["id"] == 1): ?>
                                                    <td><a href="#" class="btn-link disabled"><span class="fas <?= $client->isEnable() ? "fa-pause-circle" : "fa-check-circle"?>"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#reset-modal" data-id="<?= $client->getID() ?>" data-shortname="<?= $client->getClientInfo()["shortname"] ?>"><span class="fas fa-key"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#edit-modal" data-id="<?= $client->getID() ?>" data-name="<?= $client->getClientInfo()["name"] ?>" data-shortname="<?= $client->getClientInfo()["shortname"] ?>" data-url="<?= $client->getClientInfo()["url"] ?>" data-display="<?= $client->isDisplay()?>"><span class="fas fa-edit"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" class="btn-link disabled"><span class="fas fa-trash-alt"></span></a></td>
                                                    <?php else: ?>
                                                    <td><a href="#" data-toggle="modal" data-target="#block-modal" data-id="<?= $client->getID() ?>"  data-shortname="<?= $client->getClientInfo()["shortname"] ?>" data-enable="<?= $client->isEnable() ? 0 : 1 ?>"><span class="fas <?= $client->isEnable() ? "fa-pause-circle" : "fa-check-circle"?>"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#reset-modal" data-id="<?= $client->getID() ?>" data-shortname="<?= $client->getClientInfo()["shortname"] ?>"><span class="fas fa-key"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#edit-modal" data-id="<?= $client->getID() ?>" data-name="<?= $client->getClientInfo()["name"] ?>" data-shortname="<?= $client->getClientInfo()["shortname"] ?>" data-url="<?= $client->getClientInfo()["url"] ?>" data-display="<?= $client->isDisplay()?>"><span class="fas fa-edit"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#delete-modal" data-id="<?= $client->getID() ?>" data-shortname="<?= $client->getClientInfo()["shortname"] ?>"><span class="fas fa-trash-alt"></span></a></td>
                                                    <?php endif ?>
                                                </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">添加用户组</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-add" action="#" class="pl-3 pr-3">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-dark" for="input-add-name"><span class="text-danger">*</span> 站点名称</label>
                            <input class="form-control" id="input-add-name" type="text"
                                placeholder="请输入站点名称" value="">
                            <div class="invalid-feedback" id="help-add-name"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-shortname"><span class="text-danger">*</span> 站点简称</label>
                            <input class="form-control" id="input-add-shortname" type="text"
                                placeholder="请输入站点简称" value="">
                            <div class="invalid-feedback" id="help-add-shortname"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-url"><span class="text-danger">*</span> 站点地址</label>
                            <input class="form-control" id="input-add-url" type="text"
                                placeholder="请输入站点URL地址" value="">
                            <div class="invalid-feedback" id="help-add-url"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-display"><span class="text-danger">*</span> 前台显示</label>
                            <select class="custom-select text-black-50" id="input-add-display">
                                <option value="-1">随用户权限</option>
                                <option value="0">不显示</option>
                                <option value="1">显示</option>
                            </select>
                            <div class="invalid-feedback" id="help-add-display"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function validateAddPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('add-name', validateNotEmpty, '站点名称');
                ok &= getFormErrorAndShowHelp('add-shortname', validateNotEmpty, '站点简称');
                ok &= getFormErrorAndShowHelp('add-url', validateURL, '站点地址');
                return ok;
            }

            function submitAddPost() {
                if (!validateAddPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    name : $('#input-add-name').val(),
                    shortname : $('#input-add-shortname').val(),
                    url : $('#input-add-url').val(),
                    display: $('#input-add-display').val(),
                    action : "add"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#add-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'url') {
                        showErrorHelp("add-url", "站点地址不合法");
                    } else if (msg == 'expired') {
                        showErrorHelp("add-name", "页面会话已过期");
                    } else {
                        try {
                            var obj = JSON.parse(msg);

                            if (obj.status == 'ok') {
                                $('#reset-result-modal').modal('show');    
                                $('#add-modal').modal('hide');
                                $('#reset-result-token').text(obj.token);       
                            }
                        } catch (e) {
                            console.log(e);
                            showErrorHelp("add-name", "未知错误");
                        }
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-add').submit(function(e) {
                    e.preventDefault();
                    submitAddPost();
                });
            });
        </script>
    </div>

    <div id="block-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-block" action="#">
                    <input type="hidden" name="block-id" id="block-id" value=""/>
                    <input type="hidden" name="block-status" id="block-status" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认<span id="block-status-text"></span>站点 <span id="block-shortname"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitBlockPost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#block-id').val(),
                    status : $('#block-status').val(),
                    action : "block"
                }, function(msg) {
                    if (msg == 'ok') {
                        location.reload()
                        $('#block-modal').modal('hide');
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-block').submit(function(e) {
                    e.preventDefault();
                    submitBlockPost();
                });
            });
        </script>
    </div>

    <div id="reset-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-reset" action="#">
                    <input type="hidden" name="reset-id" id="reset-id" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认重置 <span id="reset-shortname"></span> 的主密钥</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitResetPost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#reset-id').val(),
                    action : "reset"
                }, function(msg) {
                    var obj = JSON.parse(msg);
                    
                    if (obj.status == 'ok') {
                        $('#reset-result-modal').modal('show');    
                        $('#reset-modal').modal('hide');
                        $('#reset-result-token').text(obj.token);       
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-reset').submit(function(e) {
                    e.preventDefault();
                    submitResetPost();
                });
            });
        </script>
    </div>

    <div id="reset-result-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-reset" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作成功</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>该站点的主密钥已重置，<b>仅显示一次</b>，请妥善保存</p>
                        <div class="code">
                            <span id="reset-result-token"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">编辑用户组</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-edit" action="#" class="pl-3 pr-3">
                    <input type="hidden" name="input-edit-id" id="input-edit-id" value=""/>
                    <div class="modal-body">
                    <div class="form-group">
                            <label class="text-dark" for="input-edit-name"><span class="text-danger">*</span> 站点名称</label>
                            <input class="form-control" id="input-edit-name" type="text"
                                placeholder="请输入站点名称" value="">
                            <div class="invalid-feedback" id="help-edit-name"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-shortname"><span class="text-danger">*</span> 站点简称</label>
                            <input class="form-control" id="input-edit-shortname" type="text"
                                placeholder="请输入站点简称" value="">
                            <div class="invalid-feedback" id="help-edit-shortname"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-url"><span class="text-danger">*</span> 站点地址</label>
                            <input class="form-control" id="input-edit-url" type="text"
                                placeholder="请输入站点URL地址" value="">
                            <div class="invalid-feedback" id="help-edit-url"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-display"><span class="text-danger">*</span> 前台显示</label>
                            <select class="custom-select text-black-50" id="input-edit-display">
                                <option value="-1">随用户权限</option>
                                <option value="0">不显示</option>
                                <option value="1">显示</option>
                            </select>
                            <div class="invalid-feedback" id="help-edit-display"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function validateEditPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('edit-name', validateNotEmpty, '站点名称');
                ok &= getFormErrorAndShowHelp('edit-shortname', validateNotEmpty, '站点简称');
                ok &= getFormErrorAndShowHelp('edit-url', validateURL, '站点地址');
                return ok;
            }

            function submitEditPost() {
                if (!validateEditPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#input-edit-id').val(),
                    name : $('#input-edit-name').val(),
                    shortname : $('#input-edit-shortname').val(),
                    url : $('#input-edit-url').val(),
                    display : $('#input-edit-display').val(),
                    action : "edit"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#edit-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'url') {
                        showErrorHelp("edit-url", "站点地址不合法");
                    }  else if (msg == 'expired') {
                        showErrorHelp("edit-name", "页面会话已过期");
                    } else {
                        showErrorHelp("edit-name", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-edit').submit(function(e) {
                    e.preventDefault();
                    submitEditPost();
                });
            });
        </script>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <form id="form-delete" action="#">
            <input type="hidden" name="input-delete-id" id="input-delete-id" value=""/>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认删除站点 <span id="delete-modal-shortname"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            function submitDeletePost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#input-delete-id').val(),
                    action : "delete"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#delete-modal').modal('hide');
                        location.reload();
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-delete').submit(function(e) {
                    e.preventDefault();
                    submitDeletePost();
                });
            });
        </script>
    </div>
    <script>
        $('#delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var shortname = button.data('shortname')

            var modal = $(this)

            modal.find('#delete-modal-shortname').text(shortname)
            modal.find('#input-delete-id').val(id)
        });

        $('#block-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var shortname = button.data('shortname')
            var enable = button.data('enable')

            modal.find('#block-shortname').text(shortname)
            modal.find('#block-id').val(id)
            modal.find('#block-status').val(enable)

            if (enable) {
                modal.find('#block-status-text').text("启用")
            } else {
                modal.find('#block-status-text').text("停用")
            }
        });

        $('#edit-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var name = button.data('name')
            var shortname = button.data('shortname')
            var url = button.data('url')
            var display = button.data('display')

            var modal = $(this)
            
            modal.find('#input-edit-id').val(id)
            modal.find('#input-edit-name').val(name)
            modal.find('#input-edit-shortname').val(shortname)
            modal.find('#input-edit-url').val(url)
            modal.find('#input-edit-display').val(display)
        });

        $('#edit-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('edit-name');
            showErrorHelp('edit-shortname');
            showErrorHelp('edit-url');
        })

        $('#block-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#block-shortname').text("")
            modal.find("#block-id").val("")
        });

        $('#reset-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var shortname = button.data('shortname')

            modal.find('#reset-shortname').text(shortname)
            modal.find('#reset-id').val(id)
        });

        $('#reset-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#reset-shortname').text("")
            modal.find("#reset-id").val("")

            if ($('#reset-result-modal').hasClass('show')) {
                $('body').addClass('modal-open');
            }
        });

        $('#add-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('add-name');
            showErrorHelp('add-shortname');
            showErrorHelp('add-url');

            if ($('#reset-result-modal').hasClass('show')) {
                $('body').addClass('modal-open');
            }
        })

        $('#reset-result-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)
            modal.find('#reset-result-token').text("")
            location.reload()
        });
    </script>