<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "add") {
        $username = getParam("username");
        $group = getParam("group");

        if (empty($username) || empty($group) || !validateUsername($username)) {
            exit();
        }        

        $userInfo = $user->searchUser($username, ["username"], FALSE)[0];

        if (empty($userInfo)) {
            exit("username");
        }

        $groupInfo = $user->searchGroup($group, FALSE)[0];

        if (empty($groupInfo)) {
            exit("group");
        }

        if (db::num_rows("SELECT `id` FROM `TABLEPREFIX_user_group` where `uid` = ? and `gid` = ?", "ii", [$userInfo["uid"], $groupInfo["id"]])) {
            exit("exist");
        }

        db::insert("INSERT INTO `TABLEPREFIX_user_group` (`uid`, `gid`) VALUES (?, ?)", "ii", [$userInfo["uid"], $groupInfo["id"]]);
        user::cacheUserGroupUpdate($userInfo["uid"]);

        log::writeLog(2, 3, 312, "新增用户组分配");
        exit("ok");
    } else if ($_POST["action"] == "delete") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit("");
        }

        $uid = db::selectFirst("SELECT `uid` FROM `TABLEPREFIX_user_group` where `id` = ?", "i", [$id])["uid"];

        if (empty($uid)) {
            exit("");
        }
        
        db::delete("DELETE FROM `TABLEPREFIX_user_group` where `id` = ?", "i", [$id]);       
        user::cacheUserGroupUpdate($uid);

        log::writeLog(2, 3, 313, "删除用户组分配");   
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        $assets["base64"] = '';
        $assets["jquery-ui"] = '';
        return true;
    }

    $pageURL = "/panel/group/assign";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");
        $gid = db::selectFirst("SELECt `id` FROM `TABLEPREFIX_groups` where `name` = ?", "s", [$keyword])["id"];

        $sql = "FROM `TABLEPREFIX_user_group`, `TABLEPREFIX_userinfo` where ((TABLEPREFIX_userinfo.`username` = ? or TABLEPREFIX_userinfo.`realname` like ?) or TABLEPREFIX_user_group.`gid` = ?) and TABLEPREFIX_userinfo.`uid` = TABLEPREFIX_user_group.`uid`";

        $argt = "ssi";
        $argv = [$keyword, "{$keyword}%", $gid];
    } else {
        $sql = "FROM `TABLEPREFIX_user_group`";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT TABLEPREFIX_user_group.`id` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT * {$sql} ORDER BY TABLEPREFIX_user_group.`id` DESC LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">用户组分配</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                        <div style="margin-left: auto !important; order: 2;">
                                            <a href="#" data-toggle="modal" data-target="#add-modal"><button class="btn btn-primary waves-effect waves-light float-right ml-2" type="button"><span class="btn-label"><i class="fas fa-plus-circle"></i></span> 新增 </button></a>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">用户名</th>
                                                    <th scope="col">姓名</th>
                                                    <th scope="col">用户组</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $id = $dataTotal - $startID; ?>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $user = new user($data["uid"]); ?>
                                                <?php $groupInfo = user::getUserGroupInfo($data["gid"]); ?>
                                                <tr>
                                                    <td><?= $id ?></td>
                                                    <td><a href="/panel/user/list?keyword=<?= $user->getUsername() ?>" target="_blank"><?= $user->getUsername() ?></a></td>
                                                    <td><?= $user->getUserInfo()["realname"] ?></td>
                                                    <td><a href="/panel/group/list?keyword=<?= $groupInfo['name'] ?>" target="_blank"><?= $groupInfo["name"] ?></a></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#delete-modal" data-id="<?= $data["id"] ?>" data-username="<?= $user->getUsername() ?>" data-group="<?= $groupInfo['name'] ?>"><span class="fas fa-trash-alt"></span></a> </td>
                                                </tr>
                                                <?php $id = $id - 1; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="form-delete" action="#">
        <input type="hidden" name="input-delete-id" id="input-delete-id" value=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>是否确认删除 <span id="delete-modal-username"></span>  的预置用户组 <span id="delete-modal-group"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                        data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function submitDeletePost() {
            $.post('<?= $pageURL ?>', {
                csrf : "<?= frame::clientKey() ?>",
                id : $('#input-delete-id').val(),
                action : "delete"
            }, function(msg) {
                if (msg == 'ok') {
                    $('#delete-modal').modal('hide');
                    location.reload();
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-delete').submit(function(e) {
                e.preventDefault();
                submitDeletePost();
            });
        });
    </script>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">添加用户组分配</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-add" action="#" class="pl-3 pr-3">
                    <div class="modal-body ui-front">
                        <div class="form-group">
                            <label class="text-dark" for="input-add-username"><span class="text-danger">*</span> 用户名</label>
                            <input class="form-control" id="input-add-username" type="text"
                                placeholder="请输入用户名" value="" maxlength="20">
                            <div class="invalid-feedback" id="help-add-username"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-group"><span class="text-danger">*</span> 用户组</label>
                            <input class="form-control" id="input-add-group" type="text"
                                placeholder="请输入用户组名称" value="">
                            <div class="invalid-feedback" id="help-add-group"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function(){
                $("#input-add-username").autocomplete({
                    source: function(request, response){
                        $.post('/apiv2/autocomplete/searchUser', {
                            csrf : "<?= frame::clientKey() ?>",
                            data: Base64.encode(JSON.stringify({keyword: $('#input-add-username').val()}))
                        }, function(data) {
                            res = JSON.parse(data)
                            res = JSON.parse(Base64.decode(res.data))
                            response(res)
                        });
                    },
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<div>" + item.value + " <small><i>(" + item.realname + "," + item.year + ")</i></small></div>" )
                    .appendTo( ul );
                };
            });

            $(function(){
                $("#input-add-group").autocomplete({
                    source: function(request, response){
                        $.post('/apiv2/autocomplete/searchGroup', {
                            csrf : "<?= frame::clientKey() ?>",
                            data: Base64.encode(JSON.stringify({keyword: $('#input-add-group').val()}))
                        }, function(data) {
                            res = JSON.parse(data)
                            res = JSON.parse(Base64.decode(res.data))
                            response(res)
                        });
                    },
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<div>" + item.value + " <small><i>(" + item.description + ")</i></small></div>" )
                    .appendTo( ul );
                };
            });

            function validateAddPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('add-username', validateUsername);
                ok &= getFormErrorAndShowHelp('add-group', validateNotEmpty, '用户组');
                return ok;
            }

            function submitAddPost() {
                if (!validateAddPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    username : $('#input-add-username').val(),
                    group : $('#input-add-group').val(),
                    action : "add"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#add-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("add-username", "页面会话已过期");
                    } else if (msg == 'group') {
                        showErrorHelp("add-group", "用户组名称不存在");
                    } else if (msg == 'username') {
                        showErrorHelp("add-username", "用户名不存在");
                    } else if (msg == 'exist') {
                        showErrorHelp("add-username", "用户预置记录已存在");
                    } else {
                        showErrorHelp("add-username", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-add').submit(function(e) {
                    e.preventDefault();
                    submitAddPost();
                });
            });
        </script>
    </div>

    <div id="batch-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">批量操作</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-batch" action="#" class="pl-3 pr-3">
                    <div class="modal-body">
                        <p>1. <a href="<?= getCDNLink("assets/用户组预置.xlsx") ?>">点击此处</a>下载数据表</p>
                        <p>2. 按照操作说明填写并保存</p>
                        <p class="mb-3">3. 上传填写完成的数据表</p>
                        <hr/>
                        <div class="form-group">
                            <label class="text-dark" for="input-batch-file"><span class="text-danger">*</span> 数据表</label>
                            <input class="form-control" id="input-batch-file" type="file" accept=".xlsx">
                        </div>
                        <code style="white-space: pre-line;" id="batch-status"></code>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitBatchPost() {
                if ($('#input-batch-file').prop('files').length == 0) {
                    return false;
                }

                var data = new FormData();
                data.append("file", $('#input-batch-file').prop('files')[0]);
                data.append("csrf", "<?= frame::clientKey() ?>");
                data.append("action", "batch");

                $.ajax({
                    url: "<?= $pageURL ?>",
                    type: "POST",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(res) {
                        if (res.code == 403) {
                            var codeblock = $('#batch-status');
                            codeblock.html("");

                            $.each(res.text, function (index, obj) {
                                codeblock.html(codeblock.html() + obj + "\n");
                            })
                        } else {
                            location.reload();
                        }
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-batch').submit(function(e) {
                    e.preventDefault();
                    submitBatchPost();
                });
            });
        </script>
    </div>
    
    <script>
        $('#delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var username = button.data('username')
            var group = button.data('group')

            var modal = $(this)

            modal.find('#delete-modal-username').text(username)
            modal.find('#delete-modal-group').text(group)
            modal.find('#input-delete-id').val(id)
        });

        $('#add-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('add-username');
            showErrorHelp('add-group');
        })

        $('#batch-modal').on('hidden.bs.modal', function (event) {
            $('#batch-status').html("");
        })
    </script>

