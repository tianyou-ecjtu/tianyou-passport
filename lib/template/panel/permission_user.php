<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    preg_match_all("/\/panel\/permission\/(\d+)\/user/", frame::getAbsoluteURL(), $arr);
    $cid = $arr[1][0];

    $user = $route_data["user"];
    $client = new client($cid);
    
    if (!$client->check()) {
        invalid(404, "请求非法", "站点ID不存在");
    }

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "edit") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit();
        }

        $uid = db::selectFirst("SELECT `uid` FROM `TABLEPREFIX_permission_user` where `id` = ?", "i", [$id])["uid"];

        if (empty($uid)) {
            exit();
        }

        $permissionArr = [];
        $permissionList = db::selectAll("SELECT * FROM `TABLEPREFIX_permission_list` where `cid` = ?", "i", [$cid]);

        foreach ($permissionList as $permission) {
            $val = getParam("permission_".$permission["name"]);

            if (!empty($val) && is_numeric($val) && ($val == 1 || $val == -1)) {
                $permissionArr[$permission["name"]] = $val;
            }
        }

        db::update("UPDATE `TABLEPREFIX_permission_user` SET `permission` = ? where `id` = ? and `cid` = ?", "sii", [json_encode($permissionArr, JSON_UNESCAPED_UNICODE), $id, $cid]);
        user::cacheUserPermissionUpdate($uid, $cid);

        log::writeLog(2, 3, 338, "编辑用户权限");
        exit("ok");
    } else if ($_POST["action"] == "delete") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit();
        }       

        $uid = db::selectFirst("SELECT `uid` FROM `TABLEPREFIX_permission_user` where `id` = ?", "i", [$id])["uid"];

        if (empty($uid)) {
            exit();
        }
        
        db::delete("DELETE FROM `TABLEPREFIX_permission_user` where `id` = ? and `cid` = ?", "ii", [$id, $cid]);
        user::cacheUserPermissionUpdate($uid, $cid);

        log::writeLog(2, 3, 339, "删除用户权限");
        exit("ok");
    } else if ($_POST["action"] == "add") {
        $username = getParam("username");

        if (empty($username) || !validateUsername($username)) {
            exit();
        }

        $userInfo = $user->searchUser($username, ["username"], FALSE)[0];

        if (empty($userInfo)) {
            exit("username");
        }

        if (db::num_rows("SELECT `id` FROM `TABLEPREFIX_permission_user` where `uid` = ? and `cid` = ?", "ii", [$userInfo["uid"], $cid])) {
            exit("exist");
        }

        $permissionArr = [];
        $permissionList = db::selectAll("SELECT * FROM `TABLEPREFIX_permission_list` where `cid` = ?", "i", [$cid]);

        foreach ($permissionList as $permission) {
            $val = getParam("permission_".$permission["name"]);

            if (!empty($val) && is_numeric($val) && ($val == 1 || $val == -1)) {
                $permissionArr[$permission["name"]] = $val;
            }
        }

        db::insert("INSERT INTO `TABLEPREFIX_permission_user` (`uid`, `cid`, `permission`) VALUES (?, ?, ?)", "iis", [$userInfo["uid"], $cid, json_encode($permissionArr, JSON_UNESCAPED_UNICODE)]);
        user::cacheUserPermissionUpdate($userInfo["uid"], $cid);

        log::writeLog(2, 3, 337, "新增用户权限");
        exit("ok");        
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        $assets["base64"] = '';
        $assets["jquery-ui"] = '';
        return true;
    }

    $pageURL = "/panel/permission/{$cid}/user";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");

        $sql = "FROM `TABLEPREFIX_permission_user`, `TABLEPREFIX_userinfo` where (TABLEPREFIX_userinfo.`username` = ? or TABLEPREFIX_userinfo.`realname` like ? or TABLEPREFIX_permission_user.`permission` like ?) and `cid` = ? and TABLEPREFIX_userinfo.`uid` = TABLEPREFIX_permission_user.`uid`";

        $argt = "sssi";
        $argv = [$keyword, "{$keyword}%", "%\"{$keyword}\"%", $cid];
    } else {
        $sql = "FROM `TABLEPREFIX_permission_user` where `cid` = ?";

        $argt = "i";
        $argv = [$cid];
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT TABLEPREFIX_permission_user.`id` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT * {$sql} order by TABLEPREFIX_permission_user.`id` DESC LIMIT $startID, $pageSize", $argt, $argv);    
    $permissionList = db::selectAll("SELECT * FROM `TABLEPREFIX_permission_list` where `cid` = ? order by `name`", "i", [$cid]);
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">用户权限</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                        <div style="margin-left: auto !important; order: 2;">
                                            <a href="#" data-toggle="modal" data-target="#add-modal"><button class="btn btn-primary waves-effect waves-light float-right" type="button"><span class="btn-label"><i class="fas fa-plus-circle"></i></span> 新增 </button></a>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">用户名</th>
                                                    <th scope="col">姓名</th>
                                                    <th scope="col">设置权限数</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $id = $dataTotal - $startID; ?>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $user = new user($data["uid"]); ?>
                                                <tr>
                                                    <td><?= $id ?></td>
                                                    <td><a href="/panel/user/list?keyword=<?= $user->getUsername() ?>" target="_blank"><?= $user->getUsername() ?></a></td>
                                                    <td><?= $user->getUserInfo()["realname"] ?></td>
                                                    <td><?= count(json_decode($data["permission"], true)) ?></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#edit-modal" data-id="<?= $data["id"] ?>" data-username="<?= $user->getUsername() ?>" data-permission='<?= $data["permission"] ?>'><span class="fas fa-edit"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#delete-modal" data-id="<?= $data["id"] ?>" data-username="<?= $user->getUsername() ?>"><span class="fas fa-trash-alt"></span></a></td>
                                                </tr>
                                                <?php $id = $id - 1; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="form-delete" action="#">
        <input type="hidden" name="input-delete-id" id="input-delete-id" value=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>是否确认删除用户权限 <span id="delete-modal-username"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                        data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function submitDeletePost() {
            $.post('<?= $pageURL ?>', {
                csrf : "<?= frame::clientKey() ?>",
                id : $('#input-delete-id').val(),
                action : "delete"
            }, function(msg) {
                if (msg == 'ok') {
                    $('#delete-modal').modal('hide');
                    location.reload();
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-delete').submit(function(e) {
                e.preventDefault();
                submitDeletePost();
            });
        });
    </script>
    </div>

    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">编辑权限</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-edit" action="#" class="pl-3 pr-3">
                    <input type="hidden" name="input-edit-id" id="input-edit-id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-username"><span class="text-danger">*</span> 用户名</label>
                            <input class="form-control" id="input-edit-username" type="text"
                                placeholder="请输入用户名" value="" disabled>
                            <div class="invalid-feedback" id="help-edit-username"></div>
                        </div>
                        <div class="form-group mt-3">
                            <label class="text-dark mb-3" for="input-edit-permission"><span class="text-danger">*</span> 权限表</label>
                            <table class="table no-wrap text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">权限名</th>
                                        <th scope="col">随用户组</th>
                                        <th scope="col">授予</th>
                                        <th scope="col">拒绝</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($permissionList as $permission): ?>
                                    <tr>
                                        <th scope="col"><a href="#" data-toggle="tooltip" data-placement="top" title="<?= $permission["description"] ?>"><?= $permission["name"] ?></a></th>
                                        <td><input type="radio" name="edit_permission_<?= $permission["name"] ?>" value="0"></td>
                                        <td><input type="radio" name="edit_permission_<?= $permission["name"] ?>" value="1"></td>
                                        <td><input type="radio" name="edit_permission_<?= $permission["name"] ?>" value="-1"></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitEditPost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#input-edit-id').val(),
                    <?php foreach ($permissionList as $permission): ?>
                    permission_<?= $permission["name"] ?>: $('#edit-modal input[name=edit_permission_<?= $permission["name"] ?>]:checked').val(),
                    <?php endforeach ?>
                    action : "edit"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#edit-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("edit-username", "页面会话已过期");
                    } else {
                        showErrorHelp("edit-username", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-edit').submit(function(e) {
                    e.preventDefault();
                    submitEditPost();
                });
            });
        </script>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">新增权限</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-add" action="#" class="pl-3 pr-3">
                    <div class="modal-body ui-front">
                        <div class="form-group">
                            <label class="text-dark" for="input-add-username"><span class="text-danger">*</span> 用户名</label>
                            <input class="form-control" id="input-add-username" type="text"
                                placeholder="请输入用户名" value="">
                            <div class="invalid-feedback" id="help-add-username"></div>
                        </div>
                        <div class="form-group mt-3">
                            <label class="text-dark mb-3" for="input-add-permission"><span class="text-danger">*</span> 权限表</label>
                            <table class="table no-wrap text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">权限名</th>
                                        <th scope="col">随用户组</th>
                                        <th scope="col">授予</th>
                                        <th scope="col">拒绝</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($permissionList as $permission): ?>
                                    <tr>
                                        <th scope="col"><a href="#" data-toggle="tooltip" data-placement="top" title="<?= $permission["description"] ?>"><?= $permission["name"] ?></a></th>
                                        <td><input type="radio" name="add_permission_<?= $permission["name"] ?>" value="0"></td>
                                        <td><input type="radio" name="add_permission_<?= $permission["name"] ?>" value="1"></td>
                                        <td><input type="radio" name="add_permission_<?= $permission["name"] ?>" value="-1"></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function(){
                $("#input-add-username").autocomplete({
                    source: function(request, response){
                        $.post('/apiv2/autocomplete/searchUser', {
                            csrf : "<?= frame::clientKey() ?>",
                            data: Base64.encode(JSON.stringify({keyword: $('#input-add-username').val()}))
                        }, function(data) {
                            res = JSON.parse(data)
                            res = JSON.parse(Base64.decode(res.data))
                            response(res)
                        });
                    },
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<div>" + item.value + " <small><i>(" + item.realname + "," + item.year + ")</i></small></div>" )
                    .appendTo( ul );
                };
            });

            function validateAddPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('add-username', validateUsername);
                return ok;
            }

            function submitAddPost() {
                if (!validateAddPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    username : $('#input-add-username').val(),
                    <?php foreach ($permissionList as $permission): ?>
                    permission_<?= $permission["name"] ?>: $('#add-modal input[name=add_permission_<?= $permission["name"] ?>]:checked').val(),
                    <?php endforeach ?>
                    action : "add"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#add-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("add-username", "页面会话已过期");
                    } else if (msg == 'username') {
                        showErrorHelp("add-username", "用户名不存在");
                    }  else if (msg == 'exist') {
                        showErrorHelp("add-username", "用户权限已存在");
                    } else {
                        showErrorHelp("add-username", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-add').submit(function(e) {
                    e.preventDefault();
                    submitAddPost();
                });
            });
        </script>
    </div>
    
    <script>
        $('#delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var username = button.data('username')

            var modal = $(this)

            modal.find('#delete-modal-username').text(username)
            modal.find('#input-delete-id').val(id)
        });

        $('#edit-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var username = button.data('username')
            var permission = button.data('permission')

            var modal = $(this)
            
            modal.find('#input-edit-id').val(id)
            modal.find('#input-edit-username').val(username)

            $("#edit-modal input[value=0]").prop("checked", true);

            for (var key in permission) {
                $("#edit-modal input[name=edit_permission_"+key+"][value=0]").prop("checked", false);
                $("#edit-modal input[name=edit_permission_"+key+"][value="+permission[key]+"]").prop("checked", true);
            }
        });

        $('#add-modal').on('show.bs.modal', function (event) {
            $("#add-modal input").prop("checked", false);
            $("#add-modal input[value=0]").prop("checked", true);
        });


        $('#edit-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('edit-username');
            $("#edit-modal input").prop("checked", false);
        })

        $('#add-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('add-username');
            $("#add-modal input").prop("checked", false);
        })
    </script>

