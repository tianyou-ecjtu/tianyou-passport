<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "add") {
        $username = getParam("username");
        $realname = getParam("realname");
        $group = getParam("group");

        if (empty($username) || empty($realname) || empty($group) || mb_strlen($realname) > 10 || mb_strlen($username) > 20) {
            exit();
        }        

        $groupInfo = $user->searchGroup($group, FALSE)[0];

        if (empty($groupInfo)) {
            exit("group");
        }

        if (db::num_rows("SELECT `id` FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` = ?", "s", [$username])) {
            exit("exist");
        }

        db::insert("INSERT INTO `TABLEPREFIX_user_group_preset` (`ecjtuID`, `gid`, `description`) VALUES (?, ?, ?)", "sis", [$username, $groupInfo["id"], $realname]);

        log::writeLog(2, 3, 309, "新增用户组预置");
        exit("ok");
    } else if ($_POST["action"] == "delete") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit("");
        }

        db::delete("DELETE FROM `TABLEPREFIX_user_group_preset` where `id` = ?", "i", [$id]);       
        
        log::writeLog(2, 3, 310, "删除用户组预置");   
        exit("ok");
    }

    load3rdparty("simplecache");
    load3rdparty("phpSpreadSheet");

    use PhpOffice\PhpSpreadsheet\Spreadsheet;

    if ($_POST["action"] == "batch") {
        $path = $_FILES["file"]["tmp_name"];
        $mime = $_FILES["file"]["type"];
        $filename = $_FILES["file"]["name"];

        $code = 200;
        $text = [];
        $sql = [];

        if (end(explode(".", $filename)) != "xlsx" || $mime != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            invalid(410, "请求非法", "上传数据表格式错误");

            $code = 403;
            array_push($text, "文件类型错误");
        } else {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($path);

            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();

            if ($worksheet->getCellByColumnAndRow(8, 3) != "b8c8745f3bf93ce1dcb7f36178904efd") {
                $code = 403;
                array_push($text, "数据表版本无效");
            } else {
                for ($row = 3; $row <= $highestRow; ++$row) {
                    $index = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $action = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $platform = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $username = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $description = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $group = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
    
                    if (!empty($index) || !empty($action) || !empty($username) || !empty($group) || !empty($description)) {
                        if (empty($index) || empty($action) || empty($username) || empty($group) || empty($description)) {
                            $code = 403;
                            array_push($text, "第{$row}行 - 表格未填写完整");
                            continue;
                        }

                        if ($action != "增加" && $action != "删除") {
                            $code = 403;
                            array_push($text, "第{$row}行 - 不存在的操作 {$action}");
                            continue;
                        }

                        if ($platform != "ECJTU") {
                            $code = 403;
                            array_push($text, "第{$row}行 - 不存在的三方平台 {$platform}");
                            continue;
                        }

                        if (mb_strlen($username) > 20) {
                            $code = 403;
                            array_push($text, "第{$row}行 - 三方平台账号长度超限 {$platform}");
                            continue;
                        }

                        if (mb_strlen($description) > 10) {
                            $code = 403;
                            array_push($text, "第{$row}行 - 姓名长度超限 {$platform}");
                            continue;
                        }

                        $groupInfoCache = [];

                        if (!$groupInfoCache[$group]) {
                            $groupInfoCache[$group] = $user->searchGroup($group, FALSE)[0];

                            if (empty($groupInfoCache[$group])) {
                                $code = 403;
                                array_push($text, "第{$row}行 - 不存在的用户组 {$group}");
                                continue;                                
                            }
                        }

                        $cnt = db::num_rows("SELECT `id` FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` = ?", "s", [$username]);

                        if ($action == "增加") {
                            if ($cnt > 0) {
                                $code = 403;
                                array_push($text, "第{$row}行 - 权限预置已存在 {$username}");
                                continue;        
                            }            
                            
                            array_push($sql, 0, "INSERT INTO `TABLEPREFIX_user_group_preset` (`ecjtuID`, `description`, `gid`) VALUES (?, ?, ?)", "ssi", [$username, $description, $groupInfoCache[$group]["id"]]);
                        } else {
                            if ($cnt == 0) {
                                $code = 403;
                                array_push($text, "第{$row}行 - 权限预置不存在 {$username}/{$group}");
                                continue;        
                            }            
                            
                            array_push($sql, 1, "DELETE FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` = ? and `gid` = ?", "si", [$username, $groupInfoCache[$group]["id"]]);                            
                        }
                    }
                }
            }
        }

        if ($code == 200) {
            for ($i=0; $i<count($sql); $i+=4) {
                $action = $sql[$i];
                $sql_text = $sql[$i+1];
                $argc = $sql[$i+2];
                $argv = $sql[$i+3];

                if ($action == 0) {
                    db::insert($sql_text, $argc, $argv);
                } else {
                    db::delete($sql_text, $argc, $argv);
                }
            }

            log::writeLog(2, 3, 311, "批量进行用户组预置"); 
        }

        exit(json_encode(["code" => $code, "text" => $text], JSON_UNESCAPED_UNICODE));
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        $assets["base64"] = '';
        $assets["jquery-ui"] = '';
        return true;
    }

    $pageURL = "/panel/group/preset";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");
        $gid = db::selectFirst("SELECt `id` FROM `TABLEPREFIX_groups` where `name` = ?", "s", [$keyword])["id"];

        $sql = "FROM `TABLEPREFIX_user_group_preset` where `ecjtuID` like ? or `description` like ? or `gid` = ?";

        $argt = "ssi";
        $argv = ["{$keyword}%", "{$keyword}%", $gid];
    } else {
        $sql = "FROM `TABLEPREFIX_user_group_preset`";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT `id` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT * {$sql} ORDER BY `id` DESC LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">用户组预置</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                        <div style="margin-left: auto !important; order: 2;">
                                            <a href="#" data-toggle="modal" data-target="#add-modal"><button class="btn btn-primary waves-effect waves-light float-right ml-2" type="button"><span class="btn-label"><i class="fas fa-plus-circle"></i></span> 新增 </button></a>
                                            <a href="#" data-toggle="modal" data-target="#batch-modal"><button class="btn btn-primary waves-effect waves-light float-right" type="button"><span class="btn-label"><i class="fas fa-upload"></i></span> 批量操作 </button></a>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">三方类型</th>
                                                    <th scope="col">三方账号</th>
                                                    <th scope="col">姓名</th>
                                                    <th scope="col">用户组</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $id = $dataTotal - $startID; ?>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $groupInfo = user::getUserGroupInfo($data["gid"]); ?>
                                                <tr>
                                                    <td><?= $id ?></td>
                                                    <td>智慧交大</td>
                                                    <td><?= $data["ecjtuID"] ?></td>
                                                    <td><?= $data["description"] ?></td>
                                                    <td><a href="/panel/group/list?keyword=<?= $groupInfo['name'] ?>" target="_blank"><?= $groupInfo["name"] ?></a></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#delete-modal" data-id="<?= $data["id"] ?>" data-username="<?= $data["ecjtuID"] ?>" data-group="<?= $groupInfo['name'] ?>" data-description="<?= $data["description"] ?>"><span class="fas fa-trash-alt"></span></a> </td>
                                                </tr>
                                                <?php $id = $id - 1; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="form-delete" action="#">
        <input type="hidden" name="input-delete-id" id="input-delete-id" value=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>是否确认删除 <span id="delete-modal-username"></span>/<span id="delete-modal-description"></span> 的预置用户组 <span id="delete-modal-group"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                        data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function submitDeletePost() {
            $.post('<?= $pageURL ?>', {
                csrf : "<?= frame::clientKey() ?>",
                id : $('#input-delete-id').val(),
                action : "delete"
            }, function(msg) {
                if (msg == 'ok') {
                    $('#delete-modal').modal('hide');
                    location.reload();
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-delete').submit(function(e) {
                e.preventDefault();
                submitDeletePost();
            });
        });
    </script>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">添加用户组预置</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-add" action="#" class="pl-3 pr-3">
                    <div class="modal-body ui-front">
                        <div class="form-group">
                            <label class="text-dark" for="input-add-username"><span class="text-danger">*</span> 三方账号（限20字）</label>
                            <input class="form-control" id="input-add-username" type="text"
                                placeholder="请输入学工号" value="" maxlength="20">
                            <div class="invalid-feedback" id="help-add-username"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-realname"><span class="text-danger">*</span> 姓名（限10字）</label>
                            <input class="form-control" id="input-add-realname" type="text"
                                placeholder="请输入姓名" value="" maxlength="10">
                            <div class="invalid-feedback" id="help-add-realname"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-group"><span class="text-danger">*</span> 用户组</label>
                            <input class="form-control" id="input-add-group" type="text"
                                placeholder="请输入用户组名称" value="">
                            <div class="invalid-feedback" id="help-add-group"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function(){
                $("#input-add-group").autocomplete({
                    source: function(request, response){
                        $.post('/apiv2/autocomplete/searchGroup', {
                            csrf : "<?= frame::clientKey() ?>",
                            data: Base64.encode(JSON.stringify({keyword: $('#input-add-group').val()}))
                        }, function(data) {
                            res = JSON.parse(data)
                            res = JSON.parse(Base64.decode(res.data))
                            response(res)
                        });
                    },
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<div>" + item.value + " <small><i>(" + item.description + ")</i></small></div>" )
                    .appendTo( ul );
                };
            });

            function validateAddPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('add-username', validateNotEmpty, '三方账号');
                ok &= getFormErrorAndShowHelp('add-realname', validateNotEmpty, '姓名');
                ok &= getFormErrorAndShowHelp('add-group', validateNotEmpty, '用户组');
                return ok;
            }

            function submitAddPost() {
                if (!validateAddPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    username : $('#input-add-username').val(),
                    realname : $('#input-add-realname').val(),
                    group : $('#input-add-group').val(),
                    action : "add"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#add-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("add-username", "页面会话已过期");
                    } else if (msg == 'group') {
                        showErrorHelp("add-group", "用户组名称不存在");
                    } else if (msg == 'exist') {
                        showErrorHelp("add-username", "用户预置记录已存在");
                    } else {
                        showErrorHelp("add-username", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-add').submit(function(e) {
                    e.preventDefault();
                    submitAddPost();
                });
            });
        </script>
    </div>

    <div id="batch-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">批量操作</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-batch" action="#" class="pl-3 pr-3">
                    <div class="modal-body">
                        <p>1. <a href="<?= getCDNLink("assets/用户组预置.xlsx") ?>">点击此处</a>下载数据表</p>
                        <p>2. 按照操作说明填写并保存</p>
                        <p class="mb-3">3. 上传填写完成的数据表</p>
                        <hr/>
                        <div class="form-group">
                            <label class="text-dark" for="input-batch-file"><span class="text-danger">*</span> 数据表</label>
                            <input class="form-control" id="input-batch-file" type="file" accept=".xlsx">
                        </div>
                        <code style="white-space: pre-line;" id="batch-status"></code>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitBatchPost() {
                if ($('#input-batch-file').prop('files').length == 0) {
                    return false;
                }

                var data = new FormData();
                data.append("file", $('#input-batch-file').prop('files')[0]);
                data.append("csrf", "<?= frame::clientKey() ?>");
                data.append("action", "batch");

                $.ajax({
                    url: "<?= $pageURL ?>",
                    type: "POST",
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(res) {
                        if (res.code == 403) {
                            var codeblock = $('#batch-status');
                            codeblock.html("");

                            $.each(res.text, function (index, obj) {
                                codeblock.html(codeblock.html() + obj + "\n");
                            })
                        } else {
                            location.reload();
                        }
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-batch').submit(function(e) {
                    e.preventDefault();
                    submitBatchPost();
                });
            });
        </script>
    </div>
    
    <script>
        $('#delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var username = button.data('username')
            var group = button.data('group')
            var description = button.data('description')

            var modal = $(this)

            modal.find('#delete-modal-username').text(username)
            modal.find('#delete-modal-description').text(description)
            modal.find('#delete-modal-group').text(group)
            modal.find('#input-delete-id').val(id)
        });

        $('#add-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('add-username');
            showErrorHelp('add-group');
            showErrorHelp('add-realname');
        })

        $('#batch-modal').on('hidden.bs.modal', function (event) {
            $('#batch-status').html("");
        })
    </script>

