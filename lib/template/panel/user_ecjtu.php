<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "remove") {
        $id = getParam("id");

        $removeUser = new user($id);

        if (empty($id) || !is_numeric($id)) {
            log::writeLog(2, 2, 401, "请求非法", "请求参数不合法");
            exit();
        }

        if (!$removeUser->checkExist()) {
            log::writeLog(2, 2, 402, "请求非法", "用户 ID 不存在");
            exit();
        }

        db::update("UPDATE `TABLEPREFIX_userinfo` SET `ecjtuID` = NULL where `uid` = ?", "i", [$id]);
        user::cacheUserInfoUpdate($id);
        user::cacheUserGroupUpdate($id);
        
        log::writeLog(2, 3, 305, "解绑用户智慧交大账号");
        exit("ok");   
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        return true;
    }

    $pageURL = "/panel/user/ecjtu";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");

        $sql = "FROM `TABLEPREFIX_userinfo` where (`username` = ? or `realname` = ? or `idcard` = ? or `ecjtuID` like ?) and `isThirdpart` = 0 and `ecjtuID` is NOT NULL";

        $argt = "ssss";
        $argv = [$keyword, $keyword, $keyword, $keyword."%"];
    } else {
        $sql = "FROM `TABLEPREFIX_userinfo` where `isThirdpart` = 0 and `ecjtuID` is NOT NULL";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT `uid` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT `uid` {$sql} order by `uid` DESC LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">智慧交大绑定</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">用户名</th>
                                                    <th scope="col">姓名</th>
                                                    <th scope="col">学工号</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $user_data = new user($data["uid"]);?>
                                                
                                                <tr>
                                                    <td><?= $user_data->getID() ?></td>
                                                    <td><a href="/panel/user/list?keyword=<?= $user_data->getUsername() ?>" target="_blank"><?= $user_data->getUsername() ?></a></td>
                                                    <td><?= $user_data->getUserInfo()["realname"] ?></td>
                                                    <td><?= $user_data->getUserInfo()["ecjtuID"] ?></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#remove-modal" data-id="<?= $user_data->getID() ?>" data-username="<?= $user_data->getUsername() ?>" ><span class="fas fa-trash-alt"></span></a></td>
                                                </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="remove-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-remove" action="#">
                    <input type="hidden" name="remove-id" id="remove-id" value=""/>
                    <div class="modal-header">
                        <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>是否确认解绑账户 <span id="remove-username"></span> 的智慧交大账号</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function submitRemovePost() {
                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#remove-id').val(),
                    action : "remove"
                }, function(msg) {
                    if (msg == 'ok') {
                        location.reload()
                        $('#block-modal').modal('hide');
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-remove').submit(function(e) {
                    e.preventDefault();
                    submitRemovePost();
                });
            });
        </script>
    </div>

    <script>
        $('#remove-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)

            var id = button.data('id')
            var username = button.data('username')

            modal.find('#remove-username').text(username)
            modal.find('#remove-id').val(id)

        });

        $('#remove-modal').on('hidden.bs.modal', function (event) {
            var modal = $(this)

            modal.find('#remove-username').text("")
            modal.find("#remove-id").val("")
        });

    </script>