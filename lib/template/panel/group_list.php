<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if (!empty($_POST["action"])) {
        if (!checkCSRF()) {
            exit();
        }
    }

    if ($_POST["action"] == "delete") {
        $id = getParam("id");

        if (empty($id) || !is_numeric($id)) {
            exit("");
        }

        db::delete("DELETE FROM `TABLEPREFIX_groups` where `id` = ?", "i", [$id]);       
        db::delete("DELETE FROM `TABLEPREFIX_user_group` where `gid` = ?", "i", [$id]);     
        db::delete("DELETE FROM `TABLEPREFIX_user_group_preset` where `gid` = ?", "i", [$id]);    
        db::delete("DELETE FROM `TABLEPREFIX_permission_group` where `id` = ?", "i", [$id]);     
        user::cacheUserGroupInfoUpdate($id);
        user::cacheUserGroupPermissionUpdateAll($id);

        log::writeLog(2, 3, 308, "删除用户组");
        exit("ok");
    } else if ($_POST["action"] == "edit") {
        $id = getParam("id");
        $name = getParam("name");
        $description = getParam("description");

        if (empty($name) || empty($description)) {
            exit();
        }        

        if (db::num_rows("SELECT `id` FROm `TABLEPREFIX_groups` where `name` = ? and `id` != ?", "si", [$name, $id])) {
            exit("name");
        }

        db::update("UPDATE `TABLEPREFIX_groups` SET `name` = ?, `description` = ? where `id` = ?", "ssi", [$name, $description, $id]);

        log::writeLog(2, 3, 307, "编辑用户组");
        user::cacheUserGroupInfoUpdate($id);
        exit("ok");
    } else if ($_POST["action"] == "add") {
        $name = getParam("name");
        $description = getParam("description");

        if (empty($name) || empty($description)) {
            exit();
        }        

        if (db::num_rows("SELECT `id` FROm `TABLEPREFIX_groups` where `name` = ?", "s", [$name])) {
            exit("name");
        }

        db::insert("INSERT INTO `TABLEPREFIX_groups` (`name`, `description`) VALUES (?, ?)", "ss", [$name, $description]);

        log::writeLog(2, 3, 306, "新增用户组");
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets["table"] = '';
        return true;
    }

    $pageURL = "/panel/group/list";
    $argt = "";
    $argv = [];

    if (isset($_GET["keyword"])) {
        $keyword = getParam("keyword", "GET");

        $sql = "FROM `TABLEPREFIX_groups` where `name` like ? or `description` like ?";

        $argt = "ss";
        $argv = ["%{$keyword}%", "%{$keyword}%"];
    } else {
        $sql = "FROM `TABLEPREFIX_groups`";
    }

    $page = getPageID();
    $pageSize = 10;

    $dataTotal = db::num_rows("SELECT `id` {$sql}", $argt, $argv);

    $pageTotal = floor($dataTotal / $pageSize);
    
    if($dataTotal % $pageSize != 0){
        $pageTotal = $pageTotal + 1;
    }

    $pageTotal = max($pageTotal, 1);

    if($page != 1 && $page > $pageTotal){
        header("Location:{$pageURL}?page={$pageTotal}");
        exit;
    }
    if($page < 1){
        header("Location:{$pageURL}?page=1");
        exit;
    }
    
    /**
     * 查询数据
     */
    
    $startID = ($page - 1) * $pageSize;
    $endID = min($dataTotal, $page * $pageSize);

    $datas = db::selectAll("SELECT `id` {$sql} LIMIT $startID, $pageSize", $argt, $argv);    
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">用户组列表</h4>
                                <div class="d-none d-md-block mt-4">
                                    <form id="form-search" class="form-inline" method="get">
                                        <div id="form-group-search" class="form-group">
                                            <label for="input-s" class="control-label">关键词</label>
                                            <input type="text" class="form-control input-sm ml-2" name="keyword" id="input-keyword" maxlength="40" style="width:15em" value="<?= $keyword ?>">
                                        </div>
                                        <div class="ml-2">
                                            <button type="submit" id="submit-search" class="btn btn-default btn-primary">搜索</button>
                                        </div>
                                        <div style="margin-left: auto !important; order: 2;">
                                            <a href="#" data-toggle="modal" data-target="#add-modal"><button class="btn btn-primary waves-effect waves-light float-right" type="button"><span class="btn-label"><i class="fas fa-plus-circle"></i></span> 新增 </button></a>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                    $('#form-search').submit(function(e) {
                                        e.preventDefault();
                                        
                                        url = '<?= $pageURL ?>';
                                        qs = [];
                                        $(['keyword']).each(function () {
                                            if ($('#input-' + this).val()) {
                                                qs.push(this + '=' + encodeURIComponent($('#input-' + this).val()));
                                            }
                                        });
                                        if (qs.length > 0) {
                                            url += '?' + qs.join('&');
                                        }
                                        location.href = url;
                                    });
                                    </script>
                                </div>
                                <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                    <div class="row">
                                        <table class="table table-hover no-wrap text-center mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">名称</th>
                                                    <th scope="col">描述</th>
                                                    <th scope="col">用户数</th>
                                                    <th scope="col">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($datas as $data): ?>
                                                <?php $usergroup_info = user::getUserGroupInfo($data["id"]);?>
                                                
                                                <tr>
                                                    <td><?= $usergroup_info["id"] ?></td>
                                                    <td><?= $usergroup_info["name"] ?></td>
                                                    <td><?= $usergroup_info["description"] ?></td>
                                                    <td><a href="/panel/group/preset?keyword=<?= $usergroup_info["name"] ?>" target="_blank"><?= user::getUserGroupPresetCnt($data["id"]) ?></a> + <a href="/panel/group/assign?keyword=<?= $usergroup_info["name"] ?>" target="_blank"><?= user::getUserGroupAssignCnt($data["id"]) ?></a></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#edit-modal" data-id="<?= $usergroup_info["id"] ?>" data-name="<?= $usergroup_info["name"] ?>" data-description="<?= $usergroup_info["description"] ?>"><span class="fas fa-edit"></span></a>&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#delete-modal" data-id="<?= $usergroup_info["id"] ?>" data-name="<?= $usergroup_info["name"] ?>"><span class="fas fa-trash-alt"></span></a></td>
                                                </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info d-none d-lg-block" id="zero_config_info" role="status" aria-live="polite">
                                                <?php if ($dataTotal): ?>
                                                当前第 <?= $startID + 1 ?> ~ <?= $endID ?> 条数据，共 <?= $dataTotal ?> 条数据
                                                <?php else: ?>
                                                共 <?= $dataTotal ?> 条数据
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous" id="zero_config_previous"><a href="<?= $pageURL ?>?page=1"
                                                            aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">最前页</a></li>
                                                    <?php for ($i=-3;$i<=3; $i++): ?>
                                                    <?php if ($page + $i > 0 && $page + $i <= $pageTotal): ?>
                                                    <?php if ($i == 0): ?>
                                                    <li class="paginate_button page-item active"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php else: ?>
                                                    <li class="paginate_button page-item"><a href="<?= getPageURL($page+$i) ?>" aria-controls="zero_config" class="page-link"><?= $page + $i ?></a></li>
                                                    <?php endif ?>
                                                    <?php endif ?>
                                                    <?php endfor ?>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="<?= getPageURL($pageTotal) ?>" aria-controls="zero_config" class="page-link">最后页</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="form-delete" action="#">
        <input type="hidden" name="input-delete-id" id="input-delete-id" value=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">操作确认</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>是否确认删除用户组 <span id="delete-modal-name"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                        data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function submitDeletePost() {
            $.post('<?= $pageURL ?>', {
                csrf : "<?= frame::clientKey() ?>",
                id : $('#input-delete-id').val(),
                action : "delete"
            }, function(msg) {
                if (msg == 'ok') {
                    $('#delete-modal').modal('hide');
                    location.reload();
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-delete').submit(function(e) {
                e.preventDefault();
                submitDeletePost();
            });
        });
    </script>
    </div>

    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">编辑用户组</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-edit" action="#" class="pl-3 pr-3">
                    <input type="hidden" name="input-edit-id" id="input-edit-id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-name"><span class="text-danger">*</span> 名称</label>
                            <input class="form-control" id="input-edit-name" type="text"
                                placeholder="请输入用户组名称" value="">
                            <div class="invalid-feedback" id="help-edit-name"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-edit-description"><span class="text-danger">*</span> 描述</label>
                            <textarea class="form-control" rows="5" maxlength="30" id="input-edit-description" placeholder="请输入用户组描述"></textarea>
                            <div class="invalid-feedback" id="help-edit-description"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function validateEditPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('edit-name', validateNotEmpty, '名称');
                ok &= getFormErrorAndShowHelp('edit-description', validateNotEmpty, '描述');
                return ok;
            }

            function submitEditPost() {
                if (!validateEditPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    id : $('#input-edit-id').val(),
                    name : $('#input-edit-name').val(),
                    description : $('#input-edit-description').val(),
                    action : "edit"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#edit-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("edit-name", "页面会话已过期");
                    } else if (msg == 'name') {
                        showErrorHelp("edit-name", "用户组名称已存在");
                    } else {
                        showErrorHelp("edit-name", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-edit').submit(function(e) {
                    e.preventDefault();
                    submitEditPost();
                });
            });
        </script>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">添加用户组</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form id="form-add" action="#" class="pl-3 pr-3">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-dark" for="input-add-name"><span class="text-danger">*</span> 名称</label>
                            <input class="form-control" id="input-add-name" type="text"
                                placeholder="请输入用户组名称" value="">
                            <div class="invalid-feedback" id="help-add-name"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="input-add-description"><span class="text-danger">*</span> 描述</label>
                            <textarea class="form-control" rows="5" maxlength="30" id="input-add-description" placeholder="请输入用户组描述"></textarea>
                            <div class="invalid-feedback" id="help-add-description"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function validateAddPost() {
                var ok = true;
                ok &= getFormErrorAndShowHelp('add-name', validateNotEmpty, '名称');
                ok &= getFormErrorAndShowHelp('add-description', validateNotEmpty, '描述');
                return ok;
            }

            function submitAddPost() {
                if (!validateAddPost()) {
                    return false;
                }

                $.post('<?= $pageURL ?>', {
                    csrf : "<?= frame::clientKey() ?>",
                    name : $('#input-add-name').val(),
                    description : $('#input-add-description').val(),
                    action : "add"
                }, function(msg) {
                    if (msg == 'ok') {
                        $('#add-modal').modal('hide');
                        location.reload();
                    } else if (msg == 'expired') {
                        showErrorHelp("add-name", "页面会话已过期");
                    } else if (msg == 'name') {
                        showErrorHelp("add-name", "用户组名称已存在");
                    } else {
                        showErrorHelp("add-name", "未知错误");
                    }
                });

                return true;
            }

            $(document).ready(function() {
                $('#form-add').submit(function(e) {
                    e.preventDefault();
                    submitAddPost();
                });
            });
        </script>
    </div>
    
    <script>
        $('#delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var name = button.data('name')

            var modal = $(this)

            modal.find('#delete-modal-name').text(name)
            modal.find('#input-delete-id').val(id)
        });

        $('#edit-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var name = button.data('name')
            var description = button.data('description')

            var modal = $(this)
            
            modal.find('#input-edit-id').val(id)
            modal.find('#input-edit-name').val(name)
            modal.find('#input-edit-description').text(description)
        });

        $('#edit-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('edit-name');
            showErrorHelp('edit-description');
        })

        $('#add-modal').on('hidden.bs.modal', function (event) {
            showErrorHelp('add-name');
            showErrorHelp('add-description');
        })
    </script>

