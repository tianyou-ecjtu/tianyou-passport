<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if($assetsFlag == 1){
        $assets["panel"] = '';
        $assets['ip'] = '';
        return true;
    }

    $id = $_GET["id"];

    if (empty($id) || !is_numeric($id)) {
        invalid(401, "请求非法", "日志 ID 不合法");
    }

    $data = Cache::getArray("log_detail_{$id}", function($id) {
        return db::selectFirst("SELECT * FROM `TABLEPREFIX_logs` where `id` = ?", "i", array($id));
    }, array($id));

    if (empty($data)) {
        invalid(403, "请求非法", "日志 ID 不存在");
    }

    $exec_stack = json_decode(log::decrypt($data["execStack"]), true);
    $exec_stack_size = count($exec_stack);

    for ($i = 0; $i < $exec_stack_size; $i++) {
        $exec_stack[$i]["file"] = frame::styleFilepath($exec_stack[$i]["file"]);

        if (!empty($exec_stack[$i]["args"])) {
            $arg_size = count($exec_stack[$i]["args"]);

            for ($j = 0; $j < $arg_size; $j++) {
                $exec_stack[$i]["args"][$j] = frame::styleFilepath($exec_stack[$i]["args"][$j]);
            }
        }
    }
?>


    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <?php route::include("template/panel/topbar") ?>
        <?php route::include("template/panel/sidemenu") ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">事件 <?= $data["logCode"] ?></h4>
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <?= $data["logTitle"] ?>
                                    </div>
                                </div>
                                <h4 class="card-title">基本信息</h4>
                                <div class="table-responsive mt-4">
                                    <table class="table table-striped text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="vertical-align: middle; min-width:150px">项目</th>
                                                <th scope="col" style="vertical-align: middle;">值</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;">日期</td>
                                                <td style="vertical-align: middle;"><?= $data["date"] ?></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">URL</td>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= log::decrypt($data["logUrl"]) ?></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">IP</td>
                                                <td style="vertical-align: middle;"><a href="#" data-toggle="tooltip" data-placement="top" title="<?= frame::getIPLocation($data["guestIP"]); ?>"><?= $data["guestIP"]; ?></a></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">用户名</td>
                                                <td style="vertical-align: middle;">
                                                    <?php
                                                        if(!$data["uid"]) {
                                                            echo "未登录";
                                                        } else {
                                                            $user = new user($data["uid"]);
                                                            echo $user->getUsername();
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">操作系统</td>
                                                <td style="vertical-align: middle;"><?= $data["guestOS"] ?></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">浏览器</td>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= $data["guestBrowser"] ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h4 class="card-title">事件快照</h4>
                                <div class="card mt-4">
                                    <div class="card-body">
                                    <?= !empty($data["logInfo"]) ? $data["logInfo"] : "无快照"; ?>
                                    </div>
                                </div>
                                <h4 class="card-title">调试信息</h4>
                                <div class="card mt-4">
                                    <div class="card-body">
                                    Log ID / <?= $data["logHash"] ?> <br/>
                                    <?= json_encode($exec_stack, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) ?>
                                    </div>
                                </div>
                                <h4 class="card-title">请求参数</h4>
                                <div class="table-responsive mt-4">
                                    <table class="table table-hover  text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">项目</th>
                                                <th scope="col">值</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="vertical-align: middle;">GET</th>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= log::decrypt($data["fieldGet"]) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" style="vertical-align: middle;">POST</th>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= log::decrypt($data["fieldPost"]) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" style="vertical-align: middle;">COOKIE</th>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= log::decrypt($data["fieldCookie"]) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" style="vertical-align: middle;">SESSION</th>
                                                <td style="vertical-align: middle;word-break:break-all; word-wrap:break-all;"><?= log::decrypt($data["fieldSession"]) ?></td>
                                            </tr>                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>