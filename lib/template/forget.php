<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    preg_match_all("/\/forget\/(\S+)/", frame::getAbsoluteURL(), $tokenArray);
    $token = $tokenArray[1][0];

    if (!empty($token)) {
        if (!$userinfo = db::selectFirst("SELECT `id`, `forgetPasswordExpire` FROM `TABLEPREFIX_users` where `forgetPasswordToken` = ?", "s", [$token])) {
            echo "<script>alert('链接无效或已过期');window.location.href='/forget';</script>";
            exit;
        }

        if (time() > $userinfo["forgetPasswordExpire"]) {
            echo "<script>alert('链接无效或已过期');window.location.href='/forget';</script>";
            exit;            
        }
    }

    if ($_POST["forget"] == "forget") {
        if (!checkCSRF()) {
            exit("expired");
        }

        if (!checkCaptcha()) {
            exit("recaptcha");
        }
    
        $username = getParam("username");

        if (empty($username)) {
            exit("");
        }

        if (!validateUsername($username) && !validateIDCard($username)) {
            exit("username");
        }

        $users = user::searchUser($username, ["username", "idcard"], FALSE);

        if (empty($users)) {
            exit("username");
        }

        $user = new user($users[0]["uid"]);

        if (!$user->isVerifyEmailExist()) {
            exit("email");
        }

        $user->forgetPassword();

        log::writeLog(2, 3, 204, "请求重置密码");
        
        exit(json_encode(["res" => "ok", "email" => hideEmail($user->getUserInfo()["email"])]));
    } else if ($_POST["forget"] == "password") {
        $token = getParam("token");
        $newpassword = getParam("password");
        $newpassword_repeat = getParam("password_repeat");

        if (empty($newpassword) || empty($newpassword_repeat) || !validatePassword($newpassword)) {
            exit("");
        }

        if ($newpassword != $newpassword_repeat) {
            exit("password");
        }

        if (!$userinfo = db::selectFirst("SELECT `id`, `forgetPasswordExpire` FROM `TABLEPREFIX_users` where `forgetPasswordToken` = ?", "s", [$token])) {
            exit("");
        }
        
        if (time() > $userinfo["forgetPasswordExpire"]) {
            exit("");
        }

        $uid = $userinfo["id"];
        $user = new user($uid);
        $user->updatePassword($newpassword);

        db::update("update `TABLEPREFIX_users` SET `forgetPasswordToken` = NULL, `forgetPasswordExpire` = NULL where `id` = ?", "i", [$uid]);
        user::cacheUserInfoUpdate($uid);

        log::writeLog(2, 3, 205, "重置密码");
        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["login"] = '';
        $assets["captcha"] = '';
        return true;
    }
?>

<body class='snippet-body'>
    <div class="center-outer">
        <div class="center-inner container-xxl px-1 px-md-5 px-lg-1 px-xl-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-4 border-line"> <img src="<?= getCDNLink("images/logo.png") ?>" class="image"> </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 px-4">
                            <div class="text row px-3 mb-4">
                                    <h4 class="or text-center">找回账号 - <?= frame::configGet("site/shortname") ?></h4>
                                
                            </div>
                            <?php if(!empty($token)): ?>
                                <form id="form-register">
                                    <input type="hidden" id="input-token" value="<?= $token ?>"/>
                                    <div class="row px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">密码</h6>
                                        </label> <input class="form-control" id="input-password" type="password" placeholder="请输入密码"> <div class="invalid-feedback" id="help-password"></div> </div>
                                        
                                    <div class="row px-3 mt-2"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">确认密码</h6>
                                        </label> <input class="form-control" type="password" id="input-password-repeat" placeholder="请再次输入密码"> <div class="invalid-feedback" id="help-password-repeat"></div> </div>
                                    <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">重置</button> </div>
                                    <div class="row mb-2 px-3"> <small class="font-weight-bold">已有账号? <a class="text-danger"  href="/">点击登录</a></small> </div>
                                </form>            
                                <script type="text/javascript">
                                    function validateRegisterPost() {
                                        var ok = true;
                                        ok &= getFormErrorAndShowHelp('password', validatePassword);
                                        ok &= getFormErrorAndShowHelp('password-repeat', validatePassword);
                                        return ok;
                                    }
                                    function submitRegisterPost() {
                                        if (!validateRegisterPost()) {
                                            return false;
                                        }

                                        $.post('/forget', {
                                            csrf : "<?= frame::clientKey() ?>",
                                            password : $('#input-password').val(),
                                            password_repeat : $('#input-password-repeat').val(),
                                            token : $('#input-token').val(),
                                            forget : "password"
                                        }, function(msg) {
                                            if (msg == 'ok') {
                                                alert("重置成功");
                                                window.location.href = "/";
                                            } else if (msg == 'expired') {
                                                showErrorHelp("username", "页面会话已过期");
                                            } else if (msg == 'password') {
                                                showErrorHelp("password", "两次密码输入不相同");
                                                showErrorHelp("password-repeat", "两次密码输入不相同");
                                            } else {
                                                showErrorHelp("username", "未知错误");
                                            }
                                        });
                                        return true;
                                    }

                                    $(document).ready(function() {
                                        $('#form-register').submit(function(e) {
                                            e.preventDefault();
                                            submitRegisterPost();
                                        });
                                    });
                                </script>  
                            <?php else: ?>
                                <form id="form-forget">
                                    <div class="row px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">账号凭据</h6>
                                        </label> <input class="form-control" id="input-username" type="text" placeholder="请输入用户名或身份证号"> <div class="invalid-feedback" id="help-username"></div> </div>
                                    <div class="row px-3 mt-2"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">人机验证</h6>
                                        </label> </div>
                                    <div class="row px-3 mb-4"> <div class="g-recaptcha" data-sitekey="<?= frame::configGet("recaptcha/key") ?>"></div> <div class="invalid-feedback" id="help-recaptcha"></div> </div>
                                    <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">提交</button> </div>
                                    <div class="row mb-2 px-3"> <small class="font-weight-bold">已有账号? <a class="text-danger"  href="/">点击登录</a></small> </div>
                                </form>
                                <script type="text/javascript">
                                    function validateForgetPost() {
                                        var ok = true;
                                        ok &= getFormErrorAndShowHelp('username', validateNotEmpty, '账号凭据');
                                        return ok;
                                    }
                                    function submitForgetPost() {
                                        if (!validateForgetPost()) {
                                            return false;
                                        }

                                        $.post('/forget', {
                                            csrf : "<?= frame::clientKey() ?>",
                                            username : $('#input-username').val(),
                                            recaptcha : grecaptcha.getResponse(),
                                            forget : "forget"
                                        }, function(msg) {
                                            if (msg == 'expired') {
                                                showErrorHelp("username", "页面会话已过期");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();
                                            } else if (msg == 'username') {
                                                showErrorHelp("username", "账号不存在");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();                                                
                                            } else if (msg == 'recaptcha') {
                                                showErrorHelp("recaptcha", "验证码不正确");
                                                grecaptcha.reset();
                                            } else if (msg == 'email') {
                                                showErrorHelp("username", "未绑定邮箱");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();
                                            } else {
                                                try {
                                                    obj = JSON.parse(msg);
                                                    if (obj.res == 'ok') {
                                                        alert("重置密码邮件已发送至注册邮箱 " + obj.email)
                                                        location.href = "/";
                                                    } else {
                                                        throw 'Unknown';
                                                    }
                                                } catch (err) {
                                                    showErrorHelp("username", "未知错误");
                                                    showErrorHelp("recaptcha", "");
                                                    grecaptcha.reset();
                                                }
                                            }
                                        });
                                        return true;
                                    }

                                    $(document).ready(function() {
                                        $('#form-forget').submit(function(e) {
                                            e.preventDefault();
                                            submitForgetPost();
                                        });
                                    });
                                </script>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="bg-blue py-3">
                    <div class="row px-3 mt-2"> <small class="ml-4 ml-sm-5 mb-2">版权所有 &copy; 2020-<?= date("Y") ?> <a href="<?= getSystemVariable("site/organization_site") ?>" target="_blank" style="text-decoration:none; color:white"><?= getSystemVariable("site/organization") ?></a></small><small class="ml-4 mr-4 ml-sm-auto"><?= getSystemVariable("site/icp") ?></small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>