<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if($assetsFlag == 1){
        return true;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1024"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?= getCDNLink("images/favicon.png") ?>">
    <title><?= $title ?> - <?= frame::configGet("site/name") ?></title>

    <script>if(!!window.ActiveXObject || "ActiveXObject" in window){location.href='/upgrade-your-browser'}</script>

    <?php if (isset($assets["jquery-ui"])): ?>
    <link href="<?= getCDNLink("dist/jquery-ui/jquery-ui.css?ver=20220204") ?>" rel="stylesheet">
    <?php endif ?>

    <?php if (isset($assets["panel"])): ?>
    <link href="<?= getCDNLink("dist/panel/panel.css?ver=20220129") ?>" rel="stylesheet">
    <?php else: ?>
    <link href="<?= getCDNLink("css/bootstrap-v4.min.css?ver=20211103") ?>" rel="stylesheet">
    <?php endif ?>

    <?php if (isset($assets["login"])) : ?>
    <link href="<?= getCDNLink("css/pages/login.css?ver=20220724") ?>" rel="stylesheet">
    <?php endif ?>

    <?php if (isset($assets["profile"])): ?>
    <link href="<?= getCDNLink("css/pages/profile.css?ver=20211104") ?>" rel="stylesheet">
    <?php endif ?>

    <script src="<?= getCDNLink("js/jquery.min.js?ver=20210123") ?>"></script>

    <?php if (isset($assets["jquery-ui"])): ?>
    <script src="<?= getCDNLink("dist/jquery-ui/jquery-ui.js?ver=20220204") ?>"></script>
    <?php endif ?>

    <script src="<?= getCDNLink("js/popper.min.js?ver=20210128") ?>"></script>
    <script src="<?= getCDNLink("js/bootstrap.min.js?ver=20210123") ?>"></script>
    <script src="<?= getCDNLink("js/validate.js?ver=20230803") ?>"></script>

    <?php if (isset($assets["captcha"])) : ?>
    <script src="https://www.recaptcha.net/recaptcha/api.js" async defer></script>
    <?php endif ?>

    <?php if (isset($assets["panel"])): ?>
    <script src="<?= getCDNLink("js/app-style-switcher.min.js?ver=20210123") ?>"></script>
    <script src="<?= getCDNLink("js/feather.min.js?ver=20210123") ?>"></script>
    <script src="<?= getCDNLink("js/perfect-scrollbar.jquery.min.js?ver=20210123") ?>"></script>
    <script src="<?= getCDNLink("js/sidebarmenu.min.js?ver=20210501") ?>"></script>
    <script src="<?= getCDNLink("js/app.min.js?ver=20220128") ?>"></script>
    <?php endif ?>

    <?php if (isset($assets["base64"])): ?>
    <script src="<?= getCDNLink("js/base64.min.js?ver=20220203") ?>"></script>
    <?php endif ?>

    <?php if (isset($assets["table"])) : ?>
    <link href="<?= getCDNLink("css/dataTables.bootstrap4.css?ver=20210127") ?>" rel="stylesheet">
    <?php endif ?>

    <?php if (isset($assets["ip"])): ?>
    <script>$(function () {$('[data-toggle="tooltip"]').tooltip()})</script>
    <?php endif ?>
    
    <style>.modal-open{overflow:auto;padding-right:0 !important;}</style>
</head>
<body>