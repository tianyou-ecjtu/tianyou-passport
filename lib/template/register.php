<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if ($_POST["register"] == "register") {
        if (!checkCSRF()) {
            exit("expired");
        }

        if (!checkCaptcha()) {
            exit("recaptcha");
        }

        $username = getParam("username");
        $password = getParam("password");

        $realname = getParam("realname");
        $idcard = getParam("idcard");
        $email = getParam("email");
        $mobile = getParam("mobile");
        $year = getParam("year");

        if (strlen($username) > 30 || !validateUsername($username)) {
            exit();
        }

        if (!validatePassword($password) || !validateRealname($realname) || !validateEmail($email) || !validateMobile($mobile) || empty($year) || !is_numeric($year)) {
            exit("");
        }

        if ($_POST["password"] != $_POST["password_confirm"]) {
            exit("password");
        }

        if (!validateIDCard($idcard)) {
            exit("idcard");
        }

        if (db::num_rows("SELECT `id` FROM `TABLEPREFIX_users` where `username` = ?", "s", [$username])) {
            exit("username");
        }        

        if (db::num_rows("SELECT `uid` FROM `TABLEPREFIX_userinfo` where `idcard` = ?", "s", [$idcard])) {
            exit("idcard2");
        }

        $salt = frame::randString(128);
        $passwordCipher = frame::oneWayEncryption($password, $salt);

        $date = date("Y-m-d H:i:s");
        $ip = frame::getIP();

        $id = db::insert("INSERT INTO `TABLEPREFIX_users` (`username`, `password`, `salt`, `registerTime`, `registerIP`) VALUES (?, ?, ?, ?, ?)", "sssss", [$username, $passwordCipher, $salt, $date, $ip]);

        db::insert("INSERT INTO `TABLEPREFIX_userinfo` (`uid`, `username`) VALUES (?, ?)", "is", [$id, $username]);

        $user = new user($id);
        $user->updateProfile($realname, $idcard, $email, $mobile, $year);

        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["login"] = '';
        $assets["captcha"] = '';
        return true;
    }
?>

<body class='snippet-body'>
    <div class="center-outer">
        <div class="center-inner container-register container-xxl px-1 px-md-5 px-lg-1 px-xl-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-12">
                        <div class="card2 card border-0 px-4">
                            <div class="text row px-3 mb-4">
                                <h4 class="or text-center">注册 - <?= frame::configGet("site/shortname") ?></h4>
                            </div>
                                <form id="form-register">
                                <div class="row d-flex">
                                    <div class="col-lg-6">
                                        <div class="row px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">电子邮箱</h6>
                                        </label> <input class="form-control" id="input-email" type="text" placeholder="请输入电子邮箱"> <div class="invalid-feedback" id="help-email"></div> </div>
                                        <div class="row mt-1  px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">用户名</h6>
                                        </label> <input class="form-control" id="input-username" type="text" placeholder="请输入用户名"> <div class="invalid-feedback" id="help-username"></div> </div>
                                        <div class="row mt-1  px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">密码</h6>
                                        </label> <input class="form-control" id="input-password" type="password" placeholder="请输入密码"> <div class="invalid-feedback" id="help-password"></div> </div>
                                        <div class="row mt-1  px-3"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">重复密码</h6>
                                        </label> <input class="form-control" id="input-password-confirm" type="password" placeholder="请再次输入密码"> <div class="invalid-feedback" id="help-password-confirm"></div> </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row px-3"> <label class="mb-1">
                                                <h6 class="mb-0 text-sm">真实姓名</h6>
                                            </label> <input class="form-control" id="input-realname" type="text" placeholder="请输入真实姓名"> <div class="invalid-feedback" id="help-realname"></div> </div>
                                            <div class="row mt-1 px-3"> <label class="mb-1">
                                                <h6 class="mb-0 text-sm">身份证号</h6>
                                            </label> <input class="form-control" id="input-idcard" type="text" placeholder="请输入身份证号"> <div class="invalid-feedback" id="help-idcard"></div> </div>
                                            <div class="row mt-1  px-3"> <label class="mb-1">
                                                <h6 class="mb-0 text-sm">联系电话</h6>
                                            </label> <input class="form-control" id="input-mobile" type="text" placeholder="请输入联系电话"> <div class="invalid-feedback" id="help-mobile"></div> </div>
                                            <div class="row mt-1  px-3"> <label class="mb-1">
                                                <h6 class="mb-0 text-sm">入学年份</h6>
                                            </label> <input class="form-control" id="input-year" type="number" placeholder="请输入入学年份" value="<?= date("Y") ?>"> <div class="invalid-feedback" id="help-year"></div> </div>                                                                                
                                    </div>
                                </div>
                                <div class="row d-flex">
                                    <div class="col-lg-6">
                                    <div class="row px-3 mt-2"> <label class="mb-1">
                                            <h6 class="mb-0 text-sm">人机验证</h6>
                                        </label> </div>
                                    <div class="row px-3"> <div class="g-recaptcha" data-sitekey="<?= frame::configGet("recaptcha/key") ?>"></div> <div class="invalid-feedback" id="help-recaptcha"></div> </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row mt-4 mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">下一步</button> </div>
                                        <div class="row mb-2 px-3"> <small class="font-weight-bold">已有账号? <a class="text-danger"  href="/">点击登录</a></small> </div>
                                    </div>
                                </div>
                                </form>
                                <script type="text/javascript">
                                    function validateRegisterPost() {
                                        var ok = true;
                                        ok &= getFormErrorAndShowHelp('email', validateEmail);
                                        ok &= getFormErrorAndShowHelp('username', validateUsername);
                                        ok &= getFormErrorAndShowHelp('password', validatePassword);
                                        ok &= getFormErrorAndShowHelp('password-confirm', validatePassword);
                                        ok &= getFormErrorAndShowHelp('realname', validateRealname);
                                        ok &= getFormErrorAndShowHelp('idcard', validateIDCard);
                                        ok &= getFormErrorAndShowHelp('mobile', validateMobile);
                                        ok &= getFormErrorAndShowHelp('year', validateYear, '入学年份');
                                        return ok;
                                    }
                                    function submitRegisterPost() {
                                        if (!validateRegisterPost()) {
                                            return false;
                                        }

                                        $.post('/register', {
                                            csrf : "<?= frame::clientKey() ?>",
                                            realname : $('#input-realname').val(),
                                            idcard : $('#input-idcard').val(),
                                            email : $('#input-email').val(),
                                            mobile : $('#input-mobile').val(),
                                            year : $('#input-year').val(),
                                            username : $('#input-username').val(),
                                            password : $('#input-password').val(),
                                            password_confirm : $('#input-password-confirm').val(),
                                            recaptcha : grecaptcha.getResponse(),
                                            register : "register"
                                        }, function(msg) {
                                            if (msg == 'ok') {
                                                alert("注册成功，验证邮件已发送至邮箱");
                                                window.location.href = "/";
                                            } else if (msg == 'expired') {
                                                showErrorHelp("email", "页面会话已过期");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();
                                            } else if (msg == 'username') {
                                                showErrorHelp("username", "用户名已被注册");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();                                                
                                            } else if (msg == 'recaptcha') {
                                                showErrorHelp("recaptcha", "验证码不正确");
                                                grecaptcha.reset();
                                            } else if (msg == 'password') {
                                                showErrorHelp("password", "两次密码输入不相同");
                                                showErrorHelp("password-confirm", "两次密码输入不相同");
                                                grecaptcha.reset();
                                            } else if (msg == 'idcard') {
                                                showErrorHelp("idcard", "身份证号不正确");
                                                grecaptcha.reset();
                                            } else if (msg == 'idcard2') {
                                                showErrorHelp("idcard", "身份证号已被注册");
                                                grecaptcha.reset();
                                            } else {
                                                showErrorHelp("email", "未知错误");
                                                showErrorHelp("recaptcha", "");
                                                grecaptcha.reset();
                                            }
                                        });
                                        return true;
                                    }

                                    $(document).ready(function() {
                                        $('#form-register').submit(function(e) {
                                            e.preventDefault();
                                            submitRegisterPost();
                                        });
                                    });
                                </script>
                        </div>
                    </div>
                </div>
                <div class="bg-blue py-3">
                    <div class="row px-3 mt-2"> <small class="ml-4 ml-sm-5 mb-2">版权所有 &copy; 2020-<?= date("Y") ?> <a href="<?= getSystemVariable("site/organization_site") ?>" target="_blank" style="text-decoration:none; color:white"><?= getSystemVariable("site/organization") ?></a></small><small class="ml-4 mr-4 ml-sm-auto"><?= getSystemVariable("site/icp") ?></small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>