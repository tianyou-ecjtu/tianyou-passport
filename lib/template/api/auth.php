<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $isGETRequest = false;

    if (!empty(getParam("cid", "GET"))) {
        $isGETRequest = true;

        $cid = getParam("cid", "GET");
        $salt = getParam("salt", "GET");
        $token = getParam("token", "GET");
        $data = getParam("data", "GET");
    } else {
        $cid = getParam("cid");
        $salt = getParam("salt");
        $token = getParam("token");
        $data = getParam("data");
    }

    if (empty($cid) || empty($salt) || empty($token) || empty($data) || !is_numeric($cid)) {
        exit('{"code": 403, "msg": "Invalid parameters"}');
    }

    $client = new client($cid);

    if (!$client->check()) {
        exit('{"code": 403, "msg": "Authentication failed, not exists"}');
    }
    
    $client->auth($salt, $token);

    if (!$client->isAuth()) {
        exit('{"code": 403, "msg": "Authentication failed"}');
    }
    
    $data = json_decode($client->decrypt($data, $salt), true);

    if (empty($data)) {
        exit('{"code": 403, "msg": "Data block decrypt failed"}');
    }

    $type = $data["type"];

    if ($type == 1) {
        $username = $data["username"];
        $password = $data["password"];
    
        if (empty($type) || empty($username) || empty($password) || !is_numeric($type)) {
            exit('{"code": 403, "msg": "Data block invalid parameters"}');
        }

        if (!validateUsername($username)) {
            exit('{"code": 403, "msg": "Data block invalid parameters"}');
        }

        $token = frame::randString(32);

        $user = new user();
        $user->remoteAuth($username, $password, $client->getID());
        $uid = $user->getID();

        if (!$uid) {
            db::insert("INSERT INTO `TABLEPREFIX_api_auth` (`cid`, `token`, `status`) VALUES (?, ?, 2)", "is", [$client->getID(), $token]);
        } else {
            db::insert("INSERT INTO `TABLEPREFIX_api_auth` (`cid`, `token`, `uid`, `username`, `status`) VALUES (?, ?, ?, ?, 1)", "isis", [$client->getID(), $token, $uid, $username]);
        }

        exit('{"code": 200, "token": "'.$token.'"}');
    } else if ($type == 2) {
        $username = $data["username"];
        $password = $data["password"];
    
        if (empty($type) || empty($username) || empty($password) || !is_numeric($type)) {
            exit('{"code": 403, "msg": "Data block invalid parameters"}');
        }

        $token = frame::randString(32);

        $auth_id = db::insert("INSERT INTO `TABLEPREFIX_api_auth` (`cid`, `token`, `status`) VALUES (?, ?, 0)", "is", [$client->getID(), $token]);
        db::insert("INSERT INTO `TABLEPREFIX_ecjtu_bind` (`uid`, `auth_id`, `ecjtuID`, `data`, `salt`) VALUES (?, ?, ?, ?, ?)", "iisss", [-1, $auth_id, $username, frame::twoWayEncryption($password, $salt, $salt), $salt]);

        exit('{"code": 200, "token": "'.$token.'"}');
    } else if ($type == 3) {
        if (!$isGETRequest) {
            exit('{"code": 403, "msg": "Only support GET Request"}');
        }

        $callback = $data["callback"];

        if (empty($callback)) {
            exit('{"code": 403, "msg": "Data block invalid parameters"}');
        }

        $token = frame::randString(32);
        $callbackToken = frame::randString(16);

        $auth_id = db::insert("INSERT INTO `TABLEPREFIX_api_auth` (`cid`, `token`, `status`) VALUES (?, ?, 0)", "is", [$client->getID(), $token]);

        frame::createSession("auth_{$callbackToken}_callback", $callback);
        frame::createSession("auth_{$callbackToken}_callback_cid", $client->getID());
        frame::createSession("auth_{$callbackToken}_callback_token", $token);

        redirect("/?callback={$callbackToken}");
    } else {
        exit('{"code": 403, "msg": "Invalid auth type"}');
    }

    exit;
?>