<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = $route_data["user"];

    if (!$user->checkExist() || $user->isBlock() || !$user->isWaiting2FA()) {
        header("Location:/404");
        exit;
    }

    if ($_POST["login"] == "2fa") {
        if (!checkCSRF()) {
            exit("expired");
        }

        if (!checkCaptcha()) {
            exit("recaptcha");
        }

        $code = getParam("code_2fa");    
        
        if (empty($code) || !is_numeric($code)) {
            exit("");
        }

        $callback = getParam("callback");
        $callback_cid = frame::readSession("auth_{$callback}_callback_cid");
        if (empty($callback_cid)) $callback_cid = 1;

        $google2fa = new twofactor($user);

        if (strlen($code) != 6 || !$google2fa->validate($code, $callback_cid)) {
            exit("code");
        } else {
            exit("ok");
        }
    }

    if($assetsFlag == 1){
        $assets["login"] = '';
        $assets["captcha"] = '';
        return true;
    }
?>

<body class='snippet-body'>
    <div class="center-outer">
        <div class="center-inner container-xxl px-1 px-md-5 px-lg-1 px-xl-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-4 border-line"> <img src="<?= getCDNLink("images/logo.png") ?>" class="image"> </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 px-4">
                            <div class="text row px-3 mb-4">
                                    <h4 class="or text-center" id="sitename"></h4>
                                
                            </div>
                            <form id="form-2fa">
                                <div class="row px-3"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">验证码</h6>
                                    </label> <input class="form-control" id="input-code" type="text" placeholder="请输入6位OTP验证码"> <div class="invalid-feedback" id="help-code"></div> </div>
                                    
                                <div class="row px-3 mt-2"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">人机验证</h6>
                                    </label> </div>
                                <div class="row px-3 mb-4"> <div class="g-recaptcha" data-sitekey="<?= frame::configGet("recaptcha/key") ?>"></div> <div class="invalid-feedback" id="help-recaptcha"></div> </div>
                                <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">验证</button> </div>
                                <div class="row mb-2 px-3"> <small class="font-weight-bold">更换账号? <a class="text-danger" href="/">点击返回</a></small> </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="bg-blue py-3">
                    <div class="row px-3 mt-2"> <small class="ml-4 ml-sm-5 mb-2">版权所有 &copy; 2020-<?= date("Y") ?> <a href="<?= getSystemVariable("site/organization_site") ?>" target="_blank" style="text-decoration:none; color:white"><?= getSystemVariable("site/organization") ?></a></small><small class="ml-4 mr-4 ml-sm-auto"><?= getSystemVariable("site/icp") ?></small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var sitename = sessionStorage.getItem("callbackName");
        document.getElementById('sitename').innerText = !sitename ? "<?= frame::configGet("site/shortname") ?>" : sitename;

        function validateLoginPost() {
            var ok = true;
            ok &= getFormErrorAndShowHelp('code', validateIntNotEmpty, "OTP验证码");
            return ok;
        }

        function submitLoginPost() {
            if (!validateLoginPost()) {
                return false;
            }

            $.post('/2fa', {
                csrf : "<?= frame::clientKey() ?>",
                recaptcha : grecaptcha.getResponse(),
                code_2fa : $('#input-code').val(),
                callback : sessionStorage.getItem("callback"),
                login : "2fa"
            }, function(msg) {
                if (msg == 'ok') {
                    if (sessionStorage.getItem("callback")) {
                        window.location.href = '/?redirect=' + sessionStorage.getItem("callback");  
                    } else {
                        window.location.href = '/';  
                    }
                } else if (msg == 'expired') {
                    showErrorHelp("code", "页面会话已过期");
                    showErrorHelp("recaptcha", "");
                    grecaptcha.reset();
                } else if (msg == 'recaptcha') {
                    showErrorHelp("recaptcha", "人机验证未通过");
                    grecaptcha.reset();
                } else if (msg == 'code') {
                    showErrorHelp("code", "验证码错误");
                    showErrorHelp("recaptcha", "");
                    grecaptcha.reset();
                } else {
                    showErrorHelp("code", "未知错误");
                    showErrorHelp("recaptcha", "");
                    grecaptcha.reset();
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-2fa').submit(function(e) {
                e.preventDefault();
                submitLoginPost();
            });
        });
    </script>
</body>