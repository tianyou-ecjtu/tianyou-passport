<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if($assetsFlag == 1){
        $assets["login"] = '';
        return true;
    }
?>

<meta http-equiv="refresh" content="5;url=/" />

<body class='snippet-body'>
    <div class="center-outer">
        <div class="center-inner container-xxl px-1 px-md-5 px-lg-1 px-xl-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-4 border-line"> <img src="<?= getCDNLink("images/logo.png") ?>" class="image"> </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 px-4">
                            <div class="text row px-3 mb-4">
                                    <h4 class="or text-center">您访问的页面不存在</h4>
                            </div>
                            <form id="form-login">
                                <div class="row px-3"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">请求序列</h6>
                                    </label> <input class="form-control" type="text" value="<?= frame::getRequestID() ?>" disabled><div style="margin-top: 0rem; display: block; height: 16px; width:100%"></div></div>
                                <div class="row px-3 mt-2"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">客户端标识</h6>
                                    </label> <input class="form-control" type="text" value="<?= frame::oneWayEncryption(frame::clientKey(), frame::clientKey(), 32) ?>" disabled><div style="margin-top: 0rem; display: block; height: 16px; width:100%"></div></div>
                                <div class="row px-3 mt-5 mb-5">
                                    <a href="/" class="text-danger text-center" style="margin:0 auto">将在 <span class="second">5</span> 秒后自动跳转至首页</a>
                                </div>
                                <div class="row mt-3 mb-3"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="bg-blue py-3">
                    <div class="row px-3 mt-2"> <small class="ml-4 ml-sm-5 mb-2">版权所有 &copy; 2020-<?= date("Y") ?> <?= getSystemVariable("site/organization") ?></small><small class="ml-4 mr-4 ml-sm-auto"><?= getSystemVariable("site/icp") ?></small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            var wait = $(".second").html();
            timeOut();

            function timeOut() {
                if(wait != 0) {
                    setTimeout(function() {
                        $('.second').text(--wait);
                        timeOut();
                    }, 1000);
                }
            }
        });
    </script>

</body>