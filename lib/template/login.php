<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if ($_POST["login"] == "login") {
        $user = $route_data["user"];
        
        if (!checkCSRF()) {
            exit("expired");
        }

        $username = getParam("username");
        $password = getParam("password");
        $save = getParam("save");

        $callback = getParam("callback");
        $callback_cid = frame::readSession("auth_{$callback}_callback_cid");
        if (empty($callback_cid)) $callback_cid = 1;

        $user->auth($username, $password, $callback_cid);

        if (!$user->checkAuth()) {
            exit("password");
        }

        if ($user->isBlock()) {
            exit("block");
        }

        $user->setLoginStatus(getParam("save") == "checked");
        
        if ($user->isEnable2FA()) {
            exit("2fa");
        }

        exit("ok");
    }

    if($assetsFlag == 1){
        $assets["login"] = '';
        return true;
    }
?>

<body class='snippet-body'>
    <div class="center-outer">
        <div class="center-inner container-xxl px-1 px-md-5 px-lg-1 px-xl-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-4 border-line"> <img src="<?= getCDNLink("images/logo.png") ?>" class="image"> </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 px-4">
                            <div class="text row px-3 mb-4">
                                    <h4 class="or text-center" id="sitename"></h4>
                            </div>
                            <form id="form-login">
                                <div class="row px-3"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">用户名</h6>
                                    </label> <input class="form-control" id="input-username" type="text" placeholder="请输入用户名"> <div class="invalid-feedback" id="help-username"></div> </div>
                                    
                                <div class="row px-3 mt-2"> <label class="mb-1">
                                        <h6 class="mb-0 text-sm">密码</h6>
                                    </label> <input class="form-control" type="password" id="input-password" placeholder="请输入密码"> <div class="invalid-feedback" id="help-password"></div> </div>
                                <div class="row px-3 mt-2 mb-4">
                                    <div class="custom-control custom-checkbox custom-control-inline"> <input type="checkbox" id="input-save" class="custom-control-input" value="checked" checked> <label for="input-save" class="custom-control-label text-sm">保存登录信息</label> </div> <a href="/forget" class="ml-auto mb-0 text-sm">找回账号?</a>
                                </div>
                                <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">登录</button> </div>
                                <div class="row mb-2 px-3"> <small class="font-weight-bold">还没有账号? <a class="text-danger" href="/register">点击注册</a></small> <small class="font-weight-bold ml-auto"><a class="text-danger" href="<?= frame::configGet("link/login_footer/site") ?>"><?= frame::configGet("link/login_footer/name") ?></a></small></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="bg-blue py-3">
                    <div class="row px-3 mt-2"> <small class="ml-4 ml-sm-5 mb-2">版权所有 &copy; 2020-<?= date("Y") ?> <a href="<?= getSystemVariable("site/organization_site") ?>" target="_blank" style="text-decoration:none; color:white"><?= getSystemVariable("site/organization") ?></a></small><small class="ml-4 mr-4 ml-sm-auto"><?= getSystemVariable("site/icp") ?></small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var sitename = sessionStorage.getItem("callbackName");
        document.getElementById('sitename').innerText = !sitename ? "<?= frame::configGet("site/shortname") ?>" : sitename;

        function validateLoginPost() {
            var ok = true;
            ok &= getFormErrorAndShowHelp('username', validateUsername);
            ok &= getFormErrorAndShowHelp('password', validatePassword);
            return ok;
        }

        function submitLoginPost() {
            if (!validateLoginPost()) {
                return false;
            }

            $.post('/', {
                csrf : "<?= frame::clientKey() ?>",
                username : $('#input-username').val(),
                password : $('#input-password').val(),
                save: $('#input-save:checked').val(),
                callback : sessionStorage.getItem("callback"),
                login : "login"
            }, function(msg) {
                if (msg == 'ok') {
                    if (sessionStorage.getItem("callback")) {
                        window.location.href = '/?redirect=' + sessionStorage.getItem("callback");  
                    } else {
                        location.reload();
                    }
                } else if (msg == '2fa') {
                    window.location.href = '/2fa'; 
                } else if (msg == 'expired') {
                    showErrorHelp("username", "页面会话已过期");
                } else if (msg == 'password') {
                    showErrorHelp("username", "用户名或密码错误");
                    showErrorHelp("password", "用户名或密码错误");
                } else if (msg == 'block') {
                    showErrorHelp("username", "账户已停用");
                } else {
                    showErrorHelp("username", "未知错误");
                }
            });

            return true;
        }

        $(document).ready(function() {
            $('#form-login').submit(function(e) {
                e.preventDefault();
                submitLoginPost();
            });
        });
    </script>
</body>