<?php
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    if($assetsFlag == 1){
        return true;
    }
?>
<?php if (isset($assets["panel"])): ?>
<script>function saveMenuPos(){let s=null;return function(){s&&clearTimeout(s),s=setTimeout(function(){sessionStorage.setItem("sidemenu-y-axis",$(".scroll-sidebar").scrollTop())},100)}}$(".scroll-sidebar").on("ps-scroll-x",saveMenuPos()),$(function(){$(".scroll-sidebar").scrollTop(sessionStorage.getItem("sidemenu-y-axis"))}),$(".preloader").fadeOut();</script>
<?php endif ?>
</body>
</html>