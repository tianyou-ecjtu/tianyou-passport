<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class APIECJTU {
        private $code = 200;
        private $data = null;
        private $msg = null;

        private function casProxyGet($data, object $client) {
            $this->data = db::selectFirst("SELECT * FROM `TABLEPREFIX_ecjtu_bind` where `status` = 1 LIMIT 1");
        }

        private function casProxyPush($data, object $client) {
            $id = $data["id"];
            $result = $data["result"];

            if (empty($id) || empty($result) || !is_numeric($id) || !is_numeric($result)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }
            
            $bindInfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_ecjtu_bind` where `id` = ?", "i", [$id]);
        
            if (empty($bindInfo)) {
                $this->invalid(532, "The specified binding ID does not exist.");
                return false;
            }
        
            if ($bindInfo["uid"] != -1) {
                if ($result == 1) {
                    $userinfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_userinfo` where `ecjtuID` = ?", "s", [$bindInfo["ecjtuID"]]);
                    
                    if (!empty($userinfo)) {
                        user::move($bindInfo["uid"], $userinfo["uid"]);
                    } else {
                        db::update("UPDATE `TABLEPREFIX_userinfo` SET `ecjtuID` = ? where `uid` = ?", "ii", [$bindInfo["ecjtuID"], $bindInfo["uid"]]);
                        user::cacheUserInfoUpdate($bindInfo["uid"]);
                        user::cacheUserGroupUpdate($bindInfo["uid"]);
                    }
        
                    db::delete("DELETE FROM `TABLEPREFIX_ecjtu_bind` where `id` = ?", "i", [$id]);
                } else {
                    db::update("UPDATE `TABLEPREFIX_ecjtu_bind` SET `status` = ? where `id` = ?", "ii", [$result, $id]);
                }
            } else {
                $auth_id = $bindInfo["auth_id"];
        
                if ($result == 1) {
                    $userinfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_userinfo` where `ecjtuID` = ?", "s", [$bindInfo["ecjtuID"]]);
        
                    if (!empty($userinfo)) {
                        db::update("UPDATE `TABLEPREFIX_api_auth` SET `status` = ?, `uid` = ?, `username` = ? where `id` = ?", "iisi", [$result, $userinfo["uid"], $userinfo["username"], $auth_id]);
                    } else {
                        $date = date("Y-m-d H:i:s");
                        $ip = frame::getIP();
                
                        $uid = db::insert("INSERT INTO `TABLEPREFIX_users` (`username`, `registerTime`, `registerIP`) VALUES (?, ?, ?)", "sss", ["*".$bindInfo["ecjtuID"], $date, $ip]);
                        db::insert("INSERT INTO `TABLEPREFIX_userinfo` (`uid`, `username`, `ecjtuID`, `isThirdpart`) VALUES (?, ?, ?, 1)", "iss", [$uid, "*".$bindInfo["ecjtuID"], $bindInfo["ecjtuID"]]);
                        db::update("UPDATE `TABLEPREFIX_api_auth` SET `status` = ?, `uid` = ?, `username` = ? where `id` = ?", "iisi", [$result, $uid, "*".$bindInfo["ecjtuID"], $auth_id]);
                    }
        
                    db::delete("DELETE FROM `TABLEPREFIX_ecjtu_bind` where `id` = ?", "i", [$id]);
                } else {
                    db::delete("DELETE FROM `TABLEPREFIX_ecjtu_bind` where `id` = ?", "i", [$id]);
                    db::update("UPDATE `TABLEPREFIX_api_auth` SET `status` = ? where `id` = ?", "ii", [$result, $auth_id]);
                }
            }
        }

        private function invalid(int $code, string $msg = NULL) {
            $this->code = $code;
            $this->msg = $msg;
        }

        public function call($action, $params, $client) {
            if ($client->getID() != 1) {
                return [
                    "code" => 520,
                    "msg" => "The requested resource requires a specified site to access."
                ];
            }
            
            if (!is_callable([$this, $action])) {
                return [
                    "code" => 521,
                    "msg" => "The requested resource cannot be called."
                ];
            }

            $this->$action($params, $client);

            if ($this->code != 200) {
                return [
                    "code" => $this->code,
                    "msg" => $this->msg
                ];
            }

            return [
                "code" => $this->code,
                "data" => $this->data
            ];
        }
    }

    $apiClass = new APIECJTU();

    API::RPCAllowlist([
        "casProxyGet" => [API::REQUEST_POST, API::NEED_SIG],
        "casProxyPush" => [API::REQUEST_POST, API::NEED_SIG]
    ]);

    API::RPCEntrance($apiClass);
?>