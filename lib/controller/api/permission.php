<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class APIPermission {
        private $code = 200;
        private $data = null;
        private $msg = null;

        private function invalid(int $code, string $msg = NULL) {
            $this->code = $code;
            $this->msg = $msg;
        }

        public function call($action, $params, $client) {
            if (!is_callable([$this, $action])) {
                return [
                    "code" => 521,
                    "msg" => "The requested resource cannot be called."
                ];
            }

            $this->$action($params, $client);

            if ($this->code != 200) {
                return [
                    "code" => $this->code,
                    "msg" => $this->msg
                ];
            }

            return [
                "code" => $this->code,
                "data" => $this->data
            ];
        }

        private function revokePermission($data, object $client) {
            $id = $data["id"];
            $permissions = $data["permissions"];

            if (empty($id) || !is_numeric($id) || !is_array(($permissions))) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            $permissionSettingJson = db::selectFirst("SELECT `permission` FROM `TABLEPREFIX_permission_user` where `uid` = ? and `cid` = ?", "ii", [$id, $client->getID()])["permission"];
            $permissionSetting = json_decode($permissionSettingJson, true);

            if (!is_array($permissionSetting)) {
                $this->invalid(533, "The specified user does not have permission record.");
                return false;
            }

            foreach ($permissions as $permission) {
                if (!db::num_rows("SELECT `id` FROM `TABLEPREFIX_permission_list` where `name` = ? and `cid` = ?", "si", [$permission, $client->getID()])) {
                    $this->invalid(534, "The specified permission does not exist.");
                    return false;
                }

                unset($permissionSetting[$permission]);
            }

            if (empty($permissionSetting)) {
                $res = db::delete("DELETE FROM `TABLEPREFIX_permission_user` where `uid` = ? and `cid` = ?", "ii", [$id, $client->getID()]);
            } else {
                $res = db::update("UPDATE `TABLEPREFIX_permission_user` SET `permission` = ? where `uid` = ? and `cid` = ?", "sii", [json_encode($permissionSetting, JSON_UNESCAPED_UNICODE), $id, $client->getID()]);
            }

            if (!$res) {
                $this->invalid(535, "The operation was not executed successfully.");
                return false;
            }

            user::cacheUserPermissionUpdate($id, $client->getID());
        }

        private function grantPermission($data, object $client) {
            $id = $data["id"];
            $permissions = $data["permissions"];

            if (empty($id) || !is_numeric($id) || !is_array(($permissions))) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            $permissionSettingJson = db::selectFirst("SELECT `permission` FROM `TABLEPREFIX_permission_user` where `uid` = ? and `cid` = ?", "ii", [$id, $client->getID()])["permission"];
            $permissionSetting = json_decode($permissionSettingJson, true);

            if (!is_array($permissionSetting)) {
                $permissionSetting = [];
            }

            foreach ($permissions as $permission) {
                if (!db::num_rows("SELECT `id` FROM `TABLEPREFIX_permission_list` where `name` = ? and `cid` = ?", "si", [$permission, $client->getID()])) {
                    $this->invalid(533, "The specified permission does not exist.");
                    return false;
                }

                $permissionSetting[$permission] = 1;
            }

            if (empty($permissionSettingJson)) {
                $res = db::insert("INSERT INTO `TABLEPREFIX_permission_user` (`uid`, `cid`, `permission`) VALUES (?, ?, ?)", "iis", [$id, $client->getID(), json_encode($permissionSetting, JSON_UNESCAPED_UNICODE)]);
            } else {
                $res = db::update("UPDATE `TABLEPREFIX_permission_user` SET `permission` = ? where `uid` = ? and `cid` = ?", "sii", [json_encode($permissionSetting, JSON_UNESCAPED_UNICODE), $id, $client->getID()]);
            }

            if (!$res) {
                $this->invalid(534, "The operation was not executed successfully.");
                return false;
            }

            user::cacheUserPermissionUpdate($id, $client->getID());
        }

        private function getPermissionAssignList($data, object $client) {
            $permissionName = $data["permission"];

            if (empty($permissionName)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            if (!db::num_rows("SELECT `id` FROM `TABLEPREFIX_permission_list` where `name` = ? and `cid` = ?", "si", [$permissionName, $client->getID()])) {
                $this->invalid(532, "The specified permission does not exist.");
                return false;
            }

            $data = [];
            $permissionUsers = db::selectAll("SELECT * FROM `TABLEPREFIX_permission_user` where `cid` = ?", "i", [$client->GetID()]);

            foreach ($permissionUsers as $permissionUser) {
                $id = $permissionUser["uid"];
                $permission = json_decode($permissionUser["permission"], true);

                if ($permission[$permissionName] == 1){
                    $user = new user($id);

                    array_push($data, [
                        "id" => $id,
                        "username" => $user->getUsername(),
                        "realname" => $user->getUserInfo()["realname"],
                    ]);
                }
            }

            $this->data = $data;
        }
    }

    $apiClass = new APIPermission();

    API::RPCAllowlist([
        "getPermissionAssignList" => [API::REQUEST_POST, API::NEED_SIG],
        "grantPermission" => [API::REQUEST_POST, API::NEED_SIG],
        "revokePermission" => [API::REQUEST_POST, API::NEED_SIG],
    ]);

    API::RPCEntrance($apiClass);
?>