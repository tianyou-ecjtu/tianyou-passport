<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class APIAutocomplete {
        private $code = 200;
        private $data = null;
        private $msg = null;

        private function searchUser($data, $client) {
            $keyword = $data["keyword"];

            if (empty($keyword)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $resp = [];
            $data = user::searchUser($data["keyword"], ["username", "realname"], true);
            
            foreach ($data as $user) {
                array_push($resp, [
                    "value" => $user["username"],
                    "realname" => $user["realname"],
                    "year" => $user["year"]
                ]);
            }

            $this->data = $resp;
        }

        private function searchUserSig($data, $client) {
            $this->searchUser($data, $client);
        }

        private function searchGroup($data, $client) {
            $keyword = $data["keyword"];

            if (empty($keyword)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $resp = [];
            $data = user::searchGroup($data["keyword"], true);
            
            foreach ($data as $group) {
                array_push($resp, [
                    "value" => $group["name"],
                    "description" => $group["description"]
                ]);
            }

            $this->data = $resp;
        }

        private function searchGroupSig($data, $client) {
            $this->searchGroup($data, $client);
        }

        private function invalid(int $code, string $msg = NULL) {
            $this->code = $code;
            $this->msg = $msg;
        }

        public function call($action, $params, $client) {
            if (!is_callable([$this, $action])) {
                return [
                    "code" => 521,
                    "msg" => "The requested resource cannot be called."
                ];
            }

            $this->$action($params, $client);

            if ($this->code != 200) {
                return [
                    "code" => $this->code,
                    "msg" => $this->msg
                ];
            }

            return [
                "code" => $this->code,
                "data" => $this->data
            ];
        }
    }

    $apiClass = new APIAutocomplete();

    API::RPCAllowlist([
        "searchUser" => [API::REQUEST_POST, API::NEED_ADMIN | API::NEED_CSRF],
        "searchUserSig" => [API::REQUEST_POST, API::NEED_SIG],
        "searchGroup" => [API::REQUEST_POST, API::NEED_ADMIN | API::NEED_CSRF],
        "searchGroupSig" => [API::REQUEST_POST, API::NEED_SIG],
    ]);

    API::RPCEntrance($apiClass);
?>