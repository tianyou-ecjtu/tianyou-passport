<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class APIUser {
        private $code = 200;
        private $data = null;
        private $msg = null;

        private function createSpecialUser($data, object $client) {
            $username = $data["username"];
            $password = $data["password"];
            $info = $data["info"];

            if (empty($username) || empty($password) || !validateUsername(substr($username, 1)) || !validatePassword($password)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            if ($username[0] != '_' && $username[0] != '#') {
                $this->invalid(532, "The specified username must start with character '_' or '#'.");
                return false;
            }

            if (db::num_rows("SELECT `id` FROM `TABLEPREFIX_users` where `username` = ?", "s", [$username])) {
                $this->invalid(533, "The specified username already taken.");
                return false;
            }

            $salt = frame::randString(128);
            $passwordCipher = frame::oneWayEncryption($password, $salt);
    
            $date = date("Y-m-d H:i:s");
    
            $id = db::insert("INSERT INTO `TABLEPREFIX_users` (`username`, `password`, `salt`, `registerTime`, `isSpecial`) VALUES (?, ?, ?, ?, ?)", "ssssi", [$username, $passwordCipher, $salt, $date, $client->getID()]);

            if (empty($id)) {
                $this->invalid(533, "The operation was not executed successfully.");
                return false;
            }

            db::insert("INSERT INTO `TABLEPREFIX_userinfo` (`uid`, `username`) VALUES (?, ?)", "is", [$id, $username]);

            foreach (["realname", "idcard", "ecjtuID", "year", "mobile"] as $key) {
                if (!empty($info[$key])) {
                    if ($key == "realname" && !validateRealname($info[$key])) continue;
                    else if ($key == "idcard" && !validateIDCard($info[$key])) continue;
                    else if ($key == "year" && !validateYear($info[$key])) continue;
                    else if ($key == "mobile" && !validateMobile($info[$key])) continue;

                    db::update("UPDATE `TABLEPREFIX_userinfo` SET {$key} = ? where `uid` = ?", "si", [$info[$key], $id]);
                }
            }

            $this->data = [
                "id" => $id
            ];
        }

        private function updateSpecialUser($data, object $client) {
            $id = $data["id"];
            $info = $data["info"];

            if (empty($id) || !is_numeric($id)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            if ($user->getUserInfo()["isSpecial"] != $client->getID()) {
                $this->invalid(533, "The specified user is not the current site special user.");
                return false;
            }

            foreach (["realname", "idcard", "ecjtuID", "year", "mobile"] as $key) {
                if (!empty($info[$key])) {
                    if ($key == "realname" && !validateRealname($info[$key])) continue;
                    else if ($key == "idcard" && !validateIDCard($info[$key])) continue;
                    else if ($key == "year" && !validateYear($info[$key])) continue;
                    else if ($key == "mobile" && !validateMobile($info[$key])) continue;

                    db::update("UPDATE `TABLEPREFIX_userinfo` SET {$key} = ? where `uid` = ?", "si", [$info[$key], $id]);
                }
            }

            user::cacheUserUpdate($id);
            user::cacheUserInfoUpdate($id);
        }

        private function deleteSpecialUser($data, object $client) {
            $id = $data["id"];
            
            if (empty($id) || !is_numeric($id)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            if ($user->getUserInfo()["isSpecial"] != $client->getID()) {
                $this->invalid(533, "The specified user is not the current site special user.");
                return false;
            }

            if (!db::update("UPDATE `TABLEPREFIX_users` SET `password` = NULL, `salt` = NULL, `isBlock` = -1 where `id` = ?", "i", [$id])) {
                $this->invalid(534, "The operation was not executed successfully.");
                return false;
            }

            user::cacheUserUpdate($id);
            user::cacheUserInfoUpdate($id);
        }

        private function resetSpecialUserPassword($data, object $client) {
            $id = $data["id"];
            $password = $data["password"];

            if (empty($id) || !is_numeric($id) || empty($password) || !validatePassword($password)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            if ($user->getUserInfo()["isSpecial"] != $client->getID()) {
                $this->invalid(533, "The specified user is not the current site special user.");
                return false;
            }

            $user->updatePassword($password);
        }

        private function getUserGroupInfo($data, object $client) {
            $id = $data["id"];

            if (empty($id) || !is_numeric($id)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user();
            $groupInfo = $user -> getUSerGroupInfo($id);

            if (empty($groupInfo)) {
                $this->invalid(532, "The specified group ID does not exist.");
                return false;
            }

            $this->data = $groupInfo;
        }

        private function getUserInfo($data, object $client) {
            $id = $data["id"];

            if (empty($id) || !is_numeric($id)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);
            
            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;
            }

            $group = $user->getUserGroup();
            if (!empty($group[0])) {
                $groupBasic = $user->getUserGroupInfo($group[0]["gid"]);
            }

            $this->data = [
                "basic" => $user->getBasicInfo(),
                "group" => $group,
                "groupBasic" => $groupBasic,
                "permission" => $user->getPermission($client->getID())
            ];
        }

        private function getUserInfoFull($data, object $client = null) {
            $id = $data["id"];

            if (empty($id) || !is_numeric($id)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user($id);

            if (!$user->checkExist()) {
                $this->invalid(532, "The specified userID does not exist.");
                return false;     
            }

            $userinfo_keys = ["username", "realname", "idcard", "birthday", "sex", "email", "mobile", "ecjtuID", "registerTime", "registerIP", "lastLoginTime", "lastLoginIP", "emailUnverify"];

            $userinfo = $user->getUserinfo();
            $userinfo_output = [];

            foreach ($userinfo_keys as $val) {
                $userinfo_output[$val] = $userinfo[$val];
            }

            $this->data = $userinfo_output;
        }

        private function searchUser($data, object $client) {
            $keyword = $data["keyword"];
            $options = $data["options"];
            $fuzzy = $data["fuzzy"];

            if (empty($keyword) || !isset($fuzzy) || !is_bool($fuzzy) || !is_array($options)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $support_option = ["realname", "idcard", "mobile", "email", "username", "ecjtuID"];
            
            foreach ($options as $option) {
                if (!in_array($option, $support_option)) {
                    $this->invalid(532, "The specified operation is not supported.");
                    return false;
                }
            }

            $users = user::searchUser($keyword, $options, $fuzzy);
            $ids = [];

            foreach ($users as $user) {
                array_push($ids, $user["uid"]);
            }

            $this->data = $ids;
            return true;
        }

        private function searchUserGroup($data, object $client) {
            $keyword = $data["keyword"];
            $fuzzy = $data["fuzzy"];

            $groups = user::searchGroup($keyword, $fuzzy);
            $ids = [];

            foreach ($groups as $group) {
                array_push($ids, $group["id"]);
            }

            $this->data = $ids;
            return true;
        }
        
        private function remoteLogin($data, object $client) {
            $type = $data["type"];
            
            if (empty($type) || !is_numeric($type)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            if ($type == 1) {
                $callback = $data["callback"];

                if (empty($callback)) {
                    $this->invalid(531, "The operation parameters are incomplete.");
                    return false;
                }

                $token = frame::randString(32);
                $callbackToken = frame::randString(16);
        
                db::insert("INSERT INTO `TABLEPREFIX_api_auth` (`cid`, `token`, `status`) VALUES (?, ?, 0)", "is", [$client->getID(), $token]);
                
                frame::initSession();
                
                frame::createSession("auth_{$callbackToken}_callback", $callback);
                frame::createSession("auth_{$callbackToken}_callback_cid", $client->getID());
                frame::createSession("auth_{$callbackToken}_callback_name", $client->getClientInfo()["name"]);
                frame::createSession("auth_{$callbackToken}_callback_token", $token);
                
                redirect("/callback?id={$callbackToken}");
            } else {
                $this->invalid(533, "The specified operation is not supported.");
                return false;                
            }
        }

        private function remoteAuth($data, $client) {
            $username = $data["username"];
            $password = $data["password"];
            $code = $data["code"];
            
            if (!validateUsername($username) || !validatePassword($password)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            if (!empty($code) && (!is_numeric($code) || strlen($code) != 6)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $user = new user();
            $user->remoteAuth($username, $password, $client->getID());

            if (!$user->checkExist()) {
                $this->data = ["result" => false, "msg" => "incorrect"];
                return false;
            }

            if ($user->isBlock()) {
                $this->data = ["result" => false, "msg" => "block"];
                return false;
            }
            
            if ($user->isEnable2FA()) {
                if (empty($code)) {
                    $this->data = ["result" => false, "msg" => "2fa_required"];
                    return false;
                } else {
                    $google2fa = new twofactor($user);
                    if (!$google2fa->validate($code, $client->getID())) {
                        $this->data = ["result" => false, "msg" => "2fa_wrong"];
                        return false;
                    }
                }
            }

            $this->data = ["result" => true, "uid" => $user->getID()];
            return true;
        }

        private function remoteLoginVerify($data, $client) {
            $token = $data["token"];

            if (empty($token)) {
                $this->invalid(531, "The operation parameters are incomplete or invalid.");
                return false;
            }

            $verifyInfo = db::selectFirst("SELECT * FROM `TABLEPREFIX_api_auth` where `cid` = ? and `token` = ?", "is", [$client->getID(), $token]);

            if (empty($verifyInfo)) {
                $this->invalid(532, "The specified token does not exist.");
                return false;
            }

            #db::delete("DELETE FROM `TABLEPREFIX_api_auth` where `id` = ?", "i", [$verifyInfo["id"]]);

            $this->data = $verifyInfo;
            return true;
        }

        private function invalid(int $code, string $msg = NULL) {
            $this->code = $code;
            $this->msg = $msg;
        }

        public function call($action, $params, $client) {
            if (!is_callable([$this, $action])) {
                return [
                    "code" => 521,
                    "msg" => "The requested resource cannot be called."
                ];
            }

            $this->$action($params, $client);

            if ($this->code != 200) {
                return [
                    "code" => $this->code,
                    "msg" => $this->msg
                ];
            }

            return [
                "code" => $this->code,
                "data" => $this->data
            ];
        }
    }

    $apiClass = new APIUser();

    API::RPCAllowlist([
        "createSpecialUser" => [API::REQUEST_POST, API::NEED_SIG],
        "deleteSpecialUser" => [API::REQUEST_POST, API::NEED_SIG],
        "updateSpecialUser" => [API::REQUEST_POST, API::NEED_SIG],
        "getUserGroupInfo" => [API::REQUEST_POST, API::NEED_SIG],
        "getUserInfo" => [API::REQUEST_POST, API::NEED_SIG],
        "getUserInfoFull" => [API::REQUEST_POST, API::NEED_ADMIN | API::NEED_CSRF],
        "remoteAuth" => [API::REQUEST_POST, API::NEED_SIG],
        "remoteLogin" => [API::REQUEST_GET, API::NEED_SIG],
        "remoteLoginVerify" => [API::REQUEST_POST, API::NEED_SIG],
        "resetSpecialUserPassword" => [API::REQUEST_POST, API::NEED_SIG],
        "searchUser" => [API::REQUEST_POST, API::NEED_SIG],
        "searchUserGroup" => [API::REQUEST_POST, API::NEED_SIG],
    ]);

    API::RPCEntrance($apiClass);
?>