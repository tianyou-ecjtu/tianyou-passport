<?php

    /**
     * API Operation
     * 
     * @since 1.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class api {
        const REQUEST_GET = 0;
        const REQUEST_POST = 1;

        const NEED_SIG = 1;
        const NEED_CSRF = 2;
        const NEED_LOGIN = 4;
        const NEED_ADMIN = 8;

        private static $code = 200;
        private static $data = null;
        private static $msg = "";

        private static $id = null;
        private static $client = null;
        private static $params = null;
        private static $authOption = 0;

        private static $rpcEntrance = null;
        private static $rpcAllowlist = null;
        private static $rpcRequirement = null;
        private static $rpcAction = null;

        public static function rpcEntrance(object $func) {
            self::$rpcEntrance = $func;
        }

        public static function rpcAllowlist(array $allowlist) {
            self::$rpcAllowlist = $allowlist;
        }

        private static function validateClassName(string $name = null) {
            return is_string($name) && preg_match('/^[a-zA-Z0-9_]{1,32}$/', $name);
        }

        private static function validateActionName(string $name = null) {
            return is_string($name) && preg_match('/^[a-zA-Z0-9_]{1,64}$/', $name);
        }

        private static function getParam($name) {
            return self::$rpcRequirement[0] == self::REQUEST_GET ? $_GET[$name] : $_POST[$name];
        }

        private static function escape($val) {
            return db::escape(escapeHTML($val));
        }

        private static function parseURL() {
            preg_match_all("/\/apiv2\/(\S+)\/(\S+)/", frame::getAbsoluteURL(), $arr);
            
            $className = $arr[1][0];
            $actionName = $arr[2][0];
            
            if (!self::validateClassName($className) || !self::validateActionName($actionName)) {
                self::invalid(501, "The requested resource is invalid.");
                return false;
            }

            if (!include_once(ABSPATH . "lib/controller/api/{$className}.php")) {
                self::invalid(502, "The requested resource does not exist.");
                return false;
            }

            self::$rpcRequirement = self::$rpcAllowlist[$actionName];

            if (!isset(self::$rpcRequirement)) {
                self::invalid(503, "The requested resource does not exist.");
                return false;
            }

            if (($_SERVER['REQUEST_METHOD'] === 'POST') != self::$rpcRequirement[0]) {
                self::invalid(504, "The requested method is invalid.");
                return false;
            }

            self::$rpcAction = $actionName;
            self::$authOption = self::$rpcRequirement[1];
            return true;
        }

        private static function parseInput() {
            $needSig = self::$authOption & self::NEED_SIG;
            $needCSRF = self::$authOption & self::NEED_CSRF;
            $needLogin = self::$authOption & self::NEED_LOGIN;
            $needAdmin = self::$authOption & self::NEED_ADMIN;

            if ($needCSRF || $needLogin || $needAdmin) {
                frame::initSession();
            }

            if ($needCSRF && !checkCSRF()) {
                self::invalid(505, "The specified csrf token is invalid.");
                return false;
            }

            if ($needLogin || $needAdmin) {
                $user = new user();

                if (!$user->checkLogin()) {
                    self::invalid(506, "The requested resource requires authorization.");
                    return false;
                }

                if ($needAdmin && !$user->isAdmin()) {
                    self::invalid(507, "The requested resource requires advanced authorization.");
                    return false;
                }
            }

            if (!$needSig) {
                self::$params = json_decode(base64_decode(self::getParam("data")), TRUE);

                if (json_last_error() != JSON_ERROR_NONE) {
                    self::invalid(516, "The request data block decryption failed.");
                    return false;
                }
            } else {
                $id = self::getParam("id");
                $data = self::getParam("data");
                $sig = self::getParam("sig");
                
                if (empty($id) || empty($data) || empty($sig)) {
                    self::invalid(510, "The request parameters are incomplete.");
                    return false;
                }

                preg_match_all('/(\d+)-(\d+)-([a-zA-Z0-9_]{16,32})/', $id, $IDPregArr);

                $timestamp = $IDPregArr[1][0];
                $cid = $IDPregArr[2][0];
                $salt = $IDPregArr[3][0];

                if (empty($timestamp) || empty($cid) || empty($salt) || !is_numeric($timestamp) || !is_numeric($cid)) {
                    self::invalid(511, "The specified reqeustID is invalid.");
                    return false;
                }

                if (abs($timestamp - time()) > 1800) {
                    self::invalid(512, "The specified reqeustID has expired.");
                    return false;
                }

                self::$client = new client($cid);

                if (!self::$client->check()) {
                    self::invalid(513, "The specified clientID does not exist.");
                    return false;
                }

                if (self::$client->calcSignature($data, $id) != $sig) {
                    self::invalid(514, "The request signature verification failed.");
                    return false;
                }

                $salt_requestID = cache::getString("api_{$cid}_{$salt}", function() {
                    return frame::getRequestID();
                }, [], 900);

                if ($salt_requestID != frame::getRequestID()) {
                    self::invalid(515, "The request signature verification has expired.");
                    return false;
                }

                self::$params = json_decode(self::$client->decrypt($data, $id), TRUE);

                if (json_last_error() != JSON_ERROR_NONE) {
                    self::invalid(516, "The request data block decryption failed.");
                    return false;
                }

                self::$id = $id;
            }

            foreach (self::$params as $key => $value) {
                if (is_string(self::$params[$key])) {
                    self::$params[$key] = self::escape(self::$params[$key]);
                }
            }
            
            return true;
        }

        private static function remoteProcedureCall() {
            $response = self::$rpcEntrance->call(self::$rpcAction, self::$params, self::$client);
            
            self::$code = $response["code"];
            self::$data = $response["data"];

            if (self::$code != 200) {
                self::invalid($response["code"], $response["msg"]);
            }
        }

        private static function output() {
            if (self::$code != 200) {
                frame::HTTPCode(403);
                echo json_encode([
                    "code" => self::$code,
                    "msg" => self::$msg
                ]);
                return;
            }

            $data = json_encode(self::$data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            if (self::$authOption & self::NEED_SIG) {
                $dataEncrypt = self::$client->encrypt($data, self::$id);
                $sig = self::$client->calcSignature($dataEncrypt, self::$id);

                echo json_encode([
                    "code" => self::$code,
                    "id" => self::$id,
                    "data" => $dataEncrypt,
                    "sig" => $sig
                ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            } else {
                echo json_encode([
                    "code" => self::$code,
                    "data" => base64_encode($data)
                ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }
        }

        private static function invalid(int $code, string $msg) {
            self::$code = $code;
            self::$msg = $msg;
        }

        public static function parse() {
            if (self::parseURL() && self::parseInput() && self::remoteProcedureCall());
            self::output();
        }
    }
?>