<?php

    /**
     * Log operation
     * 
     * @since 1.0
     */
    
    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class log {
        public static function encrypt($text) {
            return frame::twoWayEncryption($text, frame::configGet("secuirty/master"), frame::configGet("secuirty/log"));
        }

        public static function decrypt($text) {
            return frame::twoWayDecryption($text, frame::configGet("secuirty/master"), frame::configGet("secuirty/log"));
        }

        private static function removeSensitiveInfo(&$arr, $name) {
            if (isset($arr[$name])) {
                $arr[$name] = "***REMOVED***";
            }
        }

        private static function removePOSTSensitiveInfo($arr) {
            self::removeSensitiveInfo($arr, "password");
            self::removeSensitiveInfo($arr, "oldpassword");
            self::removeSensitiveInfo($arr, "newpassword");
            self::removeSensitiveInfo($arr, "newpassword_repeat");
            self::removeSensitiveInfo($arr, "code_2fa");
            return $arr;
        }

        /**
         * Write log into database
         * 
         * @param int $logLevel
         * log level
         * @param int $logCode
         * log code
         * @param string $logTitle
         * log text
         * @param string $logInfo
         * log info
         * @param string $execStack
         * log stack
         * 
         * @return null
         */

        private static function writeLogIntoDatabase($logLevel, $logCode, $logTitle, $logInfo, $execStack){
            $logURL = frame::getURL();
            $guestIP = frame::getIP();
            $guestOS = frame::getOS();
            $guestBrowser = frame::getBrowser();

            $fieldGet = json_encode($_GET);
            $fieldPost = json_encode(self::removePOSTSensitiveInfo($_POST));
            $fieldCookie = json_encode($_COOKIE);
            $fieldSession = json_encode($_SESSION);
            
            $user = new user();
            $userID = $user->getID();

            if (defined("LOG_OVERRIDE_IP")) {
                $override_ip = constant("LOG_OVERRIDE_IP");

                if (!empty($override_ip) && filter_var($override_ip, FILTER_VALIDATE_IP)) {
                    $guestIP = $override_ip;
                }
            }

            if (defined("LOG_OVERRIDE_UID")) {
                $override_uid = constant("LOG_OVERRIDE_UID");

                if (!empty($override_uid) && is_numeric($override_uid)) {
                    $user = new user($override_uid);
                    if ($user->checkExist()) {
                        $userID = $override_uid;
                    }
                }
            }

            $date = date("Y-m-d H:i:s");
            $logHash = frame::oneWayEncryption(date("Y-m-d H:i").$execStack.$fieldGet.$fieldPost.$fieldCookie.$fieldSession.$guestIP.$guestOS.$guestBrowser.$logURL.$logInfo.$userID,  frame::configGet("secuirty/log"), 64);

            $logHashQuery = db::num_rows("SELECT `id` FROM `TABLEPREFIX_logs` where `logHash` = ? LIMIT 1", "s", [$logHash]);
            
            if(!$logHashQuery){
                $logURL = self::encrypt($logURL);
                $execStack =  self::encrypt($execStack);
                
                $fieldGet = self::encrypt($fieldGet);
                $fieldPost = self::encrypt($fieldPost);
                $fieldCookie = self::encrypt($fieldCookie);
                $fieldSession = self::encrypt($fieldSession);
                
                $res = db::insert("INSERT INTO `TABLEPREFIX_logs` (`logLevel`, `logHash`, `logCode`, `logTitle`, `logInfo`, `logUrl`, `execStack`, `guestIP`, `guestOS`, `guestBrowser`, `fieldGet`, `fieldPost`, `fieldCookie`, `fieldSession`, `uid`, `date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", "isisssssssssssis", [$logLevel, $logHash, $logCode, $logTitle, $logInfo, $logURL, $execStack, $guestIP, $guestOS, $guestBrowser, $fieldGet, $fieldPost, $fieldCookie, $fieldSession, $userID, $date]);
            }
        }

        /**
         * Write Log
         * 
         * log opinion is sum of writeIntoFile(1), writeIntoDatabase(2) and printErrorMessage(4)
         * 
         * log level consist of ERROR(1), WARNING(2), INFO(3)
         * 
         * @param int $case
         * log opinion
         * @param int $logLevel
         * log level
         * @param int $logCode
         * log code
         * @param string $logTitle
         * log text
         * @param string $logInfo
         * log info
         * 
         * @return null
         */

        public static function writeLog($case, $logLevel, $logCode, $logTitle, $logInfo = ""){        
            if ($logLevel == 4 && frame::configGet("system/debug") != true) {
                return;
            }

            $execStack = json_encode(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), TRUE);

            if($case % 2 == 1){
                frame::basicLog($logLevel, $logCode, $logTitle, $logInfo);
            }

            if($case % 4 >= 2){
                self::writeLogIntoDatabase($logLevel, $logCode, $logTitle, $logInfo, $execStack);
            }

            if($case >= 4){
                $siteName = frame::getConst("site_name");
                $siteURL = "";

                if (function_exists("getSiteLink")) {
                    $siteURL = getSiteLink("");
                }
                
                frame::assert($logLevel == 1, "Receive dump signal but not a fatal error");
                frame::printErrorMessage($logCode, $logTitle, ["site_name" => $siteName, "site_url" => $siteURL]);
            }
        }
    }
?>