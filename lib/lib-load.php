<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    require_once ABSPATH . "lib/framework/load.php";

    if (version_compare(framework_version, "6.0", "<")) {
        exit("Error: Minimum framework version required 6.0, current ".framework_version);
    }

    function loadFile($filepath) {
        if (!include_once(ABSPATH . $filepath)) {
            throw new Error("Failed to require {$filepath}");
        }
    }

    function load3rdparty($name) {
        if (!include_once(ABSPATH . "lib/3rdparty/{$name}.php")) {
            throw new Error("Failed to load 3rd party library {$name}");
        }
    }

    spl_autoload_register(function($class_name) {
        if (file_exists(ABSPATH . "lib/class-". $class_name . '.php')) {
            loadFile("/lib/class-". $class_name . '.php');
        }
    });

    loadFile("/data/config/config.inc.php");
    cache::init();

    function initSession() {
        ini_set('session.name', 'CLIENTID');
        ini_set("session.sid_length", "40");
        ini_set("session.sid_bits_per_character", "5");
        ini_set("session.save_handler", "redis");
        ini_set("session.save_path", cache::getAddress());
    }

    function initService() {
        loadFile("/lib/lib-version.php");
        
        db::set(frame::configGet("mysql/ip"), frame::configGet("mysql/username"), frame::configGet("mysql/password"), frame::configGet("mysql/database"), frame::configGet("mysql/port"), frame::configGet("mysql/table_prefix"));
        
        loadFile("/lib/lib-function.php");
        loadSystemVariables();
    }

    function initServer() {
        initSession();
        frame::initSession();
        
        initService();
        loadFile("/lib/lib-route.php");        
    }
    
    function initCommandline() {
        initService();
        loadFile("/lib/lib-cli.php");
    }

    function initAPI() {
        initSession();
        initService();
        api::parse();
    }

    if (defined("cli")) {
        initCommandline();
    } else if (defined("api")) {
        initAPI();
    } else {
        initServer();
    }
?>