<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    $user = new user();

    route::setData(["user" => $user]);

    route::header("template/header");
    route::footer("template/footer");
    
    route::route("/api/auth", "template/api/auth", NULL, TRUE);

    route::routeFunctionExclusive("/upgrade-your-browser", function() {
        frame::upgradeBrowser();
    });

    route::routeFunctionExclusive("/callback", function($user) {
        $callbackToken = getParam("id", "GET");

        if (!frame::issetSession("auth_{$callbackToken}_callback")) {
            redirect("/404");
        }

        $callbackSiteName = frame::readSession("auth_{$callbackToken}_callback_name");

        if ($user->checkLogin()) {
            exit("<script>sessionStorage.setItem('callback', '{$callbackToken}'); window.location.href = '/?redirect={$callbackToken}';</script>");
        } else {
            exit("<script>sessionStorage.setItem('callback', '{$callbackToken}'); sessionStorage.setItem('callbackName', '{$callbackSiteName}'); window.location.href = '/';</script>");
        }
    }, [$user]);

    route::routeFunctionExclusive("/verify/(\S+)", function() {
        preg_match_all("/\/verify\/(\S+)/", frame::getAbsoluteURL(), $arr);
        $token = $arr[1][0];
        $verifyInfo = db::selectFirst("select `id`, `emailAuthExpire` from `TABLEPREFIX_users` where `emailAuthToken` = ?", "s", [$token]);

        if ($verifyInfo && time() <= $verifyInfo["emailAuthExpire"]) {
            $uid = $verifyInfo["id"];
            $user = new user($uid);
            
            db::update("update `TABLEPREFIX_users` SET `emailAuthToken` = null, `emailAuthExpire` = null where `id` = ?", "i", [$uid]);
            db::update("update `TABLEPREFIX_userinfo` SET `email` = ?, `emailUnverify` = null where `uid` = ?", "si", [$user->getUserinfo()["emailUnverify"], $uid]);

            user::cacheUserUpdate($uid);
            user::cacheUserInfoUpdate($uid);

            echo "<script>alert('认证成功');window.location.href='".getSiteLink("")."';</script>";
        } else {
            echo "<script>alert('链接不存在或已过期');window.location.href='".getSiteLink("")."';</script>";
        }
        exit;
    }, []);
    
    route::route("/404", "template/404", "404", FALSE);
    
    if (!$user->checkLogin()) {
        route::route("/", "template/login", "登录", FALSE);
        route::route("/2fa", "template/login_2fa", "两步验证", FALSE);
        route::route("/forget", "template/forget", "找回账号", FALSE);
        route::route("/forget/(\S+)", "template/forget", "找回账号", FALSE);
        route::route("/register", "template/register", "注册", FALSE);
    } else {
        route::route("/", "template/profile/profile", "个人信息", FALSE);
        route::route("/edit", "template/profile/edit", "个人信息", FALSE);
        route::route("/security", "template/profile/security", "安全设置", FALSE);
        route::route("/ecjtu", "template/profile/ecjtu", "智慧交大绑定", FALSE);

        route::routeFunctionExclusive("/logout", function($user){
            $user->logout();
            
            if (empty($_GET["redirect"])) {
                redirect(getSiteLink(""));
            } else {
                redirect($_GET["redirect"]);
            }
        }, [$user]);

        if ($user->isAdmin()) {
            route::route("/forget", "template/forget", "找回账号", FALSE);
            route::route("/forget/(\S+)", "template/forget", "找回账号", FALSE);
            
            route::route("/panel/permission/(\d+)/list", "template/panel/permission_list", "站点权限列表", FALSE);
            route::route("/panel/permission/(\d+)/user", "template/panel/permission_user", "站点用户权限", FALSE);
            route::route("/panel/permission/(\d+)/group", "template/panel/permission_group", "站点用户组权限", FALSE);

            route::route("/panel/sites", "template/panel/sites", "站点管理", FALSE);

            route::route("/panel/user/list", "template/panel/user_list", "注册用户列表", FALSE);
            route::route("/panel/user/3rdparty", "template/panel/user_3rdparty", "三方用户列表", FALSE);
            route::route("/panel/user/ecjtu", "template/panel/user_ecjtu", "智慧交大绑定", FALSE);

            route::route("/panel/group/list", "template/panel/group_list", "用户组列表", FALSE);
            route::route("/panel/group/assign", "template/panel/group_assign", "用户组分配", FALSE);
            route::route("/panel/group/preset", "template/panel/group_preset", "用户组预置", FALSE);

            route::route("/panel/system/log", "template/panel/system_log", "系统日志", FALSE);
            route::route("/panel/system/log/detail", "template/panel/system_log_detail", "系统日志", FALSE);
            route::route("/panel/system/setting", "template/panel/system_setting", "系统设置", FALSE);
        }
    }

    route::checkRoute();
    route::print();
?>