<?php

    /**
     * Client Operation
     * 
     * @since 1.0
     */

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    class client {
        private $id = null;
        private $authClient = false;
        private $info = [];

        private function cacheClient($cid) {
            return Cache::getArray("Client_{$cid}", function($cid) {
                return db::selectFirst("SELECT * FROM `TABLEPREFIX_clients` where `id` = ?", "i", [$cid]);
            },array($cid));
        }
        
        public static function cacheClientUpdate($cid) {
            Cache::unset("Client_{$cid}");
        }

        public static function cacheClientDisplayListUpdate() {
            Cache::unset("Client_Display_List");
        }

        function __construct($cid) {
            if (empty($cid) || !is_numeric($cid)) {
                return;
            }

            $this->id = $cid;
            $this->info = $this->cacheClient($this->id);

            if (empty($this->info)) {
                $this->id = null;
                $this->info = [];
                return;
            }
        }

        public function getID() {
            return $this->id;
        }

        public function getClientInfo() {
            return $this->info;
        }

        public function check() {
            return $this->id != NULL;
        }

        public function isEnable() {
            return $this->info["isEnable"];
        }

        public function isDisplay() {
            return $this->info["isDisplay"];
        }

        public function isAuth() {
            return $this->check() && $this->isEnable() && $this->authClient;
        }

        public static function cacheClientDisplayList() {
            return Cache::getArray("Client_Display_List", function() {
                return db::selectAll("SELECT * FROM `TABLEPREFIX_clients` where (`isDisplay` = 1 or `isDisplay` = -1) and `isEnable` = 1");
            });
        }

        public static function getClientDisplayList($user) {
            $sites = self::cacheClientDisplayList();
            $sites_count = count($sites);

            for ($i=0; $i < $sites_count; $i++) {
                if ($sites[$i]["isDisplay"] == -1 && $user->getPermission($sites[$i]["id"])['_display_frontend'] != TRUE) {
                    unset($sites[$i]);
                }
            }

            return $sites;
        }

        public static function getClientList() {
            return db::selectAll("SELECT `id`, `shortname` FROM `TABLEPREFIX_clients` where `isEnable` = 1");
        }

        public function auth($salt, $token) {
            $salt_len = strlen($salt);
            if ($salt_len < 16 || $salt_len > 32) return;
            
            if ((frame::oneWayEncryption($salt, $this->getClientInfo()["masterKey"], 128) == $token)) {
                $this->authClient = db::insert("INSERT INTO `TABLEPREFIX_client_auth` (`cid`, `salt`) VALUES (?, ?)", "is", [$this->id, $salt]) > 0;
            };
        }

        public function updateStatus($status) {
            if ($this->id == 1) return;
            db::update("UPDATE `TABLEPREFIX_clients` SET `isEnable` = ? where `id` = ?", "ii", [$status, $this->id]);
            self::cacheClientUpdate($this->id);
            self::cacheClientDisplayListUpdate();
        }

        public function updateInfo($name, $shortname, $url, $display) {
            db::update("UPDATE `TABLEPREFIX_clients` SET `name` = ?, `shortname` = ?, `url` = ?, `isDisplay` = ? where `id` = ?", "sssii", [$name, $shortname, $url, $display, $this->id]);
            self::cacheClientUpdate($this->id);       
            self::cacheClientDisplayListUpdate();     
        }

        public static function generateToken() {
            return frame::randString(48);
        }

        public function resetToken() {
            $token = self::generateToken();
            db::update("UPDATE `TABLEPREFIX_clients` SET `masterKey` = ? where `id` = ?", "si", [$token, $this->id]);
            self::cacheClientUpdate($this->id);
            return $token;
        }

        public function delete() {
            if ($this->id == 1) return;

            db::delete("DELETE FROM `TABLEPREFIX_clients` where `id` = ?", "i", [$this->id]);
            db::delete("DELETE FROM `TABLEPREFIX_permission_list` where `cid` = ?", "i", [$this->id]);
            db::delete("DELETE FROM `TABLEPREFIX_permission_user` where `cid` = ?", "i", [$this->id]);
            db::delete("DELETE FROM `TABLEPREFIX_permission_group` where `cid` = ?", "i", [$this->id]);

            self::cacheClientUpdate($this->id);
            self::cacheClientDisplayListUpdate();
        }

        public function calcSignature($data, $salt) {
            return frame::oneWayEncryption($data, $this->getClientInfo()["masterKey"].$salt, 40);
        }

        public function encrypt($data, $salt) {
            return frame::twoWayEncryption($data, $this->getClientInfo()["masterKey"], $salt);
        }

        public function decrypt($data, $salt) {
            return frame::twoWayDecryption($data, $this->getClientInfo()["masterKey"], $salt);
        }
    }
?>