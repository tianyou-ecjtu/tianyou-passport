<?php

    if(!defined("load")){
        header("Location:/404");
        exit;
    }

    load3rdparty("phpmailer");

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    class mail {
        public static function queue($target, $template, $title, $data = []) {
            db::insert("INSERT INTO `TABLEPREFIX_email` (`target`, `template`, `title`, `data`) VALUES (?, ?, ?, ?)", "ssss", [$target, $template, $title, json_encode($data, JSON_UNESCAPED_UNICODE)]);
        }

        private static function getSMTPInfo() {
            $servers = frame::configGet("smtp");
            $index = ((int)frame::randString(10, "123456789")) % count($servers);
            return $servers[$index];
        }

        private static function send($id) {
            $email = db::selectFirst("SELECT * FROM `TABLEPREFIX_email` where `id` = ?", "i", [$id]);

            if (!$email) {
                return;
            }

            $target = $email["target"];
            $template = $email["template"];
            $title = $email["title"];
            $data = json_decode($email["data"], TRUE);

            ob_start();
            include ABSPATH . "lib/template/mail/{$template}.php";
            $mailcontent = ob_get_clean();
    
            mail::sendMail($target, $title, $mailcontent);

            db::update("UPDATE `TABLEPREFIX_email` SET `status` = 1 where `id` = ?", "i", [$id]);
        }
        
        private static function sendMail($target, $title, $text) {
            $mail = new PHPMailer(true); 
            $server = mail::getSMTPInfo();

            $mail->CharSet ="UTF-8";
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $server["server"];
            $mail->SMTPAuth = true;
            $mail->Username = $server["username"];
            $mail->Password = $server["password"];
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            
            $mail->setFrom($server["username"], frame::configGet("site/organization"));
            $mail->addAddress($target, '');
            
            $mail->isHTML(true);
            $mail->Subject = $title;
            $mail->Body    = $text;
            $mail->AltBody = '';

            $mail->send();
        }

        public static function cron() {
            $emails = db::selectAll("SELECT `id` FROM `TABLEPREFIX_email` where `status` = 0");
            foreach ($emails as $email) {
                self::send($email["id"]);
            }
        }
    }
?>